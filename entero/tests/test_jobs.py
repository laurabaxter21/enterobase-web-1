# import unittest
import pytest
# import flask
import ujson
#from flask import current_app
#from entero import create_app, config
#import os

# WVN 27/2/18 Started to get problems looking up app.config
# from entero.ExtraFuncs.workspace import LocusSearch,MSTree
# from entero.jobs.jobs import get_crobot_job,MSTreeJob,NomenclatureJob,GenericJob,AssemblyJob
# from entero.ExtraFuncs.temp import UGSGenerator

from entero.test_utilities import jack_user_id

database="senterica"
# jack_user_id=158
params={"database":database,"user_id":jack_user_id}

# Tests based on old test_scheme_method code in manage.py
# Next test fails but not actually sure why - get
# connection error - no route to host
# Confirm to do that in its original place when uncommented
# from manage.py
def test_get_crobot_job(app):
    # WVN possibly unnecessary hack - redundant use of instance
    # to ensure app is used somehow
    # assert isinstance(app, flask.Flask)

    from entero.jobs.jobs import get_crobot_job
    # ID used below gives an interesting error which may be worth
    # turning into it's own test in the future - possibly relating
    # to QAssembly_ST types of job being discontinued or something.
    # (But I found it by copying Martin's test code in correctly.)
    # Job 268173 is for a pipeline that doesn't exist and Martin
    # reckons thing below should throw an exception.
    # ls = get_crobot_job(268173)
    # Not allowed to update the job below for some reason - not sure what the required behaviour
    # should be in tests so not testing it
    ls = get_crobot_job(2681713)

    assert ls != None
    
class TestJobManagement():
    
    # ID used below gives an interesting error which may be worth
    # turning into it's own test in the future - possibly relating
    # to QAssembly_ST types of job being discontinued or something.
    # (But I found it by copying Martin's test code in correctly.)
    # Job 268173 is for a pipeline that doesn't exist and Martin
    # reckons thing below should throw an exception.
    # ls = get_crobot_job(268173)    
    
    @pytest.fixture(scope = "class", params = [2689223, 2689120, 2685729])
    def get_crobot_job_res(self, app, request):
        from entero.jobs.jobs import get_crobot_job
        return get_crobot_job(request.param)
                
    def test_get_crobot_job(self, get_crobot_job_res):
        assert get_crobot_job_res is not None
        
    def test_update_job(self, get_crobot_job_res):
        try:
            get_crobot_job_res.update_job()
            assert True # Don't have a better way to test this yet other than check if we get an exception
            
            # try another update - supposed to be idempotent or something
            try:
                get_crobot_job_res.update_job()
                assert True
            except:
                assert False
        except:
            assert False
            
            def test_process_job(self, get_crobot_job_res):
                    try:
                        get_crobot_job_res.process_job()
                        assert True # Don't have a better way to test this yet other than check if we get an exception
                    except:
                        assert False    

# Tests based on code near beginning of test_scheme_method in manage.py
# They actually fail; but I'm not sure if this is a security issue with
# connecting to the relevant server from hercules.  I've confirmed that
# if I uncomment the code in its original place it fails with the same
# error.
class TestSendLocusSearch():
    @pytest.fixture(scope="class")
    def locus_search(self, app):
        # Moved import here because it needs the app fixture, I think
        from entero.ExtraFuncs.workspace import LocusSearch
        
        scheme = "cgMLST_v2"
        myparams = params.copy()
        # "latest_locus_search" leaves less mess around
        # myparams['name']="__test__locus_search"
        myparams['name']="latest__locus_search"
        fasta = open("/share_space/test_data/senterica.fasta").read()
        ls = LocusSearch.create_locus_search(myparams,scheme,fasta)
        # Moved send back here again
        ls.send_job()
        return ls

    def test_create_locus_search(self, locus_search):
        assert locus_search != None

    def test_send_locus_search(self, locus_search):
        # print "locus_search dir: ",  dir(locus_search)
        # WVN VTEMP - assert doesn't make any sense as method doesn't return anything
        # Rather should check stuff that the method changes at the end
        # assert locus_search.send_job() != None
        # WVN 26/2/18 Don't really like this test; but one of these should be
        # true if the method ran correctly.  (Potentially correct execution
        # of the code can happen, but with failure of the job on the server.)
        assert "failed" in locus_search.data or "job_id" in locus_search.data

    def test_send_locus_search_got_job_id(self, locus_search):
        # Changed my mind though - if it fails we ought to investigate it.
        # Decided to have the above assert and the job_id specific one in separate tests.
        assert "job_id" in locus_search.data


class TestSendMSTree():    
    @pytest.fixture(scope="class")
    def mstree(self, app):
        from entero.ExtraFuncs.workspace import MSTree
        
        scheme="cgMLST_v2"
        myparams = params.copy()
        myparams['name']="__test_ms_tree"
        algorithm="MSTreeV2"
        data=ujson.loads(open("/share_space/test_data/ms_tree_data.json").read())
        parameters={"strain_number":15,"algorithm":"MSTreeV2","scheme":"test"}
        ms = MSTree.create_ms_tree(myparams,data,algorithm,parameters)
        ms.send_job()
        return ms
    
    def test_mstree_not_none(self, mstree):
        assert mstree is not None
        
    def test_send_mstree(self, mstree):
        assert "failed" in mstree.data or "job_id" in mstree.data
        
    def test_send_locus_search_got_job_id(self, mstree):
            # Decided to have the above assert and the job_id specific one in separate tests.
            assert "job_id" in mstree.data
            
class TestSendNomenclatureJob():
    nomenclature_job_retval = False
        
    @pytest.fixture(scope="class")
    def nomenclature_job(self, eb_dbhandle):
        from entero.jobs.jobs import NomenclatureJob
        
        Schemes = eb_dbhandle['senterica'].models.Schemes
        scheme = eb_dbhandle["senterica"].session.query(Schemes).filter_by(id=1).one()
         
        job = NomenclatureJob(scheme=scheme,
                                  assembly_filepointer="/share_space/interact/outputs/2686/2686203/SAL_YA3618AA_AS.scaffold.fastq",
                                  assembly_barcode="SAL_YA3618AA_AS",
                                  database='senterica',
                                  user_id=jack_user_id)
        TestSendNomenclatureJob.nomenclature_job_retval = job.send_job()
        return job
    
    def test_nomenclature_job_not_none(self, nomenclature_job):
        assert nomenclature_job is not None
        
    def test_send_nomenclature_job_success(self, nomenclature_job):
        assert TestSendNomenclatureJob.nomenclature_job_retval is True
        # VTEMP - ignore
        #assert True
        
    def test_send_nomenclature_job_got_job_id(self, nomenclature_job):
        # Decided to have the above assert and the job_id specific one in separate tests.
        assert hasattr(nomenclature_job, "job_id")  
        
class TestSendGenericJob():
    generic_job_retval = False
        
    @pytest.fixture(scope="class")
    def generic_job(self, eb_dbhandle):
        from entero.jobs.jobs import GenericJob
        
        Schemes = eb_dbhandle['senterica'].models.Schemes
        scheme = eb_dbhandle["senterica"].session.query(Schemes).filter_by(description = "SeroPred").one()
         
        job = GenericJob(assembly_filepointer= "/share_space/interact/outputs/2687/2687727/SAL_YA3946AA_AS.scaffold.fastq",
                         assembly_barcode= "SAL_YA3946AA_AS",
                         database="senterica",
                         scheme=scheme,
                         user_id=jack_user_id)        
        TestSendGenericJob.generic_job_retval = job.send_job()
        return job
    
    def test_generic_job_not_none(self, generic_job):
        assert generic_job is not None
        
    def test_send_generic_job_success(self, generic_job):
        # assert True # TEMP lie
        assert TestSendGenericJob.generic_job_retval is True
        
    def test_send_generic_job_got_job_id(self, generic_job):
        # Decided to have the above assert and the job_id specific one in separate tests.
        assert hasattr(generic_job, "job_id")  
        
class TestSendAssemblyJob():
    assembly_job_retval = False
        
    @pytest.fixture(scope="class")
    def assembly_job(self, eb_dbhandle):
        from entero.jobs.jobs import AssemblyJob
        
        # Martin's original test code also had a similar test using user ID 0 (for the "crobort" login).
        # Decided not to bother with that.  (It will make it easier if something is done to tidy stuff up
        # from test jobs (easier to identify if done with "jack" or another special test login.)
        job = AssemblyJob(database='miu',
                          trace_ids=[244],
                          user_id=jack_user_id)        
        
        TestSendAssemblyJob.assembly_job_retval = job.send_job()
        
        return job
    
    def test_assembly_job_not_none(self, assembly_job):
        assert assembly_job is not None
        
    def test_send_assembly_job_success(self, assembly_job):
        # assert True # TEMP lie
        assert TestSendAssemblyJob.assembly_job_retval is True
        
    def test_send_assembly_job_got_job_id(self, assembly_job):
        # Decided to have the above assert and the job_id specific one in separate tests.
        assert hasattr(assembly_job, "job_id")

# WVN 1/3/18 Nabil thinks RemoteUGSJob was for an experimental pipeline by Martin anyway.        
class TestSendRemoteUGSJob():
    remote_ugs_job_retval = False
        
    @pytest.fixture(scope="class")
    def remote_ugs_job(self, eb_dbhandle):
        from entero.ExtraFuncs.temp import RemoteUGSJob
        
        Schemes = eb_dbhandle['miu'].models.Schemes
        scheme = eb_dbhandle["miu"].session.query(Schemes).filter_by(id=5).one()
        job = RemoteUGSJob(assembly_filepointer= "/share_space/interact/outputs/2689/2689223/MIU_AA0268AA_AS.scaffold.fastq",
                    assembly_barcode= "MIU_AA0268AA_AS",
                    database="miu",
                    scheme=scheme,
                    user_id=jack_user_id)        
        
        TestSendRemoteUGSJob.remote_ugs_job_retval = job.send_job()
        
        return job
    
    @pytest.mark.skip("RemoteUGSJob does not appear to be in current use and was just experimental - this test currently passing")
    def test_remote_ugs_job_not_none(self, remote_ugs_job):
        assert remote_ugs_job is not None

    @pytest.mark.skip("RemoteUGSJob does not appear to be in current use and was just experimental - expected to fail")
    def test_send_remote_ugs_job_success(self, remote_ugs_job):
        assert TestSendRemoteUGSJob.remote_ugs_job_retval is True
        
    @pytest.mark.skip("RemoteUGSJob does not appear to be in current use and was just experimental - expected to fail")
    def test_send_remote_ugs_job_got_job_id(self, remote_ugs_job):
        # Decided to have the above assert and the job_id specific one in separate tests.
        assert hasattr(remote_ugs_job, "job_id")

# It appears this is based on old code for a job class (BaseJob) that doesn't exist any more (like SuperJob)        
#class TestSendBaseJob():
#    base_job_retval = False
#        
#    @pytest.fixture(scope="class")
#    def base_job(self, app):
#        from entero.jobs.jobs import BaseJob
#        
#        job = BaseJob(params = "help")       
#        TestSendBaseJob.base_job_retval = job.send_job()
#        
#        return job
#    
#    def test_base_job_not_none(self, base_job):
#        assert base_job is not None
#        
#    def test_send_base_job_success(self, base_job):
#        assert TestSendBaseJob.base_job_retval is True
#        
#    def test_send_base_job_got_job_id(self, base_job):
#        # Decided to have the above assert and the job_id specific one in separate tests.
#        assert hasattr(base_job, "job_id")

class TestSendRefMaskerJob():
    refmasker_job_retval = False
        
    @pytest.fixture(scope="class")
    def refmasker_job(self, eb_dbhandle):
        from entero.jobs.jobs import RefMaskerJob
        
        dbase = eb_dbhandle['clostridium']
        ass = dbase.session.query(dbase.models.Assemblies).filter_by(barcode="CLO_BA3700AA_AS").one()
        refmasker_scheme = dbase.session.query(dbase.models.Schemes).filter_by(description='ref_masker').first()
        job = RefMaskerJob(scheme = refmasker_scheme,
                                  assembly_barcode = ass.barcode,
                                  assembly_filepointer = ass.file_pointer,
                                  workgroup = "user_upload",
                                  priority = -9,
                                  database = "miu")
        #  database="clostridium")
        TestSendRefMaskerJob.refmasker_job_retval = job.send_job()
        
        return job
    
    def test_send_refmasker_job_not_none(self, refmasker_job):
        assert refmasker_job is not None
        
    def test_send_refmasker_ugs_job_success(self, refmasker_job):
        assert TestSendRefMaskerJob.refmasker_job_retval is True
        
    def test_send_refmasker_job_got_job_id(self, refmasker_job):
        # Decided to have the above assert and the job_id specific one in separate tests.
        assert hasattr(refmasker_job, "job_id")

class TestSendRefMapperMatrixJob():
    refmapper_matrix_job_retval = False
        
    @pytest.fixture(scope="class")
    def refmapper_matrix_job(self, eb_dbhandle):
        from entero.jobs.jobs import RefMapperMatrixJob
        # import dbase.models
        #from entero.databases.generic_models import Assemblies
        
        dbase = eb_dbhandle['clostridium']
        #ass = dbase.session.query(dbase.models.Assemblies).filter_by(barcode = "CLO_BA3700AA_AS").one()
        # refmasker_scheme = dbase.session.query(dbase.models.Schemes).filter_by(description='ref_masker').first()
        #job = RefMaskerJob(scheme = refmasker_scheme,
        #                          assembly_barcode = ass.barcode,
        #                          assembly_filepointer = ass.file_pointer,
        #                          workgroup = "user_upload",
        #                          priority = -9,
        #                          database = "miu") 
        #  database="clostridium")
        # inputs? params? tag?
        # ref_barcode = "CLO_BA3700AA_AS"
        
        ref_barcode = "CLO_AA0242AA_AS"
        abarcodes_to_send = [ref_barcode, "CLO_AA0267AA_AS", "CLO_AA0556AA_AS"]
        Assemblies = dbase.models.Assemblies
        assemblies = dbase.session.query(Assemblies).filter(Assemblies.barcode.in_(abarcodes_to_send)).all()
        #inputs={"queries": {}}
        #params={"prefix": ref_barcode}
        #for ass in assemblies:
        #    if ass.barcode == ref_barcode:
        #        inputs['reference'] = ass.file_pointer
        #    else:
        #        inputs['queries'][ass.barcode] = ass.file_pointer

        reference = dbase.session.query(Assemblies).filter(Assemblies.barcode == ref_barcode).first()
        # get the refmasker
        Schemes=dbase.models.Schemes
        Lookup = dbase.models.AssemblyLookup 
        ref_masker_scheme = dbase.session.query(Schemes).filter_by(description="ref_masker").first()
        # ref_masker = dbase.session.query(Lookup).filter_by(assembly_id=self.ref_id,scheme_id=ref_masker_scheme.id).first()
        ref_masker = dbase.session.query(Lookup).filter_by(assembly_id = reference.id,scheme_id = ref_masker_scheme.id).first()
        masker_filepointer = ref_masker.other_data['file_location']
        gene_file = None # maybe change this to an annotation later
        gff_files = {"CLO_AA0267AA_AS": "/share_space/interact/outputs/2244/2244781/wvn_ribo027_snp.CLO_AA0267AA_AS.map.gff",
                     "CLO_AA0556AA_AS": "/share_space/interact/outputs/2244/2244781/wvn_ribo027_snp.CLO_AA0556AA_AS.map.gff"}
        
        #format all job parameters
        inputs={}
        params={}
        params['prefix']=ref_barcode
        # params['relax_core']=self.data.get('missing_sites',0.9)
        params['relax_core'] = 0.9
        if gene_file:
            inputs['genes'] = gene_file
        #change the reference filepointer from fastq to fasta
        ref_filepointer = reference.file_pointer
        if ref_filepointer.endswith("q"):
            ref_filepointer = ref_filepointer[:-1]+"a"
        inputs['reference'] = ref_filepointer
        inputs['repeat'] = masker_filepointer
        inputs['queries'] = gff_files
        
        #add predifned snp list if any
        # snp_list_id=self.data.get("snp_list")        
        job = RefMapperMatrixJob(inputs = inputs, params = params,
                          database = "miu",tag = "SendRefMapperMatrixJobTest", priority = -9,
                          user_id =  jack_user_id, workgroup = "user_upload")        
        TestSendRefMapperMatrixJob.refmapper_matrix_job_retval = job.send_job()
        
        return job
    
    def test_send_refmapper_matrix_job_not_none(self, refmapper_matrix_job):
        assert refmapper_matrix_job is not None
        
    def test_send_refmapper_matrix_job_success(self, refmapper_matrix_job):
        assert TestSendRefMapperMatrixJob.refmapper_matrix_job_retval is True
        
    def test_send_refmapper_matrix_job_got_job_id(self, refmapper_matrix_job):
        # Decided to have the above assert and the job_id specific one in separate tests.
        assert hasattr(refmapper_matrix_job, "job_id")

class TestSendRefMapperJob():
    refmapper_job_retval = False
        
    @pytest.fixture(scope="class")
    def refmapper_job(self, eb_dbhandle):
        from entero.jobs.jobs import RefMapperJob
        
        dbase = eb_dbhandle['clostridium']
        
        ref_barcode = "CLO_AA0242AA_AS"
        abarcodes_to_send = [ref_barcode, "CLO_AA0267AA_AS", "CLO_AA0556AA_AS"]
        Assemblies = dbase.models.Assemblies
        assemblies = dbase.session.query(Assemblies).filter(Assemblies.barcode.in_(abarcodes_to_send)).all()
        inputs={"queries": {}}
        params={"prefix": ref_barcode}
        for ass in assemblies:
            # if ass.id == self.ref_id:
            if ass.barcode == ref_barcode:
                inputs['reference'] = ass.file_pointer
            else:
                inputs['queries'][ass.barcode] = ass.file_pointer
        
        job = RefMapperJob(inputs = inputs, params = params,
                          database = "miu",tag = "SendRefMapperJobTest", priority = -9,
                          user_id =  jack_user_id, workgroup = "user_upload")        
        TestSendRefMapperJob.refmapper_job_retval = job.send_job()
        
        return job
    
    def test_send_refmapper_job_not_none(self, refmapper_job):
        assert refmapper_job is not None
        
    def test_send_refmapper_job_success(self, refmapper_job):
        assert TestSendRefMapperJob.refmapper_job_retval is True
        
    def test_send_refmapper_job_got_job_id(self, refmapper_job):
        # Decided to have the above assert and the job_id specific one in separate tests.
        assert hasattr(refmapper_job, "job_id")

class TestSendMatrixPhylogenyJob():
    matrix_phylogeny_job_retval = False
        
    @pytest.fixture(scope="class")
    def matrix_phylogeny_job(self, eb_dbhandle):
        from entero.jobs.jobs import MatrixPhylogenyJob
        
        dbase = eb_dbhandle['clostridium']
        
        ref_barcode = "CLO_AA0242AA_AS"
        
        #abarcodes_to_send = [ref_barcode, "CLO_AA0267AA_AS", "CLO_AA0556AA_AS"]
        #Assemblies = dbase.models.Assemblies
        #assemblies = dbase.session.query(Assemblies).filter(Assemblies.barcode.in_(abarcodes_to_send)).all()
        #inputs={"queries": {}}
        #params={"prefix": ref_barcode}
        #for ass in assemblies:
        #    # if ass.id == self.ref_id:
        #    if ass.barcode == ref_barcode:
        #        inputs['reference'] = ass.file_pointer
        #    else:
        #        inputs['queries'][ass.barcode] = ass.file_pointer
        
        #job = RefMapperJob(inputs = inputs, params = params,
        #                  database = "miu",tag = "SendRefMapperJobTest", priority = -9,
        #                  user_id =  jack_user_id, workgroup = "user_upload")
        inputs ={"matrix": "/share_space/interact/outputs/2244/2244815/wvn_ribo027_snp.aln.matrix.gz"}
        params={"prefix": ref_barcode}        
        job = MatrixPhylogenyJob(inputs = inputs,params = params,
                                 database = "miu", tag = "SendMatrixPhylogenyJobTest",
                                 priority = -9, workgroup = "user_upload",
                                 user_id = jack_user_id)        
        
        TestSendMatrixPhylogenyJob.matrix_phylogeny_job_retval = job.send_job()
        
        return job
    
    def test_send_matrix_phylogeny_job_not_none(self, matrix_phylogeny_job):
        assert matrix_phylogeny_job is not None
        
    def test_send_matrix_phylogeny_job_success(self, matrix_phylogeny_job):
        assert TestSendMatrixPhylogenyJob.matrix_phylogeny_job_retval is True
        
    def test_send_matrix_phylogeny_job_got_job_id(self, matrix_phylogeny_job):
        # Decided to have the above assert and the job_id specific one in separate tests.
        assert hasattr(matrix_phylogeny_job, "job_id")
