# import unittest
import pytest
#import flask
#from flask import current_app
#from entero import create_app, config
#import os
# from entero.databases.system.models import User

from entero.test_utilities import ebHTTPNotAuth, get_api_headers, enHashInfoQueryResponseData

from urllib2 import HTTPError
import urllib2
import json
import re

@pytest.mark.slow
class TestAllelesQueryCase1(object):
    alleles_query1 = "/api/v2.0/senterica/MLST_Achtman/alleles?locus=aroC&limit=50"

    # Actually, see if I can do setup via class-level fixtures
    # @classmethod
    # def setup_class(cls):
    #      pass

    # @classmethod
    # def teardown_class(cls):
    #     pass
  
    @pytest.fixture(scope='class') 
    def alleles_query1_bad_auth_response(self, eb_api_client):
        response = eb_api_client.get(self.alleles_query1, headers=get_api_headers("aninvalidkey"))
        return response

    @pytest.fixture(scope='class') 
    def alleles_query1_jack_auth_response(self, eb_api_client, eb_jack_api_key):
        response = eb_api_client.get(self.alleles_query1, headers=get_api_headers(eb_jack_api_key))
        return response

    @pytest.fixture(scope='class') 
    def alleles_query1_jack_auth_response_data(self, alleles_query1_jack_auth_response):
        return json.loads(alleles_query1_jack_auth_response.get_data(as_text=True))

    @pytest.mark.skip(reason = "Known that test fails due to status code 403 when it should get status code 401")
    def test_alleles_query1_bad_auth_status(self, alleles_query1_bad_auth_response):
        # WVN 12/3/18 For the not logged in client, it really is correct to get
        # a 401 error not a 403 error (and ebHTTPNotAuth is the latter)
        # assert alleles_query1_bad_auth_response.status_code == ebHTTPNotAuth
        assert alleles_query1_bad_auth_response.status_code == 401

    def test_alleles_query1_jack_auth_status(self, alleles_query1_jack_auth_response):
        assert alleles_query1_jack_auth_response.status_code == 200

    def test_alleles_query1_non_empty(self, alleles_query1_jack_auth_response_data):
        # response = eb_api_client.get(self.alleles_query1, headers=get_api_headers(eb_jack_api_key))
        # Not sure what checks to do on content apart from ensuring it is non-empty
        # responseData = json.loads(alleles_query1_jack_auth_response.get_data(as_text=True))
        # These two asserts really are checking pretty much the same thing - no
        # point in breaking out into separate tests
        # assert responseData is not None
        # assert len(responseData) > 0
        assert alleles_query1_jack_auth_response_data is not None
        assert len(alleles_query1_jack_auth_response_data) > 0

    def test_alleles_query1_has_alleles_key(self, alleles_query1_jack_auth_response_data):
        assert "alleles" in alleles_query1_jack_auth_response_data

    def test_alleles_query1_has_links_key(self, alleles_query1_jack_auth_response_data):
        assert "links" in alleles_query1_jack_auth_response_data

class TestAllelesQueryCaseMalformed(object):
    alleles_query1 = "/api/v2.0/senterica/MLST_Achtman/alleles?barcode=foobar&locus=aroC&limit=50"

    @pytest.fixture(scope='class') 
    def alleles_query1_bad_auth_response(self, eb_api_client):
        response = eb_api_client.get(self.alleles_query1, headers=get_api_headers("aninvalidkey"))
        return response

    @pytest.fixture(scope='class') 
    def alleles_query1_jack_auth_response(self, eb_api_client, eb_jack_api_key):
        response = eb_api_client.get(self.alleles_query1, headers=get_api_headers(eb_jack_api_key))
        return response

    @pytest.fixture(scope='class') 
    def alleles_query1_jack_auth_response_data(self, alleles_query1_jack_auth_response):
        return json.loads(alleles_query1_jack_auth_response.get_data(as_text=True))

    @pytest.mark.skip(reason = "Known that test fails due to status code 403 when it should get status code 401")
    def test_alleles_query1_bad_auth_status(self, alleles_query1_bad_auth_response):
        # WVN 12/3/18 For the not logged in client, it really is correct to get
        # a 401 error not a 403 error (and ebHTTPNotAuth is the latter)
        assert alleles_query1_bad_auth_response.status_code == 401

    def test_alleles_query1_jack_auth_status(self, alleles_query1_jack_auth_response):
        assert alleles_query1_jack_auth_response.status_code == 400
