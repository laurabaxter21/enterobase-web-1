import pytest
import flask
#from flask import current_app
#from entero import create_app, config

# WVN 27/2/18 Started to get problems looking up app.config
# from entero.ExtraFuncs.workspace import LocusSearch,MSTree
# from entero.jobs.jobs import get_crobot_job,MSTreeJob,NomenclatureJob,GenericJob,AssemblyJob
# from entero.ExtraFuncs.temp import UGSGenerator

import os

# WVN 12/2/18 Got rid off class related to xunit style tests since
# I am using fixtures

def test_app_exists():
    assert not (flask.current_app is None)

def test_app(app):
    assert isinstance(app, flask.Flask)

# Tests based on old test_scheme_method code in manage.py
# Next test fails but not actually sure why - get
# connection error - no route to host
# Confirm to do that in its original place when uncommented
# from manage.py
def test_get_crobot_job(app):
    # WVN possibly unnecessary hack - redundant use of instance
    # to ensure app is used somehow
    # assert isinstance(app, flask.Flask)

    from entero.jobs.jobs import get_crobot_job
    # ID used below gives an interesting error which may be worth
    # turning into it's own test in the future - possibly relating
    # to QAssembly_ST types of job being discontinued or something.
    # (But I found it by copying Martin's test code in correctly.)
    # Job 268173 is for a pipeline that doesn't exist and Martin
    # reckons thing below should throw an exception.
    # ls = get_crobot_job(268173)
    # Not allowed to update the job below for some reason - not sure what the required behaviour
    # should be in tests so not testing it
    ls = get_crobot_job(2681713)

    assert ls != None
