CanvasHeatMap.prototype = Object.create(null);
CanvasHeatMap.constructor = CanvasHeatMap;

function CanvasHeatMap(container_id,data){
	this.allele_clicked_listeners=[];
	this.loci_selected_listeners=[];
	this.container = $("#"+container_id);
	this.no_data=true;
	//height of both tree and map canvas
	this.map_height =100;
	this.map_width=1000;
	//width of each canvas 
	this.tree_width=250;
	this.meta_width=150;
	this.data={};
	this.original_data = null;
	this.original_loci = null;
	this.d3_xscale=null;
	//height container - controls both the tree and map container
	this.metdata=null;
	this.loci_selected=[];
	this.y_selected=0;
	this.mouse_dragging=false;
	this.x_scale=1;
	this.y_scale=1;
	
	this.max_distance = 0;
	

	this.allele_colours=null;
	this.colourPallete =d3.scale.category20().range().concat(d3.scale.category20b().range(),d3.scale.category20c().range());
	
	this.distance_scale=null;
	this.line_colours=null;
	
	
	this.strain_number=0;
	//calculate the tree
	
	this.tree_root = null;
	
	this.max_total_branch_length=0;
	this.xScale = null;
	
	
	this.terminal_nodes={};
	this.y_to_barcode={};
	this.nwk = null;
	
	
	
	
	this.header_height=0;
	this.locus_positions=null;
	this.x_to_locus={};
	

	
	this.tree_container = $("<div>").width(this.tree_width).css({"height":"100%","display":"inline-block","overflow":"hidden"});
	this.map_container = $("<div>").css({"overflow-y":"hidden","overflow":"hidden","height":"100%","display":"inline-block"});
	this.meta_container = $("<div>").css({"overflow":"hidden","height":"100%","display":"inline-block"});
	this.header_spacer= $("<div>").css({"overflow":"hidden","display":"inline-block"});
	this.header_container = $("<div>").css({"overflow":"hidden","display":"inline-block"});
	this.metaheader_container =$("<div>").css({"overflow":"hidden","display":"inline-block"});
	this.container.append(this.header_spacer).append(this.header_container).append(this.metaheader_container)
			      .append(this.tree_container).append(this.map_container).append(this.meta_container);
	
	
	
	this.tree_canvas = $("<canvas>").attr("id","chm-tree");
	this.tree_canvas.attr({"height":this.map_height+"px","width":this.tree_width+"px"});
	
	this.meta_canvas = $("<canvas>").attr("id","chm-meta");
	this.meta_canvas.attr({"height":this.map_height+"px","width":this.meta_width+"px"});
	
	this.map_canvas  = $("<canvas>").attr("id","chm-map");
	this.map_canvas.attr({"height":this.map_height+"px","width":+this.map_width+"px"});
	
	this.header_canvas= $("<canvas>").attr("id","chm-header")
	this.header_canvas.attr({"height":"50px","width":+this.map_width+"px"});	
	
	this.buffer_canvas  = $("<canvas>").attr("id","chm-buffer");
	this.buffer_canvas.attr({"height":this.height+"px","width":+this.map_width+"px"});
	
	
	
	this.tree_container.append(this.tree_canvas);
	this.map_container.append(this.map_canvas);
	this.meta_container.append(this.meta_canvas);
	this.header_container.append(this.header_canvas);
	
	
    	this.meta_context = this.meta_canvas[0].getContext("2d");
	this.tree_context = this.tree_canvas[0].getContext("2d");
	this.buffer_context = this.buffer_canvas[0].getContext("2d");
	this.map_context = this.map_canvas[0].getContext("2d");
	this.header_context=this.header_canvas[0].getContext("2d");
	
	
	
	
	var self = this;
	this.map_canvas[0].addEventListener('mousemove', function(evt) {
		self.mouseOver(evt);
	},false);
	this.map_canvas[0].addEventListener('mouseout', function(evt) {
		self.tooltip.hide();
		self.drag_start=null;
		self.mouse_dragged=false;
	},false);

	this.meta_container.scroll(function(event){
		var pos = $(this).scrollTop();
		self.tree_container.scrollTop(pos);
		self.map_container.scrollTop(pos);
	
	});
	this.map_container.scroll(function(event){
		var pos = $(this).scrollLeft();
		self.header_container.scrollLeft(pos);
		
	});
	
	this.header_container
	.mousedown(function (evt) {
		self.scroll_left_start = self.header_container.scrollLeft();
		self.scroll_top_start=self.header_container.scrollTop();
		self.drag_start= [evt.pageX, evt.pageY];
	})
       .mousemove(function (evt) {
	if (self.drag_start){
		self.mouse_dragging=true;
		s_x= self.drag_start[0];
		s_y=self.drag_start[1];
		if (!(evt.pageX === s_x && evt.pageY === s_y)) {
			var left = evt.pageX -s_x;		
			$(this).scrollLeft(self.scroll_left_start-left);
			self.map_container.scrollLeft(self.scroll_left_start-left);
			
		}
	}
    })
    .mouseup(function () {
	self.drag_start=null;
	self.mouse_dragging=false;
    });
	
	
	this.map_container
	.mousedown(function (evt) {
		self.scroll_left_start = self.map_container.scrollLeft();
		self.scroll_top_start=self.map_container.scrollTop();
		self.drag_start= [evt.pageX, evt.pageY];
		self.tooltip.hide();
	})
       .mousemove(function (evt) {
	if (self.drag_start){
		
		s_x= self.drag_start[0];
		s_y=self.drag_start[1];
		if (!(evt.pageX === s_x && evt.pageY === s_y)) {
			self.mouse_dragging=true;
			var left = evt.pageX -s_x;
			var top = evt.pageY -s_y;
			var dif = self.scroll_top_start-top;
			$(this).scrollTop(dif);
			$(this).scrollLeft(self.scroll_left_start-left);
			self.tree_container.scrollTop(dif);
			self.map_container.scrollTop(dif);
			self.meta_container.scrollTop(dif);
		}
	}
    })
    .mouseup(function (evt) {
	if (self.mouse_dragging){
		self.drag_start=null;
		self.mouse_dragging=false;
	}
	else{
		self.drag_start=null;
		self.mouse_dragging=false;
		self.mouseClicked(evt)
	}
    });







			   
	$(window).resize(function() {
		self.resizeContainer();
	});
	
	this.resizeContainer();
	this.tooltip = $("<div>");
	this.tooltip.css({
		position:"absolute",
		height:"60px",
		width:"180px",
		display:"none",
		"background-color":"yellow"
	});
	$('body').append(this.tooltip);
	
	this.buffer_context.save();
	this.tree_context.save();
	this.meta_context.save();
	this.header_context.save();
	this.container.hide();
	if (data){
		this.setData(data);
	}
	$(window).bind('mousewheel DOMMouseScroll', function(event){
		if (event.originalEvent.wheelDelta > 0 || event.originalEvent.detail < 0) {
			self.zoom(0.1);
		}
		else {
			self.zoom(-0.1);
		}
	});
}

CanvasHeatMap.prototype.scrollHorizontal=function(amount){
	var curr_pos = this.header_container.scrollLeft();
	this.header_container.scrollLeft(curr_pos+amount);
	curr_pos = this.map_container.scrollLeft();
	this.map_container.scrollLeft(curr_pos+amount);
}


CanvasHeatMap.prototype.getMatrix = function(){	
	var lines=[];
	var header = ["Strain"];
	for (var n=0;n<this.data['loci'].length;n++){
		header.push(this.data['loci'][n]);
	}
	lines.push(header.join("\t"));
	for (var key in this.data['data']){
		var name = key;
		var meta = this.metadata[key];
		if (meta){
			name = meta['strain'];
		}
		if (this.data.display_format === 'presence'){	
			lines.push(name+"\t"+this.data['data'][key].join("\t"));
		}
		else{
			var arr =[];
			for (var i in this.data['loci']){	
				var locus = this.data['loci'][i];
				arr.push(this.data['data'][key][locus]);
			}
			lines.push(name+"\t"+arr.join("\t"));
		}
	
	}
	return lines.join("\n");
}

CanvasHeatMap.prototype.calculateLocusPositions=function(max,contig_lengths){
	this.map_width=max/5;
	if (this.map_width<1000){
		this.map_width=1000;
	}
	if (this.map_width>25000){
		this.map_width=25000;
	}
	this.locus_positions={};
	this.contig_positions = {};
	this.max_distance=max;
	this.d3_xscale = d3.scale.linear().domain([1,this.max_distance]).range([1,this.map_width]);
	var offset = 0;
	
	var offset=0;
	var contig_offsets={};
	for (var contig in contig_lengths){
		contig_offsets[contig]=offset;
		this.contig_positions[contig]=Math.round(this.d3_xscale(offset));
	
		offset+= parseInt(contig_lengths[contig])
	}
	for (var locus in this.data['loci_info']){
		var info = this.data['loci_info'][locus];
		var contig = info['contig'];
		var st = parseInt(info['start']) + contig_offsets[contig];
		var en = parseInt(info['end'])+  contig_offsets[contig];
		var start =Math.round(this.d3_xscale(st));
		var end =Math.round(this.d3_xscale(en));		
		for (var n=start;n<end;n++){
			this.x_to_locus[n]=locus;
		}
		this.locus_positions[locus]=[start,end,info['direction']];		
	}
}

CanvasHeatMap.prototype.calculatePresenceAbsence=function(){
	var a = this.data['data'];

}


CanvasHeatMap.prototype.setMetadataLabel= function(category){
	this.metadata_label= category;
	this.drawMeta();

}




CanvasHeatMap.prototype.initialiseMapData=function(data){
	//if (!this.data['total_distance']){
	//	this.map_width=this.data['loci'].length;
	//}
	//else{
	//	this.map_width=1000;
	//}
	if (this.data['display_format']==='annotation'){
		//this.calculateAnnotationColour();	
		this.calculateColour3();
	}
}



CanvasHeatMap.prototype.initialiseData=function(data){
	this.colourPallete =d3.scale.category20().range().concat(d3.scale.category20b().range(),d3.scale.category20c().range());
	this.colourPallete[0]="white";
	
	
	this.data['loci_info']=data['loci_info'];
	this.data['display_format']=data['display_format'];
	this.data['id_to_colour']=data['id_to_colour'];
	this.data['data']=data['data'];
	this.x_to_locus=[];
	if (this.data['display_format']==='annotation'){
		this.calculateAnnotationColour();	
	}
	else if (this.data['total_distance']){
		this.max_distance=this.data['total_distance'];
		this.buffer_width=800;
		this.map_canvas.attr("width","800px");
		this.buffer_canvas.attr("width","800px");
		this.data['data']={};
		this.d3_xscale = d3.scale.linear().domain([1,this.max_distance]).range([1,this.map_width]);
		var positions = this.data['positions'];
		for (var barcode in positions){
			data_list =[];
			for (var n in positions[barcode]){
				var item = positions[barcode][n];
				var map_pos = Math.round(this.d3_xscale(item[1]));
				if (data_list[map_pos]){
					data_list[map_pos].push(item[1])
				}
				else{
					data_list[map_pos]=[item[1]]
				}
			}
		
			this.data['data'][barcode] = data_list;
		}
		if (this.data['type']==='presence'){
			this.allele_colours=null;
		}
	}
	else{
		this.original_data = {};
		for (var barcode in this.data['data']){
			this.original_data[barcode]=this.data['data'][barcode];
		} 
		
		this.original_loci = this.data['loci']
		if (this.data['display_format']==='frequency_based'){
			this.removeLowFrequency(97);
		}
		this.buffer_width=this.data['loci'].length;
		var map_width=this.data['loci'].length;
		this.map_canvas.attr("width",map_width+"px");
		this.buffer_canvas.attr("width",map_width+"px");
		this.allele_colours=null;
		this.line_colours=null;
		if (this.data['display_format']==='frequency_based'){
			this.removeLowFrequency(97);
			this.calculateColour(this.data['id_to_colour']);
			
		}
		else if (this.data['display_format']==='normal'){
			this.calculateColour2(this.data['id_to_colour']);
		}
		
	}
	
	
}

CanvasHeatMap.prototype.showCategory= function(category,show){
	this.data['gene_categories'][category]['show']=show;
	this.setXScale(this.x_scale);

}

CanvasHeatMap.prototype.setData= function(data){
	this.no_data=false;
	this.data['gene_categories']=null;
	if (data['gene_categories']){
		this.data['gene_categories']=data['gene_categories'];
		for (var cat in this.data['gene_categories']){
			this.data['gene_categories'][cat]['show']=false;
		
		}
	}
	this.data['display_format']=data['display_format'];
	if (data['contig_info']){
		this.data['total_distance']=data['contig_info']['total_distance'];
	}
	this.metadata= data['metadata'];
	this.container.show();
	this.tree_container.show();
	if(data['loci']){
		this.data['loci']=data['loci'];
		if (data['loci_info']){
			this.data['loci_info']=data['loci_info'];
			this.header_height=30;
			this.calculateLocusPositions(this.data['total_distance'],data['contig_info']['contig_lengths']);
		}
	}
	if (data['display_format']==='presence'){
		this.map_width=data['loci'].length;
	
	}
	this.metadata_label = data['metadata_label'];
	
	
	
	
	if (data['nwk']){
		this.tree_width=250;
		this.nwk = data['nwk'];
		this.calculateTree();
		this.tree_container.show();
		this.header_spacer.show();
		
		
	}
	
	else{
		this.tree_width=0;
		this.tree_container.hide();
		this.header_spacer.hide();
		this.nwk = null;
	}
	if(data['data']){
		this.meta_width=150;
		this.data['data']=data['data'];
		if (!this.nwk){
			this.processData();
		}
		this.header_container.css("overflow-x","hidden");
		//this.header_spacer.show();
		this.initialiseMapData(data);
		this.map_container.show();
		this.meta_container.show();
		this.buffer_canvas.show();
		this.metaheader_container.show();
		this.resizeContainer();
		var w =this.map_container.width();
		var h=this.map_container.height();
		if (h<200){
			h=200;
		}
		var y_scale=h/this.map_height;
		var x_scale = w/this.map_width;
		this.setXScale(x_scale);
		this.setYScale(y_scale);
		
		
	}
	else{
		this.data['data']=null;
		this.header_height=50;
	}
	if (! this.nwk && ! this.data['data']){
		this.meta_width=0;
		this.map_container.hide();
		this.tree_container.hide();
		this.meta_container.hide();
		this.header_spacer.hide();
		this.metaheader_container.hide();
		this.no_data=false;
		this.resizeContainer();
		var w =this.header_container.width();
		var x_scale = w/this.map_width;
		this.setXScale(x_scale);
		
		//this.drawHeader();
	}
	
	
	
	
	
	
	
}


CanvasHeatMap.prototype.changeData= function(data){
	
	this.initialiseData(data);
	this.setXScale(1.0);
	

}


CanvasHeatMap.prototype.resizeContainer= function(){
	if (this.no_data){
		return;
	}
	var height = this.container.parent().height();
	this.container.height(height);
	this.header_spacer.height(this.header_height);
	this.header_container.height(this.header_height);
	this.metaheader_container.height(this.header_height);
	var map_height = height-this.header_height;
	this.meta_container.height(map_height-20);
	this.map_container.height(map_height-20);
	this.tree_container.height(map_height-20);
	this._setWidth();
	
	
}

CanvasHeatMap.prototype._setWidth= function(){
	var self = this;
	//setTimeout(function(){
	self.header_spacer.width(self.tree_width);
	var t = self.tree_width;
	var m= self.meta_container.width();
	var all = self.container.width();
	var  map_width  = all-t-m;
	if (!self.data['data']){
		self.header_container.width(all);
		return;
	}
	
	var c_width = self.map_canvas.width();
	var available_map_width = all-t-self.meta_width;
	if (c_width<=available_map_width){
		self.map_container.width(c_width);
		self.header_container.width(c_width);
		self.header_spacer.width(self.tree_width);
		var m_width = all-t-c_width-10;
		self.meta_container.width(m_width);
		self.metaheader_container.width(m_width)
		//self.meta_canvas.attr("width",all-t-c_width-30+"px");
	}
	else{
		var map_width= all-t-self.meta_width;
		self.map_container.width(map_width);
		self.header_container.width(map_width);
		self.header_spacer.width(self.tree_width);
		var m_width = self.meta_width-10;
		self.meta_container.width(m_width);
		self.metaheader_container.width(m_width);
		//self.meta_canvas.attr("width",(self.meta_width-30)+"px");
	}
	//},10);

}
CanvasHeatMap.prototype.mouseClicked=function (evt){
	var rect = this.map_canvas[0].getBoundingClientRect();
	var x= evt.clientX - rect.left;
	var y= evt.clientY - rect.top;
	x=Math.round(x/this.x_scale);
	y=Math.round(y/this.y_scale);
	var barcode = this.y_to_barcode[y];
	var allele_id=0;
	var extra_info={};
	var locus="";
	if (!barcode){
		return;
	}
	
	
	
	if (this.data['display_format']=='presence'){
		var pos=x;
		var start =0;
		var end=0;
		var alleles= this.data['data'][barcode];
		allele_id = alleles[x];
		
		if (allele_id===0){
			return;
		}
		while(alleles[pos] && alleles[pos]>0){
			pos++;
		}
		var end=pos-1;
		pos=x;
		while(alleles[pos] && alleles[pos]>0){
			pos--;
		}
		var start =pos+1;
		this.highlightFeature(this.terminal_nodes[barcode],start,end);
		extra_info['selected_range']=[start,end];
		extra_info['alleles']={}
		for (var n=start;n<=end;n++){
			extra_info['alleles'][this.data['loci'][n]]=this.data['data'][barcode][n];
		
		}
		locus = this.data['loci'][x];
		
		
	
	}
	else{
		locus = this.x_to_locus[x];
		allele_id  = this.data['data'][barcode][locus];
		if (! allele_id){
			return;
		}
		extra_info['alleles']=this.data['data'][barcode];
		extra_info['locus_label']=  this.data['loci_info'][locus]['locus'];
		var arr = this.locus_positions[locus];	
		this.highlightFeature(this.terminal_nodes[barcode],arr[0],arr[1]);
	}
	
	this.map_context.drawImage(this.buffer_canvas[0],0,0);
	extra_info['metadata']=this.metadata[barcode];
	for (var index in this.allele_clicked_listeners){
		this.allele_clicked_listeners[index](locus,allele_id,barcode,extra_info);	
	}
	
	/*if (this.loci_selected.length>0){
		var p_start=this.loci_selected[0];
		var p_end = this.loci_selected[this.loci_selected.length-1];
		this.highlightFeature(this.y_selected,p_start,p_end,true);
	
	}
	this.loci_selected = [];
	for (var x=start;x<=end;x++){
		this.loci_selected.push(x)
	}
	var self =this;
	
	setTimeout(function(){
	
		for (var i in self.loci_selected_listeners){
			self.loci_selected_listeners[i](self.loci_selected);
		}
	},10);
	this.y_selected=y;
	*/
	
}

CanvasHeatMap.prototype.addAlleleClickedListener= function(callback){
	this.allele_clicked_listeners.push(callback);
}


CanvasHeatMap.prototype.addLociSelectedListener= function(callback){
	this.loci_selected_listeners.push(callback);
}



CanvasHeatMap.prototype.mouseOver= function(evt){
     
	var rect = this.map_canvas[0].getBoundingClientRect();
	var x= evt.clientX - rect.left;
	var y= evt.clientY - rect.top;
	x=Math.round(x/this.x_scale);
	y=Math.round(y/this.y_scale);
	var barcode = this.y_to_barcode[y];
	var locus = null;
	var allele=null;
	var metadata = this.metadata[barcode];
	var inf = "";
	if (metadata){
		info = metadata['strain'];
	}
	
	if (this['data'].display_format==='annotation'){
		locus = this.x_to_locus[x];
		allele=this.data['data'][barcode][locus];
		
	}
	else{
		locus = this.data['loci'][x];
		
		allele = this.data['data'][barcode][x];
	}
	if (! locus || !barcode){
		this.tooltip.hide();
		return;
	}
	 
	if (! allele){
		this.tooltip.hide();
		return;
	}
	
	this.tooltip.css({
		top:evt.pageY+3,
		left:evt.pageX+3
	});
	
	
	this.tooltip.html("Strain:"+info+"<br>Locus:"+locus+"<br>ID:"+allele);
	this.tooltip.show();
		
}


//tree functions
CanvasHeatMap.prototype.calculateX =function(len,node,max_len){
	var length = len;
	   
	var max_len=max_len;
	
	if (node.length){	
		length=length+node.length;
	}
	node.x_pos=length;
	if (length>this.max_total_branch_length){
		this.max_total_branch_length=length;
	}
	var children = node.children;
	if (children){
		for (var i in children){
			this.calculateX(length,children[i],max_len);
		}                  
	}
	//terminal node - strain
	else{
		this.strain_number++;
		
	}
};


CanvasHeatMap.prototype.processData  = function(){
	this.strain_number=0;
	for (var barcode in this.data['data']){
		this.strain_number++;
	}
	this.map_height=this.strain_number*4;
	this.terminal_nodes={};
	this.y_to_barcode={};
	var y_pos=4;
	this.row_height = Math.round(((this.map_height/this.strain_number)-1)/2);
	for (var barcode in this.data['data']){
		this.terminal_nodes[barcode]= y_pos;
		for (var n = y_pos-this.row_height;n<=y_pos+this.row_height;n++){
			this.y_to_barcode[n]=barcode;
				
		}
		y_pos+=4;
	}
	
}

CanvasHeatMap.prototype.calculateTree=function(){
	this.tree_root = this.parseNewick(this.nwk);
	this.max_total_branch_length=0;
	this.strain_number=0;
	//will calculate strain number 
	this.calculateX(0,this.tree_root);
	this.xScale = d3.scale.linear().domain([0,this.max_total_branch_length]).range([0,this.tree_width]);
	this.map_height=this.strain_number*4;
	this.tree_canvas.attr("height",this.map_height+"px");
	//reset
	this.terminal_nodes={};
	this.y_to_barcode={};
	this.row_height = Math.round(((this.map_height/this.strain_number)-1)/2);
	
	this.nodes =d3.layout.cluster()
			.separation(function(a,b){
				return 2;
				})
	.size([this.map_height,this.tree_width])
	.nodes(this.tree_root);
	for (var i in this.nodes){
		var node = this.nodes[i];
		node.y= Math.round(node.x);
		node.x=this.xScale(node.x_pos);
		if (! node.children){				
			this.terminal_nodes[node.name]=node.y;
			for (var n = node.y-this.row_height;n<=node.y+this.row_height;n++){
				this.y_to_barcode[n]=node.name;
				
			}
		}
	}
};

CanvasHeatMap.prototype.drawHeader = function(){
	this.header_context.clearRect(0, 0, this.header_canvas.width(),this.header_canvas.height());
	if (this.data.display_format=='presence'){
		return;
	}
	var y_pos = 15;
	var upper=10;
	var lower=10;
	var offset = 6/this.x_scale;
	this.header_context.beginPath();
	for (var locus in this.data['loci_info']){		
		var locus_info =  this.locus_positions[locus];
		
		if (locus_info[2] === '+'){
			
			this.header_context.moveTo(locus_info[0],y_pos-upper);
			this.header_context.lineTo(locus_info[1]-offset,y_pos-upper);
			this.header_context.lineTo(locus_info[1],y_pos);
			this.header_context.lineTo(locus_info[1]-offset,y_pos+lower);
			this.header_context.lineTo(locus_info[0],y_pos+lower);
			this.header_context.closePath();
			
		}
		else{
			
			this.header_context.moveTo(locus_info[1],y_pos-upper);
			this.header_context.lineTo(locus_info[0]+offset,y_pos-upper);
			this.header_context.lineTo(locus_info[0],y_pos);
			this.header_context.lineTo(locus_info[0]+offset,y_pos+lower);
			this.header_context.lineTo(locus_info[1],y_pos+lower);
			this.header_context.closePath();				
		}
		
	}
	this.header_context.fill();
	this.header_context.beginPath()
	for (var contig in this.contig_positions){
		var x = this.contig_positions[contig];
		if (x===0){
			continue;
		}
		this.header_context.moveTo(x,0);
		this.header_context.lineTo(x,30)
	
	
	}
	this.header_context.strokeStyle="red";
	this.header_context.stroke();
	var text_size = 14;
	this.header_context.font=text_size+"px Georgia";
	this.header_context.fillStyle= "white";
	this.header_context.textAlign= "center";
	console.log(this.x_scale);
	var  xscale = this.x_scale;
	
	
	xscale = 1*(1/xscale);
		
		
	
	
	this.header_context.restore();
	this.header_context.scale(xscale,1);
	for (var locus in this.data['loci_info']){
		var y_pos = 20;
		var locus_info =  this.locus_positions[locus];
		var label =  this.data['loci_info'][locus]['locus'];
		var l_width = (locus_info[1]-locus_info[0])/xscale;
		var tm = this.header_context.measureText(label);
		
		var pos = (locus_info[0]/xscale)+(l_width/2);
		if (tm.width<l_width){
			this.header_context.fillText(label,pos,y_pos);
		}
		else {
			tm =this.header_context.measureText(locus);
			if (tm.width<l_width){
				this.header_context.fillText(locus,pos,y_pos);
			}
		}
	}
	
				
}

CanvasHeatMap.prototype.highlightFeature=function(y_pos,x_start,x_end){
	this.drawHeatMap();
	var upper  = this.row_height;
	var lower = this.row_height;
	

	this.buffer_context.beginPath();
	
	for (var x_pos = x_start;x_pos<=x_end;x_pos++){
		this.buffer_context.moveTo(x_pos,y_pos-upper);
		this.buffer_context.lineTo(x_pos,y_pos+lower);
	}
	
	this.buffer_context.strokeStyle="red";		
	
	
	this.buffer_context.stroke();
/*	if(this.previous_selection){
		y_pos1= this.previous_selection[0];
		x_start1=this.previous_selection[1];
		x_end1=this.previous_selection[2];
		this.buffer_context.beginPath();
		for (var x_pos = x_start1;x_pos<=x_end1;x_pos++){
			this.buffer_context.moveTo(x_pos,y_pos1-upper);
			this.buffer_context.lineTo(x_pos,y_pos1+lower);
		}
		this.buffer_context.strokeStyle=this.previous_selection[3];	
		this.buffer_context.stroke();
	}
	this.previous_selection= [y_pos,x_start,x_end,hex];
	*/
	
}

CanvasHeatMap.prototype.drawMeta=function(){
	var text_size = Math.round(this.meta_canvas.height()/this.strain_number);
	this.meta_context.font=text_size+"px Georgia";
	for (var barcode in this.terminal_nodes){
		var label =barcode;
		if (this.metadata_label){
			var item = this.metadata[barcode];
			if (item){
				label = this.metadata[barcode][this.metadata_label];
				if (! label){
					continue;
				}
			}
			else{
				continue;
			}
			
		}
		
		var y_pos = Math.round((this.terminal_nodes[barcode] +1)* this.y_scale);
		this.meta_context.fillText(label,10,y_pos);
	}
}
	





CanvasHeatMap.prototype.drawHeatMap=function(){
	var upper  = this.row_height;
	var lower = this.row_height;
	this.buffer_context.clearRect(0, 0, this.buffer_canvas.width(),this.buffer_canvas.height());
	
	this.map_canvas.css("background-color","white");
	
	
	//draw all the same colour at once, with the most frequent being background colour
	if ( this.allele_colours){
		this.map_canvas.css("background-color","gray");
		for (var n in this.allele_colours){
			this.buffer_context.beginPath();
			for (var barcode in this.allele_colours[n]){
				var arr = this.allele_colours[n][barcode];
				var y_pos = this.terminal_nodes[barcode];
				for (var index in arr ){
					var x_pos = arr[index];
					this.buffer_context.moveTo(x_pos,y_pos-upper);
					this.buffer_context.lineTo(x_pos,y_pos+lower);
				}
			}
			this.buffer_context.strokeStyle=this.colourPallete[n];
			this.buffer_context.stroke();
		}	
	}
	/*else if (this.line_colours){
		for (var n in this.line_colours){
			this.buffer_context.beginPath();
			for (var index in this.line_colours[n]){
				var line_info = this.line_colours[n][index];							
				this.buffer_context.moveTo(line_info[0],line_info[1]);
				this.buffer_context.lineTo(line_info[0],line_info[2]);				
			}
			this.buffer_context.strokeStyle=this.colourPallete[n];
			this.buffer_context.stroke();
		}
	
	}*/
	else if (this.data['display_format']==='annotation'){
		/*lower=lower-1;
		upper=upper-1;
		for (var index in this.locus_colour){
			this.buffer_context.beginPath();
			for (var index2 in this.locus_colour[index]){
				var y_pos = this.terminal_nodes[barcode];
				
				var locus = this.locus_colour[index][index2][1];
				var barcode= this.locus_colour[index][index2][0]
				var locus_info =  this.locus_positions[locus];
				var y_pos = this.terminal_nodes[barcode];
				if (locus_info[2] === '+'){
					
					this.buffer_context.moveTo(locus_info[0],y_pos-upper);
					this.buffer_context.lineTo(locus_info[1]-3,y_pos-upper);
					this.buffer_context.lineTo(locus_info[1],y_pos);
					this.buffer_context.lineTo(locus_info[1]-3,y_pos+lower);
					this.buffer_context.lineTo(locus_info[0],y_pos+lower);
					this.buffer_context.closePath();
					
				}
				else{
					
					this.buffer_context.moveTo(locus_info[1],y_pos-upper);
					this.buffer_context.lineTo(locus_info[0]+3,y_pos-upper);
					this.buffer_context.lineTo(locus_info[0],y_pos);
					this.buffer_context.lineTo(locus_info[0]+3,y_pos+lower);
					this.buffer_context.lineTo(locus_info[1],y_pos+lower);
					this.buffer_context.closePath();				
				}
				
			
			}
			this.buffer_context.fillStyle=this.colourPallete[parseInt(index)+1];
			this.buffer_context.fill();
			
		}*/
		for (var index in this.line_colours){
			this.buffer_context.beginPath();
			for (var index2 in this.line_colours[index]){
				var arr = this.line_colours[index][index2]
				this.buffer_context.rect(arr[0],arr[1],arr[2],arr[3]);
			}
			this.buffer_context.fillStyle=this.colourPallete[index];
			this.buffer_context.fill();		
		}
	}
	else{
		this.buffer_context.beginPath();
		for (var barcode in this.terminal_nodes){
			var alleles= this.data['data'][barcode];
			var y_pos = this.terminal_nodes[barcode];
			if (!alleles){
				continue;
			}		
			for (var t in alleles){
				var val = alleles[t];
				var x_pos = t;
				if (val>0){	
					this.buffer_context.moveTo(x_pos,y_pos-upper);
					this.buffer_context.lineTo(x_pos,y_pos+lower);
				}
			}
		}
		this.buffer_context.strokeStyle="black";
		this.buffer_context.stroke();
	
		if (this.data['gene_categories']){
			var width = (2/this.x_scale);
			if (this.x_scale>1.5){
				width=1;
			}
			
			if (width>10){
				width=10;				
			}
			console.log(width+":"+this.x_scale);
			for (var cat in this.data['gene_categories']){
				if (!this.data['gene_categories'][cat]['show']){
					continue;
				}
				this.buffer_context.beginPath();
				var loci = this.data['gene_categories'][cat]['loci'];
				for (var index in loci){
					var x = loci[index];
					for (var barcode in this.terminal_nodes){
						var alleles= this.data['data'][barcode];
						var y_pos = this.terminal_nodes[barcode];
						if (!alleles){
							continue;
						}
						if (alleles[x]!==0){
							this.buffer_context.rect(x-width/2,y_pos-lower,width,this.row_height*2);
							//this.buffer_context.moveTo(x-2,y_pos-upper);
							//this.buffer_context.lineTo(x+2,y_pos+lower);
						}	
					}
								
				}
				this.buffer_context.fillStyle=this.data['gene_categories'][cat]['color'];;
				this.buffer_context.fill();	
			}
			
		}	
	}
	
	//var image = this.buffer_context.getImageData(0,0,this.map_canvas.width(),this.map_canvas.height());
	this.map_context.clearRect(0,0,this.map_canvas.width(),this.map_canvas.height());
	this.map_context.drawImage(this.buffer_canvas[0],0,0);
};



CanvasHeatMap.prototype.drawTree=function(){
	var self = this;
	this.tree_context.clearRect(0, 0, this.tree_canvas.width(), this.tree_canvas.height());	
	this.tree_context.beginPath();
	for (var i in  this.nodes){
		var d = this.nodes[i];
		var p=d.parent;
		if (!p){
			p=d;
		}
		this.tree_context.moveTo(d.x,d.y);
		this.tree_context.lineTo(p.x,d.y);
		this.tree_context.lineTo(p.x,p.y);
		if (! d.children){
			this.tree_context.moveTo(d.x,d.y);
			this.tree_context.lineTo(this.tree_width,d.y);
		}
		
	}
	this.tree_context.stroke();
	//this.drawHeatMap();
};

	
CanvasHeatMap.prototype.drawAll= function(x,y){
	if (y && this.nwk){
		this.drawTree();
	}
	if (this.data['data']){
		this.drawHeatMap();
	}
	if (x && this.data['loci_info']){
		this.drawHeader();
	}
	if (y){
		this.drawMeta();
	}
	this._setWidth();
	
}	


CanvasHeatMap.prototype.setXScale= function(x_scale,relative){
	if (relative){
		this.x_scale=this.x_scale*x_scale
	}
	else{
		this.x_scale = x_scale;
	}
	this.buffer_context.restore();
	this.header_context.restore();
	this.map_canvas.attr("width",(this.map_width*this.x_scale)+"px");
	this.buffer_canvas.attr("width",(this.map_width*this.x_scale)+"px");
	this.header_canvas.attr("width",(this.map_width*this.x_scale)+"px");
	this.buffer_context.scale(this.x_scale,this.y_scale);
	this.header_context.scale(this.x_scale,1.0);
	this.drawAll(true,false)
};

CanvasHeatMap.prototype.zoom= function(amount){
	this.x_scale+=this.x_scale*amount;
	this.y_scale+=this.y_scale*amount;
	this.buffer_context.restore();
	this.tree_context.restore();
	this.meta_context.restore();
	this.header_context.restore();
	//this.map_context.restore();
	this.map_canvas.attr("width",(this.map_width*this.x_scale)+"px");
	this.buffer_canvas.attr("width",(this.map_width*this.x_scale)+"px");
	this.header_canvas.attr("width",(this.map_width*this.x_scale)+"px");
	this.map_canvas.attr("height",(this.map_height*this.y_scale)+"px");
	this.buffer_canvas.attr("height",(this.map_height*this.y_scale)+"px");
	this.tree_canvas.attr("height",(this.map_height*this.y_scale)+"px");
	this.meta_canvas.attr("height",(this.map_height*this.y_scale)+"px");
	this.buffer_context.scale(this.x_scale,this.y_scale);
	//this.map_context.scale(this.x_scale,this.y_scale);
	this.header_context.scale(this.x_scale,1.0);	
	this.tree_context.scale(1.0,this.y_scale);
	//this.meta_context.scale(1.0,this.y_scale);
	this.drawAll(true,true);
	this.resizeContainer();

}

CanvasHeatMap.prototype.setYScale= function(y_scale,relative){ 
	if (relative){
		this.y_scale=this.y_scale*y_scale;
	}
	else{
		this.y_scale = y_scale;
	}
	this.buffer_context.restore();
	this.tree_context.restore();
	this.meta_context.restore();
	this.map_canvas.attr("height",(this.map_height*this.y_scale)+"px");
	this.buffer_canvas.attr("height",(this.map_height*this.y_scale)+"px");
	this.tree_canvas.attr("height",(this.map_height*this.y_scale)+"px");
	this.meta_canvas.attr("height",(this.map_height*this.y_scale)+"px");
	this.buffer_context.scale(this.x_scale,this.y_scale);
	this.tree_context.scale(1.0,this.y_scale);
	//this.meta_context.scale(1.0,y_scale);
	this.drawAll(false,true);
};

CanvasHeatMap.prototype.removeLowFrequency= function(max_percent){
	var mc = this.data['missing']?this.data['missing']:0
	//clear the data
	this.data['loci']=[];
	for (var barcode in this.data['data']){
		this.data['data'][barcode]=[];
	
	}
	//rebuiid it 
	for (var n=0;n<this.original_loci.length;n++){
		var breakdown= this._getAlleleBreakdown(n);
		if (parseInt(breakdown[0][0]) <= 0){
			continue;
		}
		
		var number = breakdown[0][1];
		var percent =number*100/this.strain_number;
		//if (percent < max_percent){
			//if (breakdown.length<4){
			this.data['loci'].push(this.original_loci[n]);
			for (var barcode in this.data['data']){
				this.data['data'][barcode].push(this.original_data[barcode][n])
			}
			//}
		//}
	}
}


CanvasHeatMap.prototype._getAlleleBreakdown= function(pos){
	var allele_freq={};
	
	for (var barcode in this.original_data){
		var allele_id = this.original_data[barcode][pos];
		if (! allele_freq[allele_id]){
			allele_freq[allele_id]=[barcode];
			
		}
		else{	
			allele_freq[allele_id].push(barcode);
		}	
	}
	
	allele_list = [];
	for (var allele_id in allele_freq){
		allele_list.push([allele_id,allele_freq[allele_id].length]);
	}
	allele_list.sort(function (a,b){
		return b[1]-a[1];	
	});
	return allele_list;


}

CanvasHeatMap.prototype.orderByFrequency= function(){
	var pos_to_freq=[];
	for (var n=0;n<this.data['loci'].length;n++){
		pos_to_freq[n]=[n,0];
		for (var barcode in this.data['data']){
			var allele_id = this.data['data'][barcode][n];
			if (allele_id){
				pos_to_freq[n][1]++;
			}
			
		}
		pos_to_freq.sort(function (a,b){
			return b[1]-a[1];	
		});
	}
	
	for (var barcode in this.data['data']){
		var temp=[];
		var old_array = this.data['data'][barcode];
		for (var n in pos_to_freq){
			temp.push(old_array[pos_to_freq[n][0]]);
		}
		this.data['data'][barcode]=temp;
	}
	var temp_loci = [];
	var old_loci = this.data['loci'];
	for (var n in pos_to_freq){
		temp_loci.push(old_loci[pos_to_freq[n][0]]);
	}
	this.data['loci']=temp_loci;
};


CanvasHeatMap.prototype.calculateAnnotationColour=function(){
	this.locus_colour=[];
	for (var n=0;n<this.data['loci'].length;n++){
		var locus = this.data['loci'][n];
		var allele_count={}
		var allele_to_barcode={}
		for (var barcode in this.terminal_nodes){
			var dat = this.data['data'][barcode];
			if (!dat){
				continue;
			}
			var allele = dat[locus];
			if (!allele){
				continue;
			}
			var barcode_list = allele_to_barcode[allele];
			if (!barcode_list){
				barcode_list=[];
				allele_to_barcode[allele]=barcode_list;
			}
			barcode_list.push(barcode);
			
			if (!allele_count[allele]){
				allele_count[allele]=1;
			}
			else{
				allele_count[allele]++;
			}
			
		}
		var sort_list=[];
		for (var allele in allele_count){
			sort_list.push([allele,allele_count[allele]]);
			
		}
		sort_list.sort(function(a,b){
			return b[1]-a[1];		
		});
		for (var index in sort_list){
			if (!this.locus_colour[index]){
				this.locus_colour[index]=[];
			}
			var barcode_list = allele_to_barcode[sort_list[index][0]];
			for(var i in barcode_list){
				this.locus_colour[index].push([barcode_list[i],locus])
			}
			
		}
	}
	
}


CanvasHeatMap.prototype.calculateColour2=function(value_to_colour){
	this.line_colours=[];
	this.colourPallete = [];
	var index =0;
	var allele_id_to_colour = {};
	for (var val in value_to_colour){
		this.colourPallete[index]=value_to_colour[val];
		allele_id_to_colour[val]=index;
		this.line_colours[index]=[];
		index++;		
	}
	for (var n=0;n<this.data['loci'].length;n++){
		var line_info=null;
		var prev_info=null;
		var barcode;
		var first = true;
		for (barcode in this.terminal_nodes){
			var allele_list = this.data['data'][barcode];
			if (!allele_list){
				continue;			
			}
			var allele_id =allele_list[n];
			if (first){
				first =false;
				line_info=[n,this.terminal_nodes[barcode]-this.row_height];
				prev_info = [allele_id,barcode];
				continue;
			}
			if (allele_id !==prev_info[0]){
				
					line_info.push(this.terminal_nodes[prev_info[1]]+this.row_height);
					this.line_colours[allele_id_to_colour[prev_info[0]]].push(line_info);
					line_info=[n,this.terminal_nodes[barcode]-this.row_height];	
			}
			prev_info = [allele_id,barcode];
		
		}
		line_info.push(this.terminal_nodes[prev_info[1]]+this.row_height);
		this.line_colours[allele_id_to_colour[prev_info[0]]].push(line_info);
	}


}


CanvasHeatMap.prototype.calculateColour3=function(){
	this.line_colours=[];
	var index =0;
	this.line_colours[0]=[];
	
	var allele_id_to_colour = {};
	
	for (var n=0;n<this.data['loci'].length;n++){
		var line_info=null;
		var prev_info=null;
		var first = true;
	
		index=0;
		var allele_to_index={};
		var locus = this.data['loci'][n];
		var l_start = this.locus_positions[locus][0]+1;
		var l_end = this.locus_positions[locus][1]-1;
		var width = l_end-l_start;
		var allele_id = null;
		for (var barcode in this.terminal_nodes){
	
			var alleles = this.data['data'][barcode];
			if (!alleles){
				continue;			
			}
			allele_id =alleles[locus];
			if (first){
				first =false;
				if (allele_id){
					line_info=[l_start,this.terminal_nodes[barcode]-this.row_height,width];
					
					allele_to_index[allele_id]=index;
				}
				prev_info = [allele_id,barcode];
				continue;
			}
			if (allele_id !==prev_info[0]){
				
				if (prev_info[0]){
					var ind = allele_to_index[prev_info[0]];
					if (! ind && ind !==0){
						index++;
						if (!this.line_colours[index]){
							this.line_colours[index]=[];
						}
						ind=index
						allele_to_index[prev_info[0]]=index;
					}
					var height = (this.terminal_nodes[prev_info[1]]+this.row_height)-line_info[1];
					line_info.push(height);
					this.line_colours[ind].push(line_info);
				}
				if (allele_id){
					line_info=[l_start,this.terminal_nodes[barcode]-this.row_height,width];
				}
				
			}
			prev_info = [allele_id,barcode];		
		}
		if (allele_id){		
			var ind = allele_to_index[allele_id];
			if (! ind && ind !==0){
				index++;
				if (!this.line_colours[index]){
					this.line_colours[index]=[];
				}
				ind=index;
		
			}		
			var height = (this.terminal_nodes[prev_info[1]]+this.row_height)-line_info[1];
			line_info.push(height);
			this.line_colours[ind].push(line_info);
		}
	}
}



CanvasHeatMap.prototype.calculateColour= function(value_to_colour){
	this.allele_colours=[];
	this.allele_colours[0]={};
	var allele_id_to_colour_index={};
	var mc = this.data['missing']?this.data['missing']:0
	if (value_to_colour){
		this.colourPallete = [];
		var index =1;
		this.colourPallete[0]="white"
		for (var val in value_to_colour){
			this.colourPallete[index]=value_to_colour[val];
			allele_id_to_colour_index[val]=index;
			this.allele_colours[index]={};
			index++;		
		}	
	}		
	for (var n=0;n<this.data['loci'].length;n++){
		var allele_freq={};
		for (var barcode in this.data['data']){
			var allele_id = this.data['data'][barcode][n];
			if (! allele_freq[allele_id]){
				allele_freq[allele_id]=[barcode];
				
			}
			else{	
				allele_freq[allele_id].push(barcode);
			}	
		}
		if (allele_freq[mc]){
			var barcodes = allele_freq[mc];
			for (index in barcodes){
				var barcode= barcodes[index];
				if (!this.allele_colours[0][barcode]){
					this.allele_colours[0][barcode]=[n]				
				}
				this.allele_colours[0][barcode].push(n);
			}
			delete allele_freq[mc];
		
		}
		allele_list = [];
		for (var allele_id in allele_freq){
			allele_list.push([allele_id,allele_freq[allele_id]]);
		}
		allele_list.sort(function (a,b){
			return b[1].length-a[1].length;	
		});
		//list of [id,barcode]
		if (allele_list.length >1){
			for (var i=1;i< allele_list.length;i++){
				if  (! this.allele_colours[i]){
					this.allele_colours[i]={};
				}
			}
			for (var i=1;i< allele_list.length;i++){
				var colour_index=i;
				if (value_to_colour){
					colour_index= allele_id_to_colour_index[allele_list[i][0]]
				}
				var barcodes = allele_list[i][1];
				for (index in barcodes){
					var barcode= barcodes[index];
					if (!this.allele_colours[colour_index][barcode]){
						this.allele_colours[colour_index][barcode]=[n]				
					}
					this.allele_colours[colour_index][barcode].push(n);
				}
			}
		}
	}	
};



CanvasHeatMap.prototype.parseNewick =  function (a){
	for(var e=[],r={},s=a.split(/\s*(;|\(|\)|,|:)\s*/),t=0;t<s.length;t++){
		var n=s[t];
		switch(n){
			case"(":var c={};r.children=[c],e.push(r),r=c;break;
			case",":var c={};e[e.length-1].children.push(c),r=c;break;
			case")":r=e.pop();break;case":":break;
			default:var h=s[t-1];")"===h||"("===h||","===h?r.name=n:":"===h&&(r.length=parseFloat(n));
		}
	}
	return r;
};
