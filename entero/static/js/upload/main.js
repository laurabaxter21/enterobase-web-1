/*
 * jQuery File Upload Plugin JS Example 8.9.1
 * https://github.com/blueimp/jQuery-File-Upload
 *
 * Copyright 2010, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/MIT
 */

/* global $, window */



 function is_fastq(file,callback) {
   try{
            var reader = new FileReader();
            reader.readAsArrayBuffer(file.slice(0,1000));
            reader.onload = function(e){
            try{
            var result= pako.inflate(reader.result,{to:'string'});
            var lines = result.split("\n");
            if (lines[0].substring(0,1) === '@' && lines[2].substring(0,1)=='+'){
                        callback(file,true);
            }
            else{
                        callback(file,false);
            }
            }catch(e){
            callback(file,false);
            }
           
        }
            }catch(e){
             callback(file,false);
            
            }
    }