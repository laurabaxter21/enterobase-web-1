SchemeGrid.prototype = Object.create(BaseSchemeGrid.prototype);
SchemeGrid.prototype.constructor= SchemeGrid;



//create new grid giving the name of the species and the scheme
function SchemeGrid(name,config,species,scheme,scheme_name){
	
	BaseSchemeGrid.call(this,name,config,species,scheme,scheme_name);
	
};




SchemeGrid.prototype.getProfilesForPhyloviz = function(callback,input,onlySelected){
	var stColIndex = this.getColumnIndex('st');     
	if (true){
		var barcode_to_ST ={};
		var barcodes=[];
		var self = this;
		//get the barcodes and link to st
		for (var rowIndex=0;rowIndex<this.getRowCount();rowIndex++){
				var id =  this.getRowId(rowIndex);
				if (onlySelected && ! this.selectedRows[id]){
						continue;                
				}            
				var st = this.getValueAt(rowIndex,stColIndex);
				var barcode = this.extraRowInfo[id];
				if (! barcode){
					continue;
				}
				barcode_to_ST[barcode]=st;
				
		}
		for (var barcode in barcode_to_ST){
			barcodes.push(barcode);
		}
		
			input.barcodes=barcodes;
			input.barcode_to_ST=barcode_to_ST;
			callback(input);
			return;
		
		}
		else{
 
	var sts = {};
	var profiles = [];      
	for (var rowIndex=0;rowIndex<this.getRowCount();rowIndex++){
		var id =  this.getRowId(rowIndex);
		if (onlySelected && ! this.selectedRows[id]){
			continue;                
		}              
		var st = this.getValueAt(rowIndex,stColIndex);
		if (!st){
			continue;
		}
		if (! sts[st]){
			var profile = {"ID":"ST"+st};
			for (var index2 in this.loci){
				var lociName = this.loci[index2];
				var alColIndex = this.getColumnIndex(lociName);
				profile[lociName]= this.getValueAt(rowIndex,alColIndex);		
			}
			sts[st]=true;
			profiles.push(profile);                                               
		}          
	}
	input.profiles=profiles; 
	callback(input);
	}
}









