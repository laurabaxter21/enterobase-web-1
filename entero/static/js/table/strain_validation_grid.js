/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//Extend validation grid
StrainValidationGrid.prototype = Object.create(ValidationGrid.prototype);
StrainValidationGrid.prototype.constructor= StrainValidationGrid;

DescriptionsForColumns = {
Accession:"",
Location:"The location where the strain was isolated. The information is based on \
	  the http://www.geonames.org. There are five levels ranging from continent to \
	  city (any built up area )" ,
Source:"",
"Collection Date":""
}

/**
 * Represents a Grid containing the metatdata describing a strain
 * @constructor
 * @extends ValidationGrid
 * @param {string} name - The name of the grid
 * @param {object} config - Configuration options e.g.
 * <pre><code>{enableSort: true, 
 *		   editmode: "static",
 * editorzoneid: "edition",allowSimultaneousEdition: false,
 * pageSize:25}</code></pre>
 * @param {string} species - The name of the database associated with this table
 */
function StrainValidationGrid(name,config,species){
	this.google_to_geo={ locality:"city",
						"administrative_area_level_2":"admin2",
						"administrative_area_level_1":"admin1",
						country:"country"
	
	}
    this.user_upload=false;
    this.species=species;
    this.countries = {};
    var self = this;
    this.submitButtonName = "";
    this.checkDuplicates=true;
    this.ignoreList = ["antibiotic_resistance"];
    this.map=null;
    //involved in submitting data
    this.records_submitted=0;
    this.bad=0;
    this.infoList=[];
    this.total_records=0;
    this.finishedUploadCallback = function(){};
    /**
     * If true location will be checked upon  a call to  validateCell for Location
     * otherwise the location will be added to {@link StrainValidationGrid#allLocations|allLocations}
     */
     this.checkLocation=true;
     
    this.uploadURI="/api/v1.0/strains";
    /**
     *stores valid locations, so they do not have to be looked up twice on geonames
     * e.g. validLocations['Europe,United Kingdom,England,,,'] = true
     */
    this.validLocations = {};
      /**
     * stores all the locations (dereplicated) after a parse of the data linked to
     * a list of row IDs
     * e.g. allLocations['Europe,United Kingdom,England,,,'] = [1,27,34,55,99]
     */
    this.allLocations = {};
    this.callsToGeonames=0;
    this.paginator = $("#paginator");
    this.submitDataFunction=function(){};
    ValidationGrid.call(this,name,config);
    //preload all the country information
    Enterobase.call_restful_json("/get_country_info","GET","",function(msg){Enterobase.modalAlert(msg);})
	.done(function (data) {
	    self.countries=data
		 
    });
    
    this.addCustomCellValidator("Accession",function(colName,rowID){
	if (!this.validateCompoundField(colName,rowID)){
            var msg = "One or more values in this field are incorrect";
	    return this.updateErrors(colName,rowID,msg);
        }
	this.updateErrors(colName,rowID,"");
	if (self.checkDuplicates && self.editMode){
	    self.duplicateRecord();	
	}
    });
    
    this.addRowRemovedListener(function(rowID){
        var rowIndex = this.getRowIndex(rowID);
		var colIndex = this.getColumnIndex("Accession");
        var val1 = this.getValueAt(rowIndex,colIndex);  
        if (val1){
             this.duplicateRecord(rowID);
        }
    });
    
    this.addExtraRenderer('antibiotic_resistance',function(cell,value,rowID,col_name){
		cell.style.backgroundColor='gray';
		$(cell).click(function(e){
		    e.stopPropagation();
		    Enterobase.modalAlert("This feature has not yet been implemented","Information",false);
		});
    });
	//change the entry of dates to something more friendly
	//get human readable value to display
	this.comp_values.setCustomDisplayValue("Collection Date",function(lists){
		var dateText="";
		var list = lists[0];
		if (list[2]){dateText+=list[2]+"/"+list[1]+"/"+list[0]}
		else if (list[1]){dateText+=list[1]+"/"+list[0]}
		else if (list[0]){dateText+=list[0]}
		return dateText;
	
	});
	
	//entry via datepicker or directly type into text
	this.addCustomColumnHandler("Collection Date",function(cell,rowIndex,rowID,e){
		if (!self.editMode || self.nonEditableRows[rowID]){
			return;
		}
		if ($(cell).data("edit")){
			return;
		}
        var colName = "Collection Date";
		var columnIndex= self.getColumnIndex(colName);
		var target = e.target || e.srcElement;
		//if icon clicked open the calendar
		if ($(target).hasClass('glyphicon glyphicon-calendar')){
			var input = $("<input>");
			//don't want to see the text box
			$(cell).prepend(input.hide());
			input.datepicker({
				changeMonth: true,
				changeYear: true,
				dateFormat:"dd/mm/yy",
				yearRange: "1800:2020",
				onClose:function(dateText){
					//put date in the grid
					if (!dateText){
						return;
					}
					var dateList  = Enterobase.getDateAsList(dateText);					
					oldVals = self.comp_values.setRow(colName,rowID,dateList);
					self.compoundValueChanged(colName,rowID,1,null,oldVals,dateList)
					self.updateCompoundCell(rowIndex,columnIndex);
					input.remove();
				}		
			}).datepicker("show");	
		}
		//normal cell editing click cell outside icon
		else{
			var column = self.columns[columnIndex];                      
			var c = $(cell);
			c.data("edit",true);
			c.children().hide();
			c.html("");
			var input = $("<input type='text'>");
			input.val(self.comp_values.getDisplayValue(colName,rowID));
			input.blur(function(e){
				var dateList = Enterobase.getDateAsList($(this).val());
				oldVals = self.comp_values.setRow(colName,rowID,dateList);
				self.compoundValueChanged(colName,rowID,1,null,oldVals,dateList)
				self.updateCompoundCell(rowIndex,columnIndex);
				$(this).remove();
				c.data("edit",false);
				c.children().show();
				}
						
			)
			
			.on("keyup",function(event){
				if(event.keyCode===13){
					$(this).blur();
				}
			});
			
			c.append(input);
			input.focus();
			
		}
	});
	//render both date and icon
	this.addExtraRenderer("Collection Date",function(cell,value,rowID,col_name){
		var c = $(cell);
		//assign value
		c.html(value);
		//append icon if in edit mode
		if (self.editMode){
			c.prepend($("<span class='glyphicon glyphicon-calendar' style='padding:0px;margin:0px;display:inline-block;float:right;font-size:+"+(self.fontSize)+"px'></span>"));
	    }
	});
	//validate 
	this.addCustomCellValidator("Collection Date",function(colName,rowID,value){
		var valid= true;
		var msg ="";
		var row = self.comp_values.getRow(colName,rowID,1);
		if (!row){
			return self.updateErrors(colName,rowID,msg);
		}
		for (var n=0;n<3;n++){
			if (row[n] && isNaN(row[n])){
				valid=false;
				break;
			}
		}		
		if (row[0] && (row[0]<1000 || row[0]>2030)){
			valid =false;
		}
		if (row[1] && (row[1]<1|| row[1]>12)){
			valid=false;
		}
		if (row[2] && (row[2]<1|| row[2]>31)){
			valid=false;
		}
		
		if (!valid){
			msg = "Date needs to be in the format DD/MM/YYYY or MM/YYYY or YYYY";
		}
		return self.updateErrors(colName,rowID,msg);
	
	});
	/*
	//custom file parser to write date in one column and splitting it when reading it in
	this.setCustomFileParser("Collection Date",
		function(rowIndex,colIndex){
			return self.getValueAt(rowIndex,colIndex);
		},
		function(text,columnName,rowIndex){
			var dateList = Enterobase.getDateAsList(text);
			self.comp_values.setRow(columnName,rowIndex,dateList);
			return text;
	});
	*/
	//add the map to the default location popup
	this.addExtraPopupRenderer("Location",function(div,columnName,rowID){
	    //add the div composing the map to the popup
		var extradiv = $("<div>").css({
					"float":"left"}
					).attr("id","entero-google-map").width(400).height(300);
		
		div.append(extradiv);
		//get existing location values
		var list = self.comp_values.getRow(columnName,rowID,1);
		var lat= 0;
		var lng = 0;
		var zoom =7;
		var setMap = false;
		//check whether latitude and longitue exist - if so
		//map location can be set on creation
		if(list){
		/*
			if (list[6] && list [7]){
			   lat=list[6];
			   lng=list[7];
			   zoom=10;
			}
			//If there is place data (i.e. at least continent) map can be set
			else if (list[0]){
				setMap =true;
			}	
			
		*/
			//changed at 31/01/2019, yulei
			if(list.length>7){
				if( list[6]==="" || list[6] ==0 || list[7]==="" || list[7] ==0){
					if (list[0]){
						setMap =true;
					}
				}else{
					lat=list[6];
					lng=list[7];
				}
			}else{
				if (list[0]){
					setMap =true;
				}			
			}
			
		}
		//set up the map
		self.map = new GMaps({
			el: '#entero-google-map',
			lat: lat,
			lng: lng,
			zoom:zoom,
		    //store details on the location clicked
			click:function(e){
			   //can only change location in edit mode
			   if (!self.editMode || self.nonEditableRows[rowID]){
				   return;
			   }
			   self.map.removeMarkers();
				self.map.addMarker({
					lat: e.latLng.lat(),
					lng: e.latLng.lng(),
					title: 'new coordinates',
				});
			   $("#latitude").val(e.latLng.lat());
			   $("#longitude").val(e.latLng.lng());
			   //find the nearby place names and update
			   //grid accordingly
			   jeoquery.getGeoNames('findNearbyPlaceName', {
				   style: "FULL",
				   maxRows: 1,
				   lat: e.latLng.lat(),
				   lng:e.latLng.lng(),
				   radius:3
			   }).done(function(data){
					//no nearby place information - just store lat,lng
					if (data['geonames'].length === 0){
					   $("#country").val("");
					   $("#admin1").val("");
					   $("#admin2").val("");
					   $("#city").val("");
					   $("#continent").val("");
					   return;
					}
					//update the text boxes
					var rec = data['geonames'][0];
					var country = rec.countryName;
					$("#country").val(country);
					$("#admin1").val(rec.adminName1);
					$("#admin2").val(rec.adminName2);
					$("#city").val(rec.name);
					$("#continent").val(self.countries[country]);
				});
			}
		});	
		if (lat != 0 && lng != 0) {
			self.map.addMarker({
				lat: lat,
				lng: lng,
				title: lat + ' ' + lng,
			});
		}
		if (setMap){
			self.setMapLocation(rowID);		
		}			
	});
	
	
	
	this.comp_values.setCustomDisplayValue("Location",function(list){
		for (var n=4;n>=0;n--){
			if (list[0][n]){
				return list[0][n];
			}
			
		}
		return "";
	});
	
	
}

/**
 *Sends a message to a modal dilog box see {@link Enterobase.modalAlert}
 *@overide
 *@param {string} msg - The message to be displayed
 *@param {string} title - The title of the message
 */
StrainValidationGrid.prototype.displayMessage = function (msg,title){
    Enterobase.modalAlert(msg,title,false);
}; 

/**
 *Sets the maps locatiom according to the location details in the RowID supplied
 *@param {integer} rowID - The ID of the row
 */

StrainValidationGrid.prototype.setMapLocation= function(rowID){
		var list = this.comp_values.getRow("Location",rowID,1);
		var info ="";
		var zoomLevel=10;
		var self=this;
		//no gps co-ordinate, therefore have to work it out
		//if (!list[6] && list[6] !==0){
		
		if(list.length>7){		
		
			if (list[6] ==="" || list[6] ==0 || list[7] ==="" || list[7] ==0){
					if (list[4]){
						info = list[4]+" "+list[3]+" "+list[2]+" "+list[1];
					}
					else if (list[3]){
						info = list[3]+" "+list[2]+" "+list[1];
						zoomLevel=8;
					}
					else if (list[2]){
						info = list[2]+" "+list[1];
						zoomLevel=6;
					}
					else if (list[1]){
						zoomLevel=4;
						info= list[1];
					
					}
					GMaps.geocode({
						address: info,
						callback: function(results, status){
							if(status=='OK'){
								var latlng = results[0].geometry.location;
								self.map.setCenter(latlng.lat(), latlng.lng());
								self.map.setZoom(zoomLevel);
							}
						}
					});
				}
				//set according to GPS
				else{
					self.map.setCenter(list[6],list[7]);
					self.map.setZoom(10);
				}
			
			}

}

/**
 *Sends a message to a modal dilog box see {@link Enterobase.modalAlert}
 *@overide
 *@param {string} msg - The message to be displays
 *@param {string} title - The title of the message
 */


StrainValidationGrid.prototype.duplicateRecord = function(ignoreRowID){
   
    var c = this.getColumnIndex("Accession");
    var combos = {};
   
    var data = this.dataUnfiltered?this.dataUnfiltered:this.data;
    for (var index1 in data){
	var r = data[index1].id;
	if (r=== ignoreRowID){
	    continue;
	}
	var val1= data[index1].columns[c];
	if (!val1){
	    continue;
	}
	vals = val1.split(",");
	for (index2 in vals){
	    var combo = vals[index2];
	    var list = combos[combo];
	    if (!list){
		list=[];
		combos[combo]=list;
	    }
	    list.push(r);
	}
    }
	 
    var dirty={};
    for (var key in combos){
	list =combos[key];
	
	if (list.length>1){
	    var msg = "Duplicate Reads";
	    for (index in list){
		var rowID = list[index];
		dirty[rowID]=true;
		this.updateErrors("Accession",rowID,msg);
	    }
	
	}
	else {
	    
	    var rowID = list[0];
	    var msg = this.errors["Accession"][rowID];
	    if (msg && msg !=="Duplicate Reads"){
		return;
	    }
	     if (!dirty[rowID]){
		this.updateErrors("Accession",rowID,"");
		this.checkRowForErrors(this.getRowIndex(rowID),false);
	    }
	}
	    
    }
  
     
}

StrainValidationGrid.prototype.updateAllChangedRecords = function(user_upload,rowID,username,password){
	this.total_records = 0;
	this.records_submitted = 0;
	this.user_upload=user_upload;
	this.bad = 0;
	this.infoList=[];
	rowIDs=[];
	if (rowID){
	    rowIDs.push(rowID);
	    this.total_records=1;
	}
	else{
	    for(var i=0;i<this.getRowCount();i++){
			var rowID  = this.getRowId(i);
			if (this.rowsWithChanges[rowID] || user_upload){
				this.total_records++;
				rowIDs.push(rowID)			
			}
	    }
	}
	if (this.total_records===0){
	    Enterobase.modalAlert("You have no edited records without errors<br>in the current filtered results","Warning",false);
	    return;
	}
	this.updateDatabase(rowIDs,user_upload,username,password);
}







/**sends the records to the server to be updated
 * @param {list} rowIDs A list of rowIDs to be sent
 * @param {boolean} user_upload If true then the records represent new records associated with
 * user uplaoded reads
 * @param {string} username for authentication (optional)
 * @param {string} password for authentication (optional)
*/
StrainValidationGrid.prototype.updateDatabase= function(rowIDs,user_upload,username,password){   
    //loop through table sending each value
    
    for (index in rowIDs){	
        var rowID=rowIDs[index];
		if (!user_upload && ! this.rowsWithChanges[rowID]){
			continue;
		}
		var values =  this.getValuesForRecord(rowID,user_upload);
		if (user_upload){
			values["user_upload"]=true;
		}	
	//actually send the record    
		this.sendRecord(values,rowID,username,password);   
    }
};

StrainValidationGrid.prototype.showMessage = function(msg){
	msg = msg.toLowerCase();
	msg=msg.replace(/\s/g,"-");
	window.open(Enterobase.wiki_url+"Metadata_Fields#rst-header-"+msg,'newwindow','width=1000, height=600');
}

StrainValidationGrid.prototype.getValuesForRecord = function(rowID,user_upload){
    var values = {};
    var rowIndex = this.getRowIndex(rowID);
	if (!user_upload){
		values['id']=rowID;
	}
    
    for (var colIndex=0;colIndex<this.getColumnCount();colIndex++){	    
	    var colName = this.getColumnName(colIndex);	   
	    if (this.col_list[colIndex]['not_write']){
			continue;
	    }
	/*   if (this.errors[colName][rowID] ){
			if (colName !=='Location' && colName !== 'Accession'){
				continue;
			}
	   }
	   */
	 //   if (!this.cell_modified[colName][rowID] && !user_upload){
	//		continue;
	  //  }
	    if (this.col_list[colIndex]['is_compound']){
		var ids = this.comp_values.getRowIDs(colName,rowID);
		for (index in ids){
		    var dict =this.comp_values.getRowAsDict(colName,rowID,ids[index]);
		    for (key in dict){
			if (!dict[key]){
				
				continue;
			}
			//multiple values i.e. reads for user upload
			if (values[key] && key !== "seq_insert" && user_upload){
			    values[key] +=","+dict[key];
			}
			//only single value  as all accessions map to same strain
			else{
			    values[key]=dict[key];
			}
		    }
		}
	    }
	    else{
		var val =  this.getValueAt(rowIndex,colIndex);
		//user upload ignore empty cells -  but if editing user could have deleted
		if (this.col_list[colIndex]['datatype']==='integer' && isNaN(val)){
			continue;
		}
		
		if (val || !user_upload ){
		    values[colName]=val;
	        }
	    }
	 }
    return values;
};

StrainValidationGrid.prototype.finishedUploadRecord=function (accession,msg){
   
	if (msg.substring(0,2) !=="OK"){    
	    this.bad++;
	    this.infoList.push(accession+":"+msg);
	}
	else{
	    if (this.user_upload){
		msg = "Sucessfully sent for Assembly";
	    }
	    else{
		msg = "Sucessfully updated";
	    }
	    this.infoList.push(accession+":"+msg);
	}
	
	this.records_submitted++;
	
	if (this.records_submitted===this.total_records || msg ==="abort"){
	    var action = "updated in  "
	    if (this.user_upload){
		action = "queued for assembly in "
	    }
	    var str =(this.records_submitted-this.bad)+ " record(s) "+action+this.species+" database:<br>"
	    if (this.bad){
		str+=""+this.bad+" record(s) contained an error<br>";
		     
	    }
	    for (var i in this.infoList){
		    str+=this.infoList[i]+"<br>";
	    }
		 
	    this.finishedUploadCallback(str);	
	}
}
	    


 StrainValidationGrid.prototype.sendRecord = function (sendData,rowID,username,password){
             
        var self = this;
        var request = {
            url: this.uploadURI+"/"+this.species,
            type: "PUT",     
            dataType: "json",  //type of data recieved              
            data:sendData,        
            //send password/username  if authentication is required
            beforeSend: function (xhr) {
                if (password){
                    xhr.setRequestHeader("Authorization", 
                    "Basic " + btoa(username + ":" +password));
                }
            },           
            error: function(jqXHR,text,body) {                           
                console.log("ajax error in sending record " + sendData['accession']);
                 var msg="connection error";
                 try{
                   msg= jqXHR.responseText;
                 }catch(e){
                      console.log(e);
                 }
                self.finishedUploadRecord(sendData['strain'],msg);
            },
            success: function(data){
                //self.removeRow(rowID);        
				self.deleteChangesInRow(rowID);         
                self.finishedUploadRecord(sendData['strain'],"OK");              
            }
        };
        //actually send the request
        $.ajax(request);     
};


StrainValidationGrid.prototype.processMetaData = function(callback){
    callback("OK");

};

/** retreives records from the database and loads the data into the grid. Loads the data using the
 * native id as row ID and does not validate see {@link ValidationGrid#loadNewData}
 * @param {string} link  The uri of the method used to retrieve the data
 * @param {object} data The data for the upload
 * @param {callback} callback The methods called once the data has loaded
*/
StrainValidationGrid.prototype.getRecords =  function(link,data,callback){
    var self = this;
     Enterobase.call_restful_json("/get_strains"+"/"+this.species,"POST",data,callback).done(function(data){
	self.loadNewData(data,false,true);
        callback("OK");
        });
};

/**
Gets all the information  on columns and validation to set up the table
@param {string} link the address of the api call
@param {callback} callback - this is called when the table has been initialized
(has single string argument which is 'OK' or error message)
@param {string} username optional but can be used for authentication
@param {string} password optional but can be used for authentication
*/
StrainValidationGrid.prototype.setMetaData= function(link,callback, username,password){   
    var self =this;    
    Enterobase.call_restful_json("/species/"+this.species+"/get_strain_columns","GET","",callback,username,password).done(function(data){
        //check for compound columns and put in right format
        for (var index in data){
			if (self.ignoreList.indexOf(data[index]['name'])!==-1){
				delete data[index];
				continue;
			}
			//Deal with compound columns
            if (data[index].length>1){
				var entry = data[index][0];
				if (self.ignoreList.indexOf(entry['group_name'])!==-1){
					delete data[index];
					continue;
				}             
			//create the main column
				var column = {};
					//set the properties
				column['is_compound']=true;
				column['field_list']=[];
				column['name']= entry['group_name'];
				column['label'] =entry['group_name'];
				column['display_order']=entry['display_order'];
				column['allow_multiple_values'] = entry['allow_multiple_values'];
				column['required'] = entry['required']
				//will not edit column directly
				   
					//add the the fields to the column
				column['field_list']=[];
				//add sub-fields to column
				column['description']=  DescriptionsForColumns[column['label']];
				for (var i in data[index]){
					column['field_list'].push(data[index][i]);		    
				} 
				if (column['name']==='Accession'){
					column['editable']=false;
					column['label'] = "Data Source"
					var col={
						name:"status",
						label:"Status",
						editable:"false"
					};
				
					column['field_list'].push(col);
				}
				data.push(column);
			//delete the sub column from the main list (it is now nested in the compound column)
				delete data[index];
            }
	   
			else {
				var column = data[index];
				if (column['name'] === 'created' || column['name'] === 'release_date'){
					column['editable']=false;    
				}
			}
        }
	
        //call super method 
        ValidationGrid.prototype.setMetaData.call(self,data);
	
		self.val_params['city']['getInput']= function(rowID){
			var input =$("<input>").attr("type","text").attr('autocomplete','off');
			input.jeoLocationAutoComplete(4,self,rowID);
			return input;	
		};
		self.val_params['admin2']['getInput']= function(rowID){
			var input =$("<input>").attr("type","text").attr('autocomplete','off');
			input.jeoLocationAutoComplete(3,self,rowID);
			return input;	
		};
		self.val_params['admin1']['getInput']= function(rowID){
			var input =$("<input>").attr("type","text").attr('autocomplete','off');
			input.jeoLocationAutoComplete(2,self,rowID);
			return input;	
		};
		self.val_params['country']['getInput']= function(rowID){
			var input =$("<input>").attr("type","text").attr('autocomplete','off');
			input.jeoLocationAutoComplete(1,self,rowID);
			return input;	
		};
	

	
	
		self.addCustomCellValidator("Location",function(colName,rowID){  
			var row = self.comp_values.getRow(colName,rowID,1);
			var colindex = self.getColumnIndex(colName);
			var required = self.col_list[colindex]['required'];
		
		 
			if (!row && !required){
				return self.updateErrors(colName,rowID,""); 
			}

			var msg="";
			//Do simple checks first without calling geonames
	  
			if (!self.validateCompoundField(colName,rowID)){
				msg = "One or more values in this field are incorrect";
			}
			//country or continent are wrong
	    
			else if (row[1] && self.countries && self.countries[row[1]] !== row[0]){
				if (!required && !row[0] && !row[1]){
					return self.updateErrors(colName,rowID,""); 
				}
				msg = "The Country/Continent are incorrct";
			}
			else if ((row[6] && isNaN(row[6])) || (row[7] && isNaN(row[7]))){
				msg = "Lat/Long must be in decimal format "
			}

			else if ((row[6] || row[6]===0) && (!row[7] && row[7] !==0) ){
				msg = "Both latitude and longitude are required";
		
	    }
	    else if ((row[7] || row[7]===0) && (!row[6] && row[6] !==0) ){
		msg = "Both latitude and longitude are required";
	    }
	    else if (row[0] && !jeoquery.continents[row[0]]){
		msg = "Incorrect Continent";	
	    }
	    
	    else if (row[1] && (row[1]===row[2] || row[1]===row[3])){
		msg = "Cannot resolve location";
		
	    }
	    if (msg !==""){
		return 	self.updateErrors(colName,rowID,msg);    
	    }
	     
	    //we know country and continent are correct therefore if no
	    //other data we can pass
	    if (!row[2] && !row[3] && !row[4]){
		return 	self.updateErrors(colName,rowID,msg);  
	    }    
	    
	    
		
	    var query = row[0]+","+row[1]+","+row[2]+","+row[3]+","+row[4];
            //location already verified, no point calling jeoquery
            if (self.validLocations[query]){
                return 	self.updateErrors(colName,rowID,''); 
            }
            
            //infividually check
            if (self.checkLocation){
                jeoquery.getGeoNames('search', {
                    featureCode: "P",
                    style: "FULL",
                    maxRows: 1,
                    q: query
                }).done(function(data){
                    self.callsToGeonames++;
                    var msg="";
                    if (!data["totalResultsCount"]){
                        msg="The location given cannot be resolved";		    
                    }
                    if (msg === ""){
                        self.validLocations[query]=true;
                    }
                    self.updateErrors(colName,rowID,msg);
                    self.checkRowForErrors(self.getRowIndex(rowID),false);
                    self.refreshGrid();
                });
            }
            //check in bulk - dereplicate
            else{
                var list = self.allLocations[query];
                if (! list){
                    list=[];
                    self.allLocations[query] = list;
                }
                list.push(rowID);
            }
	});
	self.addCheckSelectColumn();
        self.addIndividualEditColumn();
        self.processMetaData(callback);
        
     }); 
};


//checks location and then updates all rows associated with that location
StrainValidationGrid.prototype.checkAllLocations= function(location){
        var self = this;
         jeoquery.getGeoNames('search', {
                    featureCode: "P",
                    style: "FULL",
                    maxRows: 1,
                    q: location
                }).done(function(data){
                    self.callsToGeonames++;
                    var msg="";
                    if (!data["totalResultsCount"]){
                        msg="The location given cannot be resolved";		    
                    }
                    if (msg === ""){
                        self.validLocations[location]=true;
                    }
		    var locs =  self.allLocations[location];
                    for (var ind in locs){
			 var rowID = locs[ind]
                         self.updateErrors('Location',rowID,msg);
                         self.checkRowForErrors(self.getRowIndex(rowID),false);
			 
                    }
		    delete self.allLocations[location];
                   
                });   
};


StrainValidationGrid.prototype.isGPSValid= function(gps){

    var isGood = true;
    var latlong = gps.split(",");
    if (latlong.length!==2){
	return false;
    }
    else {
	var lat = parseFloat(latlong[0]);
	var lng = parseFloat(latlong[0]);
	if (isNaN(lat) || isNaN (lng)){
	    isGood = false;
	}
	else{
	    if ((lat>90 || lat <-90 || lng>180 || lng <-180)){
		isGood = false;
	    }
	
	}
    
    if (isGood){
	return [lat,lng];
    }
    else{
	return false;
    }
    }
	
};
	    
/* only validate those records that user can edit
and are not already verified - only check for dupllicates once
*/
StrainValidationGrid.prototype.validateStrains=function(){
        //only need to carry out expensive operations once
	this.checkDuplicates = false;
        this.checkLocation = false;
	for (var r=0;r<this.getRowCount();r++){
	    
	    var rowID = this.getRowId(r);
	    //if (this.nonEditableRows[rowID]){
		//	continue;	    
	    //}
	    if (!this.nonVerifiedRows[rowID]){
		//continue;
	    }
	    for (var c =0;c<this.getColumnCount();c++){
		this.validateCell(r,c);
	    }
            //row will be checked when location checked
	    this.checkRowForErrors(r,false);
	}
	        
	
	//if (this.getCompleteRowCount()>0){
	//    this.duplicateRecord();
	//}
        
        for (var location in this.allLocations){
            this.checkAllLocations(location);
            
        }    
	//this.checkDuplicates = true;
        this.checkLocation = true;
	this.showErrors=true;
        console.log('Calls to geonames:'+this.callsToGeonames);
	this.errorListener(this.errorNumber,this.numberOfRowsWithErrors);
	this.refreshGrid();
    
};


StrainValidationGrid.prototype.getStrainName = function(rowID){
    var r = this.getRowIndex(rowID);
    var c= this.getColumnIndex('strain');
    return this.getValueAt(r,c);

}


