UGSGrid.prototype = Object.create(BaseExperimentGrid.prototype);
UGSGrid.prototype.constructor= UGSGrid;


function UGSGrid(name,config,species,scheme,scheme_name){
        BaseExperimentGrid.call(this,name,config,species,scheme,scheme_name);

        this.addExtraRenderer("genome_stat",function(cell,value,row_id,col_name_row_index){
                if (value>8000){
                        $(cell).css("background-color","red");
                }
        });
};




UGSGrid.prototype.setMetaData = function(data){
        data.push({
                datatype:"text",
                display_order:2,
                name:"view_data",
                label:"<span class='glyphicon glyphicon-eye-open style='padding:0px;margin:0px;display:inline-block;''></span>",
                not_write:true
        });
        var self = this;

        this.addExtraRenderer("view_data",function(cell,value,row_id,col_name,row_index){
                $(cell).html("<span class='glyphicon glyphicon-eye-open style='padding:0px;margin:0px;display:inline-block;font-size:+"+self.fontSize+"px'></span>")                            
               .css("cursor","pointer");
        });

        this.addCustomColumnHandler("view_data",function(cell,row_index,row_id){
                var col_index =  self.getColumnIndex("genome_stat")
                var value = self.getValueAt(row_index,col_index);
                self.displayInfo(value);
        });

        //call the super method
        BaseExperimentGrid.prototype.setMetaData.call(this,data);
        this.columnWidths["view_data"]=20;
}

UGSGrid.prototype.displayInfo = function(value){
        Enterobase.modalAlert("The UGS value is "+ value,"Information");
}

UGSGrid.prototype.addToContextMenu =  function (row_index,col_index, target){
        var self = this;
        var extramenu=[	{
                         name: 'More Info',
                         title: 'More Info',
                         fun: function () {
                                col_index=self.getColumnIndex('genome_stat');
                               value =self.getValueAt(row_index,col_index);
                                self.displayInfo(value);
                        }
                }        
        ];         
        return extramenu;
}