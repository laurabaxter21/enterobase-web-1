ReadsGrid.prototype = Object.create(ValidationGrid.prototype);
ReadsGrid.prototype.constructor = ReadsGrid
/**
 * Represents a Grid read information and read status e.g. uplaoding, assembled etc.
 * @constructor
 * @extends ValidationGrid
 * @param {string} name - The name of the grid
 * @param {object} config - Configuration options
 * @param {string} species - The name of the database associated with this table
 */
function ReadsGrid(name,config,species){
   
   this.species=species;
   this.read_info_url = "/get_user_read_info";
   this.remove_read_url="/"
   ValidationGrid.call(this,name,config);
   /**
 * Callback Method called when reads are removed form the grid 
 * giving a list of the read names removed
 */
   this.readsRemovedListener = function(){};
   this.warnOnExit = false;
  
};

ReadsGrid.prototype.showMessage = function(msg){
     window.open(Enterobase.wiki_url+"add-upload-reads.html",'newwindow','width=1000, height=600');
}



/**
*Shows whether the grid contains any reads that have the given status
* @param {string} status - The status to be checked e.g. uploaded
* @returns {boolean} True if any read has the given status
*/
ReadsGrid.prototype.hasReadWithStatus=function(status){
   for (var r=0;r<this.getRowCount();r++) {    
      var name_list=[];
      var name_to_status=this.getReadInfo(r)
      for (var name in name_to_status){
                var n = name_to_status[name]
                if (n ===status){
                        return true;
                
                }
        }
   }
   return false;
        
};


/**
Given a read name will return the sequencing platform of the read
* @param {string} read_name - The name of the read
*/
ReadsGrid.prototype.getReadPlatform= function(read_name){
      var c1 = this.getColumnIndex("read_1");
      var c2 = this.getColumnIndex("read_2");
      var c3 = this.getColumnIndex("seq_platform")
      for (var r=0;r<this.getRowCount();r++) { 
                if (read_name === this.getValueAt(r,c1) || read_name === this.getValueAt(r,c2)){
                        return this.getValueAt(r,c3);
                }          
      
      }
      return false;
     
};

/**
* Updates the read status in the grid to 'Local Upload' for any read names supplied in the given dictionary
* @param {object} status - A dictionay where the keys contain the names of reads which have a local file object associated with them
*/

ReadsGrid.prototype.updateLocalReads= function(local_read_dict){
        var c1 = this.getColumnIndex("read_1");
        var c2 = this.getColumnIndex("read_2");
        var ci1 = this.getColumnIndex("read_1_status");
        var ci2 = this.getColumnIndex("read_2_status");
        var plat_id = this.getColumnIndex('seq_platform');
        
        for (var r=0;r<this.getRowCount();r++){
          var  rid = this.getRowId(r);
          var platform = this.getValueAt(r, plat_id);
          var name = this.getValueAt(r,c1);
          if (local_read_dict[name]){
                this.setValueAt(r,ci1,"Local Upload",false)
          }
          name = this.getValueAt(r,c2);
          if (local_read_dict[name]){
                this.setValueAt(r,ci2,"Local Upload",false)
          }
        
        }
        this.refreshGrid();
}


/**
 * Removes a row from the grid provided that the status of all reads 
 * in the row have a status of 'Awaiting Upload' . The remote
 * database is also updated and the readsRemovedListener is called passing
 * a list of the read names removed (or an empty list)
 * @overide
 * @param {integer} rowID - The ID of the row to be removed
 * @returns {boolean} If successful true, otherwise null
 */
ReadsGrid.prototype.removeRow= function(rowID){
      var r = this.getRowIndex(rowID);
      var name_list=[];
      var name_to_status=this.getReadInfo(r)
        for (var name in name_to_status){
                var n = name_to_status[name]
                if (n !=='Processing' && n !=='Uploaded' && n !== 'Assembled'){
                       name_list.push(name);
                }
                else{
                        
                        this.readsRemovedListener(name_list);
                        return false;
                }
        }        
    
      ValidationGrid.prototype.removeRow.call(this,rowID);
      var to_send= {
        species:this.species,
        read_names:name_list.join(",")
      }
      Enterobase.call_restful("/remove_user_read","GET",to_send,function(msg){
                Enterobase.modalAlert("Cannot delete user reads from database\n"+msg);
      
      });
      this.readsRemovedListener(name_list);
      return true;
}

/**
 *Gets information about all read names and status for the given row index
 *@param {integer} rowIndex - The index of the row
 *@returns A dictionary of read names to status e.g. <code> {read1.fq:Uploaded,read2.fq:Awaiting Upload}</code>
 */
ReadsGrid.prototype.getReadInfo= function(rowIndex){      
      var c1 = this.getColumnIndex("read_1");
      var c2 = this.getColumnIndex("read_2");
      var ci1 = this.getColumnIndex("read_1_status");
      var ci2 = this.getColumnIndex("read_2_status");
      var name_to_status= {};
      var name = this.getValueAt(rowIndex,c1);
      name_to_status[name] = this.getValueAt(rowIndex,ci1);
      name = this.getValueAt(rowIndex,c2);
      name_to_status[name] = this.getValueAt(rowIndex,ci2);
      return name_to_status;
}

/**
 *Gets all the read names that are awiting uplaod
 *@return {Array} A list of all read names awaiting upload
 */
ReadsGrid.prototype.getFilesForUpload= function(){
        var name_list=[];    
        for (var r=0;r<this.getRowCount();r++){
                var name_to_status=this.getReadInfo(r)
                for (var name in name_to_status){
                        if (name_to_status[name]==='Awaiting Upload' || name_to_status[name] === 'Local Upload'){
                                name_list.push(name)   
                        }
                }
        }        
        return name_list;
};

/**
 *Gets all the read names that are awiting uplaod
 *@return {Array} A list of all read names awaiting upload
 */

ReadsGrid.prototype.setMetaData= function(){ 
        this.lineColors= ["#FFF","#FFF"]
        var columns = [
                {
            name:"strain",
            label:"Strain Name",
            datatype:"text",
            display_order:2
          
        },
        {
            name:"read_1",
            label:"Read 1",
            datatype:"text",
            display_order:3
          
        },
         {
            name:"read_1_status",
            label:"Read 1 Status",
            datatype:"text",
            display_order:4
          
        },
        {
            name:"read_2",
            label:"Read 2",
            datatype:"text",
            display_order:5
          
        },
         {
            name:"read_2_status",
            label:"Read 2 Status",
            datatype:"text",
            display_order:6
          
        },
           {
            name:"seq_platform",
            label:"Platform",
            datatype:"text",
            display_order:7
          
        }   
        ];
        ValidationGrid.prototype.setMetaData.call(this,columns);
        this.columnWidths["read_1"] = 150
        this.columnWidths["read_2"]=200;
        this.columnWidths["read_1_status"]=150;
        this.columnWidths["read_2_status"]=200;   
        this.addCheckSelectColumn();
              
}


ReadsGrid.prototype.setReadStatus = function(readName,status){
      var c1 = this.getColumnIndex("read_1");
      var c2 = this.getColumnIndex("read_2");
      var ci1 = this.getColumnIndex("read_1_status");
      var ci2 = this.getColumnIndex("read_2_status");   
        for (var r=0;r<this.getRowCount();r++){
          var  rid = this.getRowId(r);
          var name = this.getValueAt(r,c1);
          if (name===readName){
                var prevVal = this.setValueAt(r,ci1,status,false);
                this.modelChanged(r,ci1,prevVal,status);
          }
          name = this.getValueAt(r,c2);
          if (name===readName){
                var prevVal = this.setValueAt(r,ci2,status,false);
                this.modelChanged(r,ci2,prevVal,status);
          }      
        }
}

ReadsGrid.prototype.addToContextMenu =  function (rowIndex,colIndex,target){
    var rowID = this.getRowId(rowIndex);
    var self = this;
    var extramenu =[
        {
        name: 'Remove Selected Rows',
        title: 'Remove Seleceted Rows',
        fun: function () {
                for (var id in self.selectedRows){
                        self.removeRow(id);
                }
                self.selectedRows={};
                self.refreshGrid();
        }
    }
             
    ];
    return extramenu;
};



