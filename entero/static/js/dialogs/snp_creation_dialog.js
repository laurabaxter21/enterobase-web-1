SNPCreationDialog.prototype = Object.create(null);
SNPCreationDialog.prototype.constructor= SNPCreationDialog;


/**
* Creates a dialog from which the user can create an SNP Tree. The actual dialog is not created, but will be created on the first call to show
* @constructor
* @param {SRAValidationGrid} strain_gird - The table from which the SNP Tree will be created from
* @param {function} callback- A callback, which is fired when the tree has been sent, which
* should accept the tree name, id and parent workspace (if relavent)
*/
function  SNPCreationDialog(strain_grid,callback){
        this.strain_grid = strain_grid;
        this.experiment_grid=null;
        this.treeSentCallback=callback;
        this.assembly_ids=[];
        this.ws_dialog= null;
}

/**
* Shows the dialog 
* @param {ExperimentGrid} experiment_grid - The table to get the information for the tree from
* @param {integer} The parent workspace or 0 if a standalone tree
* @param {EnteroWorkspaceDiaolog} The workspace dialog, used for choosing an SNP list
*/
SNPCreationDialog.prototype.show= function(experiment_grid,parent_workspace,ws_dialog){
        this.experiment_grid=experiment_grid;
        this.parent_workspace=parent_workspace;
        if (! this.div){
                this._createDialog();
        }
        else{
                this._update();
                this.div.dialog("open");	
        }
        this.ws_dialog=ws_dialog;
}


SNPCreationDialog.prototype._createDialog=function(){
        var self = this;
        this.div  = $("<div>");
        this.snp_list_file_browser= new Enterobase.FileBrowser(function(files){
                        self._processSNPList(files[0]);      
        },".txt");
        this.defined_snp_browser= new Enterobase.FileBrowser(function(files){
                        self.defined_snps_file=files[0];
                        self.defined_snps_file_name.text(files[0].name);
                       
                      
        },".txt");
        this.div.css({
                display: "-webkit-flex",
                display: "flex"
        });
        $("body").append(this.div);
        this.div.dialog({ 
                title: "Create SNP Project", 
                buttons:[
                        {
                        text:"Submit",
                        id:"snp-creation-submit",
                        click:function(e){
                                self._submitTree();             
                                } 
                        }	
                ]
                ,
                close:function(){               
                },
                autoOpen:true,
                width: 500,
                height:500 ,
              
        });
        var top_div=$("<div>");
        var table = $("<table>").css({"display":"inline-block","flloat":"left"});
        top_div.append(table).appendTo(this.div);
        //project name
        var row = $("<tr>").appendTo(table).append("<td><label>Name</label></td>");
        this.name_input = $("<input>").css("width","250px");
        $("<td>").append(this.name_input).appendTo(row);
        //Reference Genome
        row= $("<tr>").appendTo(table).append("<td><label>Reference Genome</label></td>")
        var cell = $("<td>").appendTo(row);
        this.reference_select=$("<select>").attr("id","snp-reference-select").appendTo(cell);
        //only selected genomes
        row= $("<tr>").appendTo(table).append("<td><label>Selected Only</label></td>");
        cell = $("<td>").appendTo(row)
        this.selected_check=$("<input>").attr("type","checkbox").css("margin-left","3px").appendTo(cell);
        this.selected_check.click(function(e){
                self._update();	
        });  
        //% missing sites
        row= $("<tr>").appendTo(table).append("<td><label>Min. % Presences</label></td>");
        this.missing_sites_input = $("<input>").attr({"id":"snp-missing-sites"}).val("95").width(40);
        cell=$("<td>").append(this.missing_sites_input).appendTo(row);
        this.missing_sites_input.spinner({min:50,max:100,step:1});
        //show annototions
        row= $("<tr>").appendTo(table).append("<td><label>Show Annotations</label></td>");
        cell = $("<td>").appendTo(row)
        this.annotations_check=$("<input>").attr("type","checkbox").css("margin-left","3px").appendTo(cell);
         //email
        row= $("<tr>").appendTo(table).append("<td><label>Email when finished</label></td>");
        cell = $("<td>").appendTo(row)
        this.email_check=$("<input>").attr("type","checkbox").css("margin-left","3px").appendTo(cell);
        //Defined Matrix
        row= $("<tr>").appendTo(table).append("<td><label>User Defined SNP List</label></td>");
        cell = $("<td>").appendTo(row);
     
        this.matrix_text=$("<span>").text("None").data("id",0).appendTo(cell);
         var span =$("<span>").attr({"class":"glyphicon glyphicon-folder-open","alt":"Upload File"})
         .click(function(e){
                self._getSNPList();
         })
         .css({"cursor":"pointer","margin-left":"4px"}).appendTo(cell);
         span =$("<span>").attr({"class":"glyphicon glyphicon-upload","alt":"Upload File"})
         .click(function(e){
               self.snp_list_file_browser.getFiles();
         })
         .css({"cursor":"pointer","margin-left":"4px"}).appendTo(cell);
         //pre-existing SNPs
        row= $("<tr>").appendTo(table).append("<td><label>Pre Existiing SNPs</label></td>");
        cell = $("<td>").appendTo(row);
        this.defined_snps_file_name=$("<span>").text("None").appendTo(cell);
        span =$("<span>").attr({"class":"glyphicon glyphicon-upload","alt":"Upload File"})
         .click(function(e){
               self.defined_snp_browser.getFiles();
         })
          .css({"cursor":"pointer","margin-left":"4px"}).appendTo(cell);
         this.defined_snps_file=null;
         
       
 
       
       
       
        row= $("<tr>").appendTo(table).append("<td><label>Workspace</label></td>")
        this.workspace_name_text=$("<td>").appendTo(row);
        
        row= $("<tr>").appendTo(table).append("<td><label>No. Strains (With Genomes)</label></td>")
        this.strain_number_text=$("<td>").appendTo(row);
           
        this.warning_div= $("<div>").appendTo(top_div);
        table.find("td").css("padding-left","3px");
        this._update();
        
}


SNPCreationDialog.prototype._processSNPList= function(file){
        var self = this;
        new Enterobase.TextReader(file,"\t",function(status,data,header_index){
                if (status !== 'OK'){
                        Enterobase.modalAlert("There was a problem reading the file. Is it  a tab delimited .txt file?");
                        return;
                }
                var essential=0;
                for (var i in header_index){
                        var name = header_index[i];
                        if (name === 'contig' ||  name === 'position'){
                                essential++;
                        }
                }
                if (essential!==2){
                        Enterobase.modalAlert("The file must contain contig and position headers");
                        return;
                }
                for (var i in data){
                        if (isNaN(data[i]['position'])){
                                  Enterobase.modalAlert("The file must contain contig and position headers");
                                  return                   
                        }         
                }
                self.ws_dialog.showSaveDialog(function(name){
                        self._saveSNPList(data,name,header_index);    
                },"SNP List");
                
                  
        });
}

SNPCreationDialog.prototype._saveSNPList=function(data,name,headers){
        var self =this;
        var to_send={
                name:name,
                data:JSON.stringify(data),
                database:this.experiment_grid.species,
                headers:JSON.stringify(headers)
        };
         Enterobase.call_restful_json("/save_snp_list","POST",to_send,this.showError).done(function(ret_data){      
              if (!ret_data['success']){
                      self.showError("There was a problem saving the list");
              }
              else{
                       self.ws_dialog.addNewWorkspace([name,ret_data['id'],"mine","mine","snp_list"]);
                       self.matrix_text.text(name).data("id",ret_data['id']); 
                       Enterobase.modalAlert("The SNP list has  been sucessfully uploaded and will be used in this SNP Project");
              } 
              
      });       
        


}

SNPCreationDialog.prototype._getSNPList= function(){
        var self=this;
        this.ws_dialog.getSelectedIDs(function(ids,wps){           
                self.matrix_text.text(wps[0][0]).data("id",wps[0][3]);        
        },"OK",{"types":["snp_list"]})

}

//updates the view depending on the number of (selected) strains in the table
SNPCreationDialog.prototype._update = function(){
        //populate the reference dropdown
        var sel = $('#snp-reference-select');
        sel.empty();
        sel.append($("<optgroup label='Complete Genomes' id='snp-ref-comp'></optgroup>"));
        sel.append($("<optgroup label='Other' id='snp-ref-other'></optgroup>"));   
        var all_data = this.strain_grid.dataUnfiltered?mainGrid.dataUnfiltered:mainGrid.data;
        var colIndex = this.strain_grid.getColumnIndex("strain");
        for (var n in all_data){
                n = parseInt(n);
                var id = all_data[n].id;
                if (this.strain_grid.hasAssembly[id]!=='Assembled'){
                        continue;
                }
                var name = all_data[n].columns[colIndex];
                var cids =this.strain_grid.comp_values.getRowIDs("Accession",id);
                var is_complete=false;
                for (var i in cids){	    
                        var row = mainGrid.comp_values.getRowAsDict("Accession",id,cids[i]);
                        if (row['seq_platform']==='Complete Genome'){
                                $("#snp-ref-comp").append($("<option>").val(id).text(name));
                                is_complete=true;
                                break;
                        }
                }
                if (!is_complete){
                                $("#snp-ref-other").append($("<option>").val(id).text(name));
                }    
        }
        
        this.warning_div.empty();
        this.name_input.focus();
        $("#snp-creation-submit").attr("disabled",false);
        var sel = this.selected_check.prop("checked");
      
        this.assembly_ids=[];
        for (var n=0;n<this.strain_grid.getRowCount();n++){
                var id =  this.strain_grid.getRowId(n);
                if (sel && ! this.strain_grid.selectedRows[id]){
                        continue;
                }
                if (this.strain_grid.hasAssembly[id]==='Assembled'){
                        this.assembly_ids.push(this.strain_grid.best_assembly[id]);
                }
        }
        var strain_count = this.assembly_ids.length;
        this.strain_number_text.text(strain_count);
        var suggest_name = "snp_tree_"+ new Date().toJSON().slice(0,10);
        if (this.parent_workspace){
                suggest_name=this.parent_workspace[0]+"_"+suggest_name;
        
        }
        this.name_input.val(suggest_name);
        var ws_name = this.parent_workspace?this.parent_workspace[0]:"None";
        this.workspace_name_text.text(ws_name)
        
      
        
      
        if (strain_count<2){
                var msg = "<br><b>Error</b><br>You only have "+strain_count+ " strains(s) - the Tree will fail ";
                                msg += "Please close the dialog and either load or select more strains"
                this.warning_div.html(msg);
                $("#mst-creation-submit").attr("disabled",true);
                return;
        
        }
     

}

SNPCreationDialog.prototype.showError= function(msg){
        Enterobase.modalAlert(msg);
}

SNPCreationDialog.prototype._submitTree=function(){
        var self = this;
        var ws_id = 0;
        var ref_id = self.strain_grid.best_assembly[$('#snp-reference-select').val()];
        var ref_name = $('#snp-reference-select :selected').text();
        if (this.parent_workspace){
                ws_id=this.parent_workspace[3];
        
        }
        
        to_send= {
                assembly_ids:this.assembly_ids,
                ref_id:ref_id,
                name:this.name_input.val(),
                workspace:ws_id,
                database:this.experiment_grid.species,
                snp_list:parseInt(this.matrix_text.data("id")),
                create_annotation:this.annotations_check.prop("checked"),
                send_email : this.email_check.prop("checked"),
                missing_sites:parseInt(this.missing_sites_input.val())/100,
                parameters:{
                        strain_number:parseInt(this.strain_number_text.text()),
                        reference:ref_name,
                        snp_list:this.matrix_text.text(),
                        missing_sites:this.missing_sites_input.val()
                        
                }
        }
        
        
        
        
        
        Enterobase.call_restful_send_json("/create_snp_project","POST",to_send,this.showError).done(function(data){      
              if (!data.startsWith("OK")){
                      self.showError(data);
              }
              else{
                      if (self.defined_snps_file){
                                var formData = new FormData();
                                formData.append('file', self.defined_snps_file);

                               $.ajax({
                                        url : '/upload/upload_analysis_file?type=predefined_snps&',
                                        type : 'POST',
                                        data : formData,
                                        processData: false,  
                                        contentType: false, 
                                        success : function(data) {
                                         console.log(data);
                                        
                                }
                                });
                      
                      }
                      if (self.treeSentCallback){
                              var arr= data.split(":");
                              self.treeSentCallback(arr[1],arr[2],ws_id);
                                           
                      }
                      self.div.dialog("close"); 
              }        
      });       
}


    
