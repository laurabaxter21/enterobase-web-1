UberStrainDialog.prototype = Object.create(null);
UberStrainDialog.prototype.constructor= UberStrainDialog;



/**
* Creates a dialog from which the user can manipulate uber strains
* @constructor
* @param {SRAValidationGrid} strain_gird - The table from which the SNP Tree will be created from
* @param {EnteroWorkspaceDiaolog} - The dialog for choosing SNP matrix
* @param {function} callback- A callback, which id fired when the tree has been sent, which
* should accept the tree name and id
*/
function  UberStrainDialog(strain_grid){
     this.strain_grid=strain_grid;
     this.fields_to_display = ["strain","barcode"];
}

/**

/**
* Shows the dialog (and creates it if not already created)
* @param {list} ids- A list of ids to manipulate
*/
UberStrainDialog.prototype.show= function(){
        if (! this.div){
                this._createDialog();
        }
        
        this._update();
        
       
}

//updates the view depending on the selected strains in the table
UberStrainDialog.prototype._update = function(){
        $(".uber-strain-table-rows").remove();
        this.ids=this.strain_grid.getSelectedRows();
        if (this.ids.length===0){
                  Enterobase.modalAlert("Please select strains First");
                  return;
        }
        //get the checked uber_strain
        this.uber_strain=null;
        for (var i in this.ids){
                var id =this.ids[i];
                var uber = this.strain_grid.uberStrain[id];
                if (uber && uber.substrains){
                        if (this.uber_strain !== null){
                                 Enterobase.modalAlert("Please select just one Uber Strain");
                                 return
                        }
                        else{
                                this.uber_strain=id
                        }
                }      
        }
        var col_indexes=[];
        for (var i in this.fields_to_display){
                col_indexes.push(this.strain_grid.getColumnIndex(this.fields_to_display[i]));
        
        }
        
        for (var i in this.ids ){
                var row = $("<tr>").attr("class","uber-strain-table-rows");
                var id = this.ids[i];
                var row_index = this.strain_grid.getRowIndex(id);
                var radio_button = $("<input>").attr({
                        type:"radio",
                        name:"uber-dialog-radio-group",
                        value:id+"",
                        id:"uber-dialog-radio-"+id
                })
             
                $("<td>").append(radio_button).appendTo(row);
                for (var ii in col_indexes){
                        var col_index =  col_indexes[ii];
                        $("<td>").text(this.strain_grid.getValueAt(row_index,col_index))
                        .css("padding","3px").appendTo(row);
                }
                row.appendTo(this.table)
        }
        if (this.uber_strain !== null){
                $("#uber-dialog-radio-"+this.uber_strain).prop("checked",true);
        }
        this.div.dialog("open");
    
     

}

UberStrainDialog.prototype._createUberStrain=function(){
        var checked_id = $("input[name='uber-dialog-radio-group']:checked").val();
        if (! checked_id && checked_id !==0){
               Enterobase.modalAlert("Please Select a Master Strain");
               
        }
        if (this.uber_strain && checked_id != this.uber_strain){
                this.strain_grid.destroyUberStrain(this.uber_strain)
        }
        this.ids.splice(this.ids.indexOf(checked_id),1);
        this.strain_grid.addToUberStrain(checked_id,this.ids);
        this.div.dialog("close");

}

UberStrainDialog.prototype._removeUberStrain=function(){
       
        if (this.uber_strain){
                this.strain_grid.destroyUberStrain(this.uber_strain)
        }
        
        this.strain_grid.removeFromUberStrain(this.ids);
        this.div.dialog("close");

}


UberStrainDialog.prototype._createDialog=function(){
        var self = this;
        this.div  = $("<div>");
      
        $("body").append(this.div);
        this.div.dialog({ 
                title: "Manage Uberstrains", 
                buttons:[
                        {
                        text:"Remove",
                        click:function(e){
                                self._removeUberStrain();             
                                } 
                        },
                        {
                        text:"Create",
                        click:function(e){
                                self._createUberStrain();             
                                } 
                        },
                          {
                        text:"Cancel",
                        click:function(e){
                                self.div.dialog("close");          
                                } 
                        }
                        
                        
                ]
                ,
                close:function(){               
                },
                autoOpen:false,
                width: 500,
                height:500 ,
              
        });
        
        this.table = $("<table>").css({"display":"inline-block","flloat":"left"});
      
        //project name
        var row = $("<tr>").appendTo(this.table).append("<td>");
        for (var i in this.fields_to_display){
                var field = this.fields_to_display[i];
                var col_id = this.strain_grid.getColumnIndex(field);
                $("<td>").append("<label>"+this.strain_grid.col_list[col_id]['label']+"</label>").
                css("padding","3px").appendTo(row);
        }
        this.div.append(this.table); 
        
}


