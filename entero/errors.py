from flask import render_template
from flask import Flask
app = Flask(__name__)



@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html'), 404

@app.errorhandler(500)
def internal_error(exception):
    app.logger.exception(exception)
    return render_template('500.html'), 500

@app.errorhandler(403)
def internal_server_error(e):
    return render_template('403.html'), 403