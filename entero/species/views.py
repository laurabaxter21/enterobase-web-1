from logging import exception

from flask_login import current_user
from flask import render_template,abort,request,redirect,url_for,flash,send_file, make_response, jsonify
from ..databases.system.models import UserInputFields, UserJobs, UserPreferences, check_permission,BuddyPermissionTags, User, UserPermissionTags,query_system_database
import os 
#from entero.utilities import getStrainInfo
import json
import ujson
import os
from entero import dbhandle,app,db, get_database, rollback_close_system_db_session, get_download_scheme_folder
from entero.cell_app.tasks import process_job
from . import species
from entero.ExtraFuncs.workspace import get_access_permission

from ..decorators import admin_required, auth_login_required, view_species_required, auth_login_method, user_permission_required
from sqlalchemy import func, or_, Integer, not_
import requests
from entero.ExtraFuncs.query_functions import query_function_handle
from datetime import datetime,timedelta
import glob,gzip
from entero.ExtraFuncs.SNPCalling import get_ids_to_be_called
from entero.ExtraFuncs.workspace import SNPProject, get_analysis_object, get_shared_folders
from entero.jobs.jobs import get_crobot_job, send_all_jobs_for_assembly

ws_types=[]
ws_main_search_types=[]
if app.config.get("ANALYSIS_TYPES"):
    for ws_type in app.config['ANALYSIS_TYPES']:
        #these can't be loaded directly but from parent workspace
        if ws_type <> 'ms_tree' and ws_type <> 'snp_project' :
            ws_types.append(ws_type)
        if app.config['ANALYSIS_TYPES'][ws_type].get("create_from_search"):
            ws_main_search_types.append(ws_type)
    # I have added 'workspace_folders' to account for shared folder field
    ws_types_plus=['workspace_folders']
    ws_types_plus=ws_types_plus+ws_types
    ws_types_list = "('"+"','".join(ws_types_plus)+"')"

    #****************Main pages***********************************
    
    '''
    @main.route("ms_tree/<tree_id>/",  methods = ['GET','POST'])
    def ms_tree_page(tree_id):
         The MS Tree page with the id in URL - simply redirects to ms_tree
    
            * **url** /ms_tree/<tree_id>
            * **template** entero/templates/ms_tree/MSTree_holder.html
    
        try:
            tree_id = int(tree_id)
    '''
    
    @species.route('/index/<species>')
    @view_species_required
    def species_index(species):
        ''' The index page for each species
    
        * **url** /species/index/<species>
        * **template** entero/templates/species/species_index.html
        '''
        return get_species_index(species)
    
    
    '''
    this is used to select a folder programtically
    K.M. 22/10/2019    
    '''    
    @species.route('/<species>/folder/<folder_type>/')
    @view_species_required
    def specie_folder(species, folder_type):
        folder_name=request.args.get("folder_name","")
        if folder_type.lower() not in ('mine', 'public', 'shared'):
            flash("Parent folder (%s) is not found, it should be mine, public or shared "%folder_type)
            return redirect('species/index/%s'%species)    
        if not current_user.is_authenticated() and folder_type in ('shared', 'mine'):
            return redirect(url_for('auth.login', next=request.url))
            #flash("Parent folder (%s) is available only for logged in users"%folder_type)
            #return redirect('species/index/%s'%species)
        return get_species_index(species, folder_type=folder_type.lower(), folder_name=(folder_name))   
    
    def get_species_index(species, folder_type=None, folder_name=None):
        print "folder_type:  ", folder_type, folder_name
    
        logged_in = current_user.is_authenticated()
    
        dbase = get_database(species)
        if not dbase:
            raise Exception("database %s does not exist"%species)
        general_info = {}
        general_info['jobs_running'] =0
        general_info['my_strains']=0
        api_token = None
        workspace_own = 0
        workspace_shared = 0
        workspace_public = 0
        workspace_all=0
    
        try:
            if logged_in:
                if check_permission(current_user.id, 'api_access', species) or current_user.administrator == 1 :
                    api_token = current_user.generate_api_token(expiration=15768000)                
    
                workspace_info = db.session.query(UserPreferences.user_id).filter(UserPreferences.type.in_(ws_main_search_types),UserPreferences.database==species, or_(UserPreferences.user_id ==  current_user.id,UserPreferences.user_id==0)).all()     
    
                for rec in workspace_info:
                    if rec.user_id==0:
                        workspace_public+=1
                    else:
                        workspace_own+=1
                tags = db.session.query(BuddyPermissionTags).filter_by(buddy_id=current_user.id,species=species,field ='shared_workspaces').all()
                for tag in tags:
                    names = tag.value.split(",")                 
                    workspace_shared+=len(names)
                results = db.session.query(UserJobs.status, func.count(UserJobs.status)).filter(UserJobs.user_id == current_user.id, UserJobs.database==species).group_by(UserJobs.status).all()
                for r in results: 
                    if r[0].startswith('SUBMIT') or r[0].startswith('WAIT RESOURCE') or r[0].startswith('RUNNING')  or r[0].startswith("QUEUED"):
                        general_info['jobs_running'] += r[1]            
                general_info['my_strains']= dbase.session.query(dbase.models.Strains.id).filter_by(owner = current_user.id).count()
    
            else:
                workspace_public = db.session.query(UserPreferences.user_id).filter(UserPreferences.type.in_(ws_types),
                                                                                    UserPreferences.database==species,UserPreferences.user_id==0).count()
            workspace_all = workspace_own+workspace_public+workspace_shared
            general_info['workspaces']=[workspace_own,workspace_shared,workspace_public,workspace_all]
            general_info['total_strains'] = dbase.getStrainNumber()
            if general_info['total_strains']==None:
                flash("The database is not available now, please try later.")
                return redirect(url_for('main.index'))
    
            general_info['daily_add_list'] = daily_add_list = dbhandle[species].get_daily_increase(4)
            schemes =dbase.session.query(dbase.models.Schemes).all()
            scheme_info = []
            for scheme in schemes:
                summary_field = scheme.param.get('summary')
                if summary_field:
                    scheme_info.append([scheme.description,scheme.name,summary_field])
    
            general_info['schemes']=scheme_info
            return render_template('species/species_index.html',species=species,general_info=general_info,api_token=api_token, help = app.config['WIKI_BASE']+'/features/species-index.html',folder_type=folder_type, folder_name=folder_name)
        except Exception as e:
            dbase.rollback_close_session()
            app.logger.exception("Problem getting workspace info in index")
            flash("The database is not available now, please try later.")
            return redirect(url_for('main.index'))
            #return render_template('species/species_index.html',species=species,general_info=general_info)


@species.route('/<species>/search_strains',methods = ['GET','POST'])
@view_species_required
def search_strains(species):
    '''The main search page

    * **url** /species/<species>/search_strains
    * **template** entero/templates/species/view_edit_strains/edit_strain_metadata.html

    GET parameters
        * **query** a simple query to be run when the page loads It can have the following values

            * st_search - the page will open with the MLST Query Dialog open
            * strains:*field*:*value* - will perform a query on the strains databas with the field and value
            *e.g.* query=strains:country:France will open the page with all strains from France
            * scheme:*scheme_name*:*field*:*value* will perform a query on the scheme got get all strains with a the value for the field
            *e.g.* query=scheme:SeroPred:O:O86 wil open the page with all strains with a predicted Serotpye of O26

        *  **strain_ids** - a comma delimited list of strain ids to display on the page

    POST parameters
        * **strain_ds** see above - used when the number of ids will not fit in a GET request
   
   
   '''
    query = request.args.get("query")
    strain_ids = request.args.get("strain_ids","")
    if not strain_ids:
        strain_ids = request.form.get("strain_ids","")
    if not strain_ids :
        barcodes = request.args.get("barcodes","")
        if barcodes :
            import re
            def debarcode(s) :
                barcodes = s.split(',')
                ids = []
                for barcode in barcodes :
                    try :
                        codes = re.findall('[A-Z]{3}_([A-Z]{2})([0-9]{4})([A-Z]{2})', barcode)[0]
                        id = sum([ (ord(c)-65)*10000*(26**i) for i, c in enumerate(codes[0]+codes[2])]) + int(codes[1])
                        ids.append(str(id))
                    except :
                        pass
                return ','.join(ids)
            strain_ids = debarcode(barcodes)
                
        
    dbase=get_database(species)
    
    Schemes=dbase.models.Schemes
    try:
        schemes = dbase.session.query(Schemes).filter(Schemes.param['js_grid_file'].astext <>None).all()
        grid_files=set()
        for scheme in schemes:
            grid_files.add(scheme.param["js_grid_file"])

        species_label = app.config["ACTIVE_DATABASES"][species][0]
        data=""
        help = app.config['WIKI_BASE']+"/features/main-search-page.html"
        if (query):
            data = query
        return render_template('species/view_edit_strains/edit_strain_metadata.html',species=species,help=help,
                              data=data,species_label=species_label,strain_ids=strain_ids,grid_list=list(grid_files))
    except Exception as e:
        dbase.rollback_close_session()
        app.logger.exception("Error in search_strains for %s, error message: %s"%(species,e.message))
        flash("The database for %s is not available now, please try later."%species)
        return redirect(url_for('main.index'))


@species.route('/<species>/my_strains') 
@auth_login_required
@view_species_required
def my_strains(species):
    ''' Show users strains in the main search page
     
    * **url** /<species>/my_strains
    * **template** entero/templates/species/view_edit_strains/edit_strain_metadata.html
     '''
    help = app.config['WIKI_BASE'] + "/features/main-search-page.html"
    species_label = app.config["ACTIVE_DATABASES"][species][0]
    dbase = get_database(species)
    try:
        Schemes=dbase.models.Schemes
        schemes = dbase.session.query(Schemes).filter(Schemes.param['js_grid_file'].astext <>None).all()
        grid_files=set()
        for scheme in schemes:
            grid_files.add(scheme.param["js_grid_file"])
        return render_template('species/view_edit_strains/edit_strain_metadata.html',species=species,
                               data="my_strains",help=help,species_label=species_label,grid_list=list(grid_files))
    except Exception as e:
        dbase.rollback_close_session()
        app.logger.exception("Error while quering my_strains for database %s, error message: %s"%(species, e.message))
        flash("There are no user strains or the database is not available now, please try later")
        return (url_for('main.index'))


@species.route('/<species>/my_jobs')    
@auth_login_required
@view_species_required
def my_jobs(species,data='my_jobs'):
    ''' The Show My Jobs page
         
    * **url** /<species>/my_jobs
    * **template** entero/templates/admin/jobs_view.html
    '''
    return render_template("admin/jobs_view.html",species=species,data='my_jobs', help = app.config['WIKI_BASE']+'/features/jobs.html')


@species.route("/<species>/my_buddies",  methods = ['GET','POST'])
@auth_login_required
@view_species_required
def my_buddies(species):
    ''' The  My Buddies page
             
        * **url** /<species>/my_buddies
        * **template** entero/templates/auth/manage_buddies.html
        '''
    return render_template("auth/manage_buddies.html",species=species, help = app.config['WIKI_BASE']+'/features/buddies.html')


@species.route('/<species>/snp_project/<snp_id>/')
def snp_project(species,snp_id):
    ''' The  Snp project page - will load the snp project specified in the url

        * **url** /<species>/snp_project/<snp_id>
        * **template** entero/templates/species/snp_project.html
    '''
    permission=False
    snp_id=int(snp_id)
    snp_project = get_analysis_object(snp_id)
 
    #need to be logged in to access non public snp project
    if not current_user.is_authenticated() and snp_project.user_id <> 0:
        return redirect(url_for('auth.login',next=request.url))
    if not snp_project.has_permission(current_user):
        flash("You do not have access to this page, please contact an administrator.")
        return redirect(url_for('main.index')) 
    user_id=-1
    if current_user.is_authenticated(): 
        user_id=current_user.id
        #permission=check_permission(current_user.id, 'edit_strain_metadata', species)
        #if not permission:                         
        #if not permission:  
        permission=False
        if snp_project.data.get('assembly_ids'):
            assembly_ids=snp_project.data['assembly_ids']        
            database=get_database(species)
            if database and len(assembly_ids):
                Strains=database.models.Strains
                results= database.session.query(Strains.id).filter(Strains.best_assembly.in_(assembly_ids)).all()                       
                ids=[]                    
                ids.append(snp_project.ref_id)
                for res in results:
                    ids.append(res[0])
                permission=get_access_permission(species, user_id, strains_ids=ids)      
    
    summary = snp_project.get_summary()
    if summary.get("parameters"):
        if summary['parameters'].get('SNP List') and summary['parameters']['SNP List'] =='None':
            del summary['parameters']['SNP List']
    jbrowse_uri = None
    if snp_project.data.get("jbrowse_uri"):
        jbrowse_uri = snp_project.data.get("jbrowse_uri")+"&tracks=snp_"+str(snp_id)
    status="running"
    if snp_project.data.get("complete"):
        status="complete"
    elif snp_project.data.get("failed"):
        status="failed"
    microreact = snp_project.data.get("microreact_data")
    microreact_url=""
    if microreact:
        microreact_url = microreact['url']    
    return render_template("species/snp_project.html",summary=summary,project_name=snp_project.name,
                                             species=species,project_id=snp_id,jbrowse_uri=jbrowse_uri,status=status,microreact_url=microreact_url,permission=permission
                                             )
    

@species.route('/<species>/upload_reads')
@auth_login_required
@view_species_required
def add_uploaded_reads(species):
    ''' The  upload reads page
                     
    * **url** /<species>/upload_reads
    * **template** entero/templates/species/add_data/add_upload_reads.html
    '''
    help = app.config['WIKI_BASE']+"/features/add-upload-reads.html"
    recs= db.session.query(UserPermissionTags.value).filter_by(user_id=current_user.id,field="upload_read_allowed").all()
    read_types= []
    for rec in recs:
        read_types.append(rec.value)
    return render_template('species/add_data/add_upload_reads.html',species=species,help=help,reads_allowed=",".join(read_types))    


@species.route("/<species>/locus_search",  methods = ['GET','POST'])
@auth_login_required
@view_species_required
def locus_search(species):
    ''' The  locus search page
                       
    * **url** /<species>/locus_search
    * **template** entero/templates/species/locus_search.html
    '''
    return process_locus_accessory(species,"Locus Search")
   
@species.route("/<species>/locus_search/<ls_id>",  methods = ['GET','POST'])
@view_species_required
def locus_search_with_param(species,ls_id):
    ''' The accessory genome  page (automatically loads the accessory genome
    of the workspace specified in the URL)

    * **url** /<species>/locus_search/<ls_id>
    * **template** entero/templates/species/locus_search.html
    '''
    associated_data = request.args.get("data")
    ws_id = 0
    
    permission = True
    if associated_data:
        ws_id= int(associated_data)
        ws= get_analysis_object(ws_id)
        if not ws:
                return "Analysis not Found"
        if not ws.has_permission(current_user):
            permission = False
        
    ls_id= int(ls_id)
    ls = get_analysis_object(ls_id)
    
    if  not ls:
        return "not found"
    if not ls.has_permission(current_user):
        permission = False
    
    if not permission:
        if not current_user.is_authenticated():
            return redirect(url_for('auth.login',next=request.url))
        else:
            flash("You do not have access to this page please contact an administrator if you think you should have permission.")
            return redirect(url_for('main.index'))          
    
    return process_locus_accessory(species,"Locus Search",analysis_id=ls_id,link_id=ws_id)


@species.route("/<species>/accessory_genome",  methods = ['GET','POST'])
@view_species_required
def accessory_genome(species):
    ''' The  accessory genome page
                           
    * **url** /<species>/accessory_genome
    * **template** entero/templates/species/locus_search.html
    '''
    return process_locus_accessory(species,"Accessory Genome")

@species.route("/<species>/accessory_genome/<ws_id>",  methods = ['GET','POST'])
@view_species_required
def accessory_genome_with_param(species,ws_id):
    ''' The  locus search page (automatically loads the specified search)
    and will add any strains that are specified in a workspace  whose
    id can be specified in the GET parameter 'data'
                           
    * **url** /<species>/locus_search/<ls_id>
    * **template** entero/templates/species/locus_search.html
    '''
    try:
        ws=get_analysis_object(int(ws_id))
        if not ws.has_permission(current_user):
            flash("You do not have access to this page, please contact an administrator.")
            return redirect(url_for('main.index'))
        
        return process_locus_accessory(species,"Accessory Genome",analysis_id=ws_id)
    except Exception as e:
        return "Not Found"

def process_locus_accessory(species,temp_type,analysis_id =0,link_id=0):
    '''Helper method which processes requests for both locus search and accessory genome
    page, which all use the same template'''
    allowed_schemes=[]
    if current_user.is_authenticated():
        allowed_schemes=current_user.get_allowed_schemes(species)
    dbase =get_database(species)
    Schemes = dbase.models.Schemes
    try:
        schemes = dbase.session.query(Schemes.description,Schemes.name,Schemes.param).filter(Schemes.param['scheme']<>None).all()
        scheme_info = {}
        for scheme in schemes:
            if not scheme.name.startswith("CRISP"):
                if scheme.param['display']=='public' or scheme.description in allowed_schemes:
                    scheme_info[scheme.description]=scheme.name

        gene_categories = app.config['GENE_CATEGORIES'].get(species,[])


        return render_template("species/locus_search.html", species=species,scheme_info=scheme_info,temp_type=temp_type,
                               gene_categories=gene_categories,analysis_id=analysis_id,link_id=link_id, help = app.config['WIKI_BASE']+'/features/locus-search.html')
    except Exception as e:
        dbase.rollback_close_session()
        app.logger.exception("Error in process_locus_accessory for species %s, error message: %s"%(species, e.message))
        flash("Error in the request for %s and/or the databas is not avilable now, please try later "%species)
        return redirect(url_for('main.index'))




@species.route("/<species>/download_data",  methods = ['GET','POST'])
@view_species_required
def download_data(species):
    ''' The  schemes download page
                               
    * **url** /<species>/download_data
    * **template** entero/templates/auth/download_data.html
    '''
    dbase = get_database(species)
    try:
        Schemes = dbase.models.Schemes
        # Only public schemes at the moment
        scheme_query =   dbase.session.query(Schemes).filter(Schemes.param['display'].astext=='public',Schemes.param['pipeline'].astext=='nomenclature')
        if current_user.is_authenticated() and current_user.administrator == 1 :
                schemes = scheme_query.all()
        else:
            schemes = scheme_query.filter(Schemes.description != 'rMLST').all()
        scheme_list = []
        for scheme in schemes :
            temp_scheme = scheme.as_dict()
            temp_scheme['pipeline_scheme'] = scheme.get_param('scheme').split('_')[1]
            scheme_list.append(temp_scheme )

        return render_template("auth/download_data.html", species=species, schemes =scheme_list,database=species)
    except Exception as e:
        dbase.rollback_close_session()
        app.logger.exception(
            "Error in download_data for species %s, error message: %s" % (species, e.message))
        flash("Error in the request for %s and/or the databas is not avilable now, please try later " % species)
        return redirect(url_for('main.index'))





#*************Legacy Web Pages************************************

@species.route("/<species>/allele_st_search")
def allele_st_search(species):
    '''The old main page in the MLST web page

    * **url** /<species>/allele_st_search
    * **template** entero/templates/species/allele_st_query.html
    '''
    dbase = get_database(species)
    if not dbase:
        return "database does not exist"
    scheme= 'MLST_Achtman'
    sql = "SELECT  schemes.param->>'scheme' AS scheme,data_param.name as locus_name,data_param.label AS locus_label FROM schemes INNER JOIN data_param ON tabname = '%s' AND schemes.description = '%s' AND group_name = 'Locus' ORDER BY nested_order" %(scheme,scheme)
    results = dbase.execute_query(sql)
    alleles=[]
    for line in results:
        alleles.append(line['locus_name'])
    scheme_url = results[0]['scheme'].replace("_","/")
    if species =='ecoli':
        database_name = "Escherichia coli"
    elif species == 'senterica':
        database_name ="Salmonella enterica"
    elif species == 'yersinia':
        database_name="Yersinia pseudotuberculosis"
    elif species == 'mcatarrhalis':
        database_name ='Moraxella catarrhalis'
    return render_template("species/allele_st_query.html",species=species,alleles=alleles,database=species,database_name =database_name,scheme_url=scheme_url)






@species.route("/<species>/download_7_gene",methods=(["GET","POST"])) 
def download_7_gene(species) :
    '''The old MLST download page
   
       * **url** /<species>/download_7_gene
       * **template** entero/templates/species/download_7_gene.html
    '''
    dbase = get_database(species)
    scheme= 'MLST_Achtman'
    sql = "SELECT  schemes.param->>'scheme' AS scheme,data_param.name as locus_name,data_param.label AS locus_label FROM schemes INNER JOIN data_param ON tabname = '%s' AND schemes.description = '%s' AND group_name = 'Locus' ORDER BY nested_order" %(scheme,scheme)
    links= []
    results = dbase.execute_query(sql)
    for line in results:
        arr = line['scheme'].split("_")
        database=arr[0]
        scheme=arr[1]
        data=line['locus_name']
        obj={"name":line['locus_label']}
        href = "/species/%s/download_alleles/%s.fas" % (species,data.upper())
        links.append({"name":data+".fas","href":href})
   
    href = "/species/%s/download_alleles/publicSTs.txt" % species
    links.append({"name":"STs.csv","href":href})
    return render_template("species/download_7_gene.html",links=links,species=species,database=species)



@species.route("/<species>/mlst_legacy_info",  methods = ['GET','POST'])
def mlst_legacy_info(species):
    '''Page explaining the switch from the old MLST site to Enterobase
   
       * **url** /<species>/mlst_legacy_info
       * **template** entero/templates/species/mlst_legacy_info.html
       '''    
    dbase = dbhandle[species]    
    return render_template("species/mlst_legacy_info.html", species=species,database=species)

#*************End of Legacy Web Pages************************************




#**************Methods*****************************************************

#*********Curation Methods**************************************************
@species.route('/<database>/assembly_status',methods = ['GET','POST'])
@user_permission_required("change_assembly_status")
def update_assembly_status(database):
    '''Updates the assembly status of the specified assemlblies. If an assemnbly's
    status is changed to Assembled then all scheme jobs for this assembly
    will be sent.Returns a json dictionary conatining the key status with either OK or Error e.g. {"status":"Error"}

    * **url** <species>/assembly_status

    POST parameters
        * **status** The new status if the Assembly either Assembled, Failed QC or Comtaminated or Poor Quality
        * **ids** a comma delimited list of strain ids to change
        '''
    dbase = get_database(database)
    try:

        Strains = dbase.models.Strains
        Assemblies = dbase.models.Assemblies 
        AssemblyLookup = dbase.models.AssemblyLookup
        Schemes = dbase.models.Schemes
        for strain_id in request.form['ids'].split(','):
            assembly = dbase.session.query(Assemblies).filter(Strains.id == int(strain_id)).join(Strains, Strains.best_assembly == Assemblies.id).one()
            assembly.status = request.form['status']
            dbase.session.add(assembly)
            if assembly.status == 'Assembled':
                send_all_jobs_for_assembly(assembly,database)
            else: 
                dbase.session.query(AssemblyLookup).filter(AssemblyLookup.assembly_id == assembly.id).delete()
                if assembly.status == 'Delete Assembly':
                    dbase.session.delete(assembly)
            dbase.session.commit()
        return json.dumps({"status":'OK'})
    except Exception as e:
        dbase.rollback_close_session()
        app.logger.exception("Error in update_assembly_status for %s, error message: %s"%(database, e.message))
        return json.dumps({"status":"Error","msg":"There was an error"})

@species.route('/<database>/change_owner',methods = ['GET','POST'])
@user_permission_required("change_strain_owner")
def change_owner(database):
    '''Changes the owner of the specified strains to the specified owner. Returns 
    a json dictionary containing the key status, with either OK or Error e.g. {"status":"OK"}

    * **url** /<species>/change_owner

    POST parameters
        * **user_id** The id of the new owner of the strains
        * **ids** a comma delimited list of strain ids to change
    '''    
    dbase = get_database(database)
    user_id = int(request.form.get("user_id"))
    id_array = request.form.get('ids').split(',')
    if len(id_array) < 1: return ujson.dumps("No strains have been selected ")
   
    try:
        Strains = dbase.models.Strains
        strains = dbase.session.query(Strains).filter(Strains.id.in_(id_array)).all()
        for strain in strains:
            strain.owner = user_id
            dbase.session.add(strain)
        dbase.session.commit()
        return ujson.dumps({"status":"OK"})        
    except Exception  as e:
        dbase.rollback_close_session()
        app.logger.exception("Error in change_owner for %s, error message: %s" % (database, e.message))
        return ujson.dumps({"status":"Error"})
    

#*********Legacy Methods**************************************************
@species.route("/<species>/legacy_allele_call",methods = ['POST'])
def legacy_allele_call(species):
    '''Mimics the old allele call (although if an allele sequence is submitted - an
    allele id is returned only if is is a complete match)

    * **url** /species/<species>/legacy_allele_call
    Returns the allele id if a sequence was given  e.g. {"allele_id":363} or
    a dictionary contianing allele values,differences, st complex and st e.g.

    .. code-block:: json

        [
            {
                "alleles" [2, 1, 59, 2, 2, 2, 2],
                "differences'" 2,
                "st_complex": "None",
                "st": 146
            },
            ..
        
    POST parameters
        *  **scheme_url** nserve's scheme name e.g Escherichia/UoW for Escherichia_UoW
        * **sequence** the allele sequence
        * **locus**   the name of the locus
        * **alleles** a json dictionary of loci:alelle value
        * **identical_alleles** the number of alleles that must be identical in the match
        * **limit** - the numer of matches to return
    '''
    try:
        scheme_url = request.form.get("scheme_url")
        sequence = request.form.get("sequence")
        if sequence:
            locus = request.form.get("locus")
            url="%s/search.api/%s/alleles" %(app.config['NSERV_ADDRESS'],scheme_url)
            params = {"locus":locus,"fieldnames":"allele_id","seq":sequence}
            resp = requests.post(url,data=params,timeout=app.config['NSERV_TIMEOUT'])
            if resp.text:
                try:
                    data = ujson.loads(resp.text)
                    print "Data: ", data
                except Exception:
                    raise Exception("Response from the srever %s could not be loadded"%resp.text)
                if len(data)==0:
                    allele_id=0
                else:
                    allele_id = data[0]['allele_id']
                return ujson.dumps({"allele_id":allele_id})
            else:
                app.logger.error('No response from NSERV for %s %s' %(url,params))
                return make_response('No response from NServ', 422)
        if not request.form.get("alleles"):
            app.logger.error("Error in legacy allele call, error message: alleles value is needed")
            return ujson.dumps([])
        allele_info = ujson.loads(request.form.get("alleles"))
        if not request.form.get("limit") or not request.form.get("limit").isdigit():
            #if the user does not provide the limit so we will return him everything
            limit=None
            #app.logger.error("Error in legacy allele call, error message: integer for limit attribute is needed")
            #return ujson.dumps([])
        else:
            limit = int(request.form.get("limit"))
        if not request.form.get("indentical_alleles") or not request.form.get("indentical_alleles").isdigit():            
            raise Exception ("No indentical_alleles field is provided or it is not a number, extracted value is: %s"%request.form.get("indentical_alleles"))
        identical_alleles=int(request.form.get("indentical_alleles"))
        allele_no = len(allele_info)
        dist_max= allele_no-identical_alleles
        allele_list = ""
        for allele in allele_info:
            allele_list += "&"+allele+"="+str(allele_info[allele])
        
        url="%s/match.api/%s/type_search?fieldnames=value_indices,info%s" % ( app.config['NSERV_ADDRESS'],scheme_url,allele_list)
        if dist_max <> 0:
            url+="&distmax=%i" % dist_max
        if limit:
            url=url+"&limit=%i" % limit
        resp = requests.get(url=url,timeout=app.config['NSERV_TIMEOUT'])
        try:
            data = ujson.loads(resp.text)
        except Exception as e:
            raise Exception ("Errro while loading json from NServ call (%s) response, resp is: %s"%(url, resp.text))
        ret_value =[]
        count=1
        if isinstance(data, unicode) or isinstance( data, int):
            return json.dumps(ret_value)
        for entry in data:
            alleles={}
            st = entry['ST_id']
            if st <1:
                continue
            for a in entry['alleles']:
                alleles [a['locus']]=a['allele_id']
                #alleles.append(a['allele_id'])
            if  len(alleles)<7:
                continue
            st_complex="None"
            info = entry.get('info')
            if info:
                if info.get("st_complex"):
                    st_complex= info['st_complex']
            ret_value.append({"alleles":alleles,"st":st,"st_complex":st_complex,"differences":allele_no-entry['matches']})
            if limit:
                if count==limit:
                    break
            count+=1
        return ujson.dumps(ret_value)
    except  requests.exceptions.ConnectTimeout  as e:
        app.logger.exception("legacy allele call failed, server is not avalable, error message: %s"%e.message)
        return ujson.dumps("Failed")
    except Exception as e:
        app.logger.exception("Error in legacy allele call, error message: %s"%e.message)
        return ujson.dumps("Failed")
    
@species.route('/<species>/assembly_location',methods = ['GET', 'POST'])
def get_assembly_location(species):
    dbase = get_database(species)
    if not dbase:     
        message="No database found for the provided species: %s"%species
        app.logger.error(message)
        return make_response(message)
    barcodes = request.args.get("barcodes")
    if barcodes:               
        try:
            Strains = dbase.models.Strains
            Assemblies = dbase.models.Assemblies              
            barcode_list = barcodes.split(',')
            search = None
            if barcode_list[0].endswith('_AS'):
                search = dbase.session.query(Assemblies.barcode, Assemblies.file_pointer)\
                    .filter(Assemblies.barcode.in_(barcode_list)).all()
            else:
                search = dbase.session.query(Assemblies.barcode, Assemblies.file_pointer)\
                    .filter(Strains.barcode.in_(barcode_list))\
                    .filter(Strains.best_assembly == Assemblies.id).all()
            return jsonify(search)
        except Exception as e:        
            app.logger.exception("Error in get_assembly_location for database: %s, error message: %s, barcodes: %s"%(species,e.message, barcodes))
            return make_response('Error') 
    else:
        return make_response('No barcodes submitted')

@species.route('/<species>/get_metadata/<scheme>/<fieldname>',methods = ['GET', 'POST'])
def get_metadata(species=None, scheme='MLST_Achtman', fieldname='serotype'):
    dbase = get_database(species)
    if not dbase:     
        message="No database found for the provided species: %s"%species
        app.logger.error(message)
        return make_response(message)    
    
    Strains = dbase.models.Strains
    Assemblies = dbase.models.Assemblies 
    AssemblyLookup = dbase.models.AssemblyLookup
    Schemes = dbase.models.Schemes
    scheme_id, scheme_desc = dbase.session.query(Schemes.id, Schemes.description).filter(Schemes.description.like('%{0}%'.format(scheme))).order_by(Schemes.id.desc()).first()
    search = [dict(st_barcode= x[0], scheme=scheme_desc, metadata=x[2])\
              for x in dbase.session.query(AssemblyLookup.st_barcode, AssemblyLookup.scheme_id, Strains.__table__.c[fieldname])\
              .filter(AssemblyLookup.st_barcode != 'EGG_ST')\
              .filter(AssemblyLookup.scheme_id == scheme_id)\
              .join(Assemblies).join(Strains).all()]
    with open('/share_space/interact/metadata.csv','w') as f:
        f.write('scheme,metadata,st_barcode\n')
        for j in search:
            j['metadata'] = '' if not j['metadata'] else j['metadata'].encode('ascii', 'ignore')
            f.write('%s\n' %'\t'.join(j.values()))
        f.close()
    return send_file('/share_space/interact/metadata.csv')

@species.route('/<species>/get_serotype',methods = ['GET', 'POST'])
def get_serotype(species):
    dbase = get_database(species)
    if not dbase:     
        message="No database found for the provided species: %s"%species
        app.logger.error(message)
        return make_response(message)    
    
    Strains = dbase.models.Strains
    Assemblies = dbase.models.Assemblies 
    AssemblyLookup = dbase.models.AssemblyLookup
    Schemes = dbase.models.Schemes
    scheme_lookup = {x[0]: x[1] for x in dbase.session.query(Schemes.id, Schemes.description)\
                     .filter(Schemes.description.in_(['MLST_Achtman', 'rMLST'])).all()}
    search = [dict(st_barcode= x[0], scheme=scheme_lookup[x[1]], serotype=x[2])\
              for x in dbase.session.query(AssemblyLookup.st_barcode, AssemblyLookup.scheme_id, Strains.serotype)\
              .filter(AssemblyLookup.st_barcode != 'EGG_ST')\
              .filter(Strains.serotype != None)\
              .filter(AssemblyLookup.scheme_id.in_(scheme_lookup.keys()))\
              .join(Assemblies).join(Strains).all()]
    with open('/share_space/interact/serotype.csv','w') as f:
        f.write('st_barcode,scheme,serotype\n')
        for j in search:
            print j.values()
            j['serotype'] = j['serotype'].encode('ascii', 'ignore')
            f.write('%s\n' %','.join(j.values()))
        f.close()
    return send_file('/share_space/interact/serotype.csv')

@species.route("/<species>/download_alleles/<locus>")
def download_alleles(species,locus):
    '''Gets all the MLST Achtman allele profiles for all the STs in a format that mimics the old MLST website
    the list is returned as HMTL. The species and locus are specified in the url
    
    * **url** <species>/download_alleles/<locus>
    '''
    dbase = get_database(species)

    try:
        print species, locus
        

        scheme= 'MLST_Achtman'
        sql = "SELECT  schemes.param->>'scheme' AS scheme  FROM schemes WHERE description='MLST_Achtman'"
        results = dbase.execute_query(sql)
        folder = results[0]['scheme'].replace("_",".")
        address=results[0]['scheme'].replace("_","/")
        location = r'/share_space/interact/NServ_dump/%s/' %(folder)        
        if locus == "publicSTs.txt":
            arr=[]
            if species =='senterica'or species =='ecoli':
                st_complex_name = "EBG"
                if species=='ecoli':
                    st_complex_name='ST Complex'
                #url = "%s/search.api/%s/types?type_id=%%20%%3E%%200&flag=1&convert=0&fieldnames=type_id,value_indices,info&sub_fieldnames=value_id,fieldname"% (app.config['NSERV_ADDRESS'],address)
                url = "%s/search.api/%s/types?type_id=%%20%%3E%%200&convert=0&fieldnames=type_id,value_indices,info&sub_fieldnames=value_id,fieldname"% (app.config['NSERV_ADDRESS'],address)
                resp = requests.get(url=url,timeout=app.config['NSERV_TIMEOUT'])
                try:                    
                    data =ujson.loads(resp.text)
                    print len (data)
                except:
                    raise Exception("Error in NSERV ADDRESS server response, response is: %s" % resp.text)

                data.sort(key = lambda x:int(x['type_id']))
                loci= data[0]['fieldvalues']
                loci_names=[]

                for val in loci:
                    loci_names.append(val['fieldname'])
                no_loci= len(loci_names)
                #soert the header
                loci_names.sort()
                header= "ST\t"+st_complex_name+"\t"+"\t".join(loci_names)
                arr.append(header)                
                for entry in data:                    
                    st_complex=''
                    info = entry.get('info')
                    if info:
                        st_complex = info.get("st_complex","")
                    line = "%s\t%s\t" % (entry['type_id'],st_complex)
                    if len(entry['fieldvalues'])<no_loci:                        
                        continue 
                    not_include=False
                    con=0                    
                    for name in loci_names:
                        value=0
                        con+=1
                        #Search for the column name to get the value
                        for x in entry['fieldvalues']:
                            if x['fieldname']==name:
                                value=x['value_id']
                                break
                        if value<=0:
                            not_include=True
                            break
                        if con==1:
                            line=line+str(value)
                        else:
                            line=line+'\t'+str(value)
                    
                    #alleles = reduce(lambda x,y: str(x)+"\t"+str(y),map(lambda x:x['value_id'],entry['fieldvalues']))
                    if not_include:                       
                        continue
                    arr.append(line)#+alleles)
            else:
                #location+="profiles.list.gz"
                location = os.path.join(location, "profiles.list.gz")
                if not os.path.exists(location):
                    if not os.path.exists(location):
                        raise Exception("%s is not found, for species %s and locus %S"%(location, species, locus))
                    #app.logger.warning("%s is not found" % location)
                    #return
                with gzip.open(location) as f:

                    for line in f:
                        line=line.strip()
                        if line.startswith("-"):
                            continue
                        arr.append(line)

            return "<pre style='word-wrap: break-word; white-space: pre-wrap;'>"+"\n".join(arr)+"</pre>"

        else:
            if species =='mcatarrhalis' and locus=='GLYBETA.fas':
                begin='glyBeta'
            else:       
                locus = locus.split(".")[0]                
                begin = locus[0:3].lower()
                if len(locus)==4:
                    begin+=locus[3]                
            location=os.path.join(location,(begin+".fasta.gz"))
            #location=location+begin+".fasta.gz"
            if not os.path.exists(location):
                message = ("Errro is download_alleles, %s is not found, for species %s and locus %s" % (location, species, locus))
                app.logger.error(message)
                return "<pre style='word-wrap: break-word; white-space: pre-wrap;'></pre>"

            arr =[]
            with gzip.open(location) as f:
                for line in f:
                    line = line.strip()
                    if line.startswith(">"):

                        arr.append(line.upper().replace("_",""))
                    else:
                        arr.extend([line[i:i+60] for i in range(0, len(line), 60)])

            arr.append("<pre>")
            return "<pre style='word-wrap: break-word; white-space: pre-wrap;'>"+"\n".join(arr)+"</pre>"
    except Exception as e:
        app.logger.exception("Error in download_alleles, error message: %s"%e.message)
        flash("Error in the request could not find the file")
        return "<pre style='word-wrap: break-word; white-space: pre-wrap;'></pre>"
#***************Methods For Species Index***************************************************************
@species.route("/get_scheme_pie_chart",methods = ['GET','POST'])
def get_scheme_pie_chart():
    '''Gets data to display in the scheme pie chart in the species index page. Returns data in the following format 
     
      .. code-block:: json

          {
                "field_label": "O Antigen",
                "list": [ ["O157", 6648L], ["O25", 4207L],...,["other", 12422L]],
                "field_name": "O"
            }
        
    * **url** /species/get_scheme_pie_chart

    POST parameters
        * **field** the name of the fields e.g. st
        * **database** the name of the database
        * **schene** the name(description) of the scheme e.g. rMLST
    '''
    scheme = request.form['scheme']
    database = request.form['database'] 
    field = request.form['field']
    ret_list =  query_function_handle[database][scheme](database,scheme,field,"summary_breakdown")
    return json.dumps(ret_list)


@species.route("/get_strain_pie_chart",methods = ['GET','POST'])
def get_strain_pie_chart():
    '''Gets data to display in the strain pie chart in the species index page. Returns data in the form of 
    list of dictionaries conatining the category and number in that category (in order )in the following format
    
    .. code-block:: json

        [
            {"y": 22018, "indexLabel": "United States", "legendText": "United States"},
            {"y": 9045, "indexLabel": "United Kingdom", "legendText": "United Kingdom"}
        ]

    * **url** /species/get_strain_pie_chart

    POST Request
        * **field** the name of the fields e.g. country
        * **database** the name of the database
    '''
    pie_list = []
    field = request.form['field']
    database = request.form['database']
    dbase = get_database(database)
    if not dbase or not field:
        app.logger.error("get_strain_pie_chart failed, databas and/or filed are missing, database: %s, field: %s"%(database, field))
        return json.dumps(pie_list)
    data=dbase.getStrainInfo(field)
    #data = getStrainInfo(database,field)
    count =0

    other=0
    non_specified=0
    for record in data:
        if count==20:
            other+=int(record["num"])
            continue
        if record[field]==None or record[field]=="" or record[field]=='ND' :
            non_specified += int(record['num'])
            continue
        pie_list.append({
            "y":record['num'],
            "legendText":record[field],
            "indexLabel":record[field]
        })
        count+=1
    if other:
        pie_list.append({
            "y":other,
            "legendText":"Other",
            "indexLabel":"Other"
        });
    if non_specified:
        pie_list.append({
            "y":non_specified,
            "legendText":"Unspecified",
            "indexLabel":"Unspecified"
        });    
    return json.dumps(pie_list)



@species.route("/get_database_info",methods = ['GET','POST'])
def get_database_info():
    '''Returns information about the strain (and traces) fields that can be displayed in the
    left hand pie chart of the species index page and data for the graph showing short reads
    
    Returns a dictionary with the following entries:-
    
    * **cum_totals**  - a list of lists each containig the month and the cumulative total of reads e.g
    
    .. code-block:: json

        [
            ["02-12",4],
            ["03-12",5],
            ........,
            ["18-01",346374]
        ]

    * **fields** -  a list of dictionaries with the field name and label that can loaded into the pie chart e.g. 

    .. code-block:: json

        [
            {"name": "source_niche", "label": "Source Niche"},
            {"name": "contact","label": "Lab Contact"},
               .....
        ]

    * **url** /species/get_database_info

    GET parameters
        * **database** the name of the database

    '''
    database = request.form['database']
    allowed_params = ["continent","country","region","city","contact"
                      ,"source_niche","source_type","source_details"
                      ,"serotype","species","seq_platform"]
    dbase = get_database(database)
    if not dbase:
        app.logger.warning("get_database_info failed, could not find %s database"%database)
        return json.dumps({"cum_totals": [], "fields": []});
    try:
        cum_data = dbase.get_cumulative_strain_number()
        params = dbase.get_dataparam('strains')
       
        fields = []
        for param in params:
            if param['name'] in allowed_params:
                fields.append({"name":param['name'],"label":param['label']})
        params = dbase.get_dataparam('traces')
        for param in params:
                if param['name'] in allowed_params:
                    fields.append({"name":param['name'],"label":param['label']})
        
        return json.dumps({"cum_totals":cum_data,"fields":fields});
    except Exception as e:
        app.logger.exception("Error in getting database info, error message: %s"%e.message)
        return json.dumps({"cum_totals":[],"fields":[]});
    

#***************Methods For the Main Search Page***************************************************************

@species.route("/<species>/get_saved_strain_queries",methods = ['GET','POST'])
def get_saved_strain_queries(species):
    '''Returns a list of the names of the users saved queries (for the main search_strains page)

    * **url** /species/<species>/get_saved_strain_queries
    '''
    try:
        if not current_user.is_authenticated():
            return ujson.dumps([])
        queries = db.session.query(UserPreferences).filter_by(database=species,type='main_query',user_id=current_user.id).all()
        query_list = []
        for query in queries:
            query_list.append(query.name)
        return ujson.dumps(query_list)
    except Exception as e:
        db.session.rollback()
        app.logger.exception("Error in get_saved_strain_queries, error message: %s" % e.message)
        return json.dumps([]);

@species.route('/<species>/get_strain_columns',methods = ['GET','POST'])
def get_strain_columns(species):
    '''Returns the field information for the strains grid in a format required fo the
    setMetaData method of the JavaScript StrainValidationGrid 
    
    * **url** /species/<species>/get_strain_columns
    '''
    data = []
    dbase  = get_database(species)
    if not dbase:
        return("No valid database '%s' is provided"%species)
    param = dbase.models.DataParam
    try:
        results = dbase.session.query(param).filter(param.tabname.in_(['strains','traces']))\
            .order_by(param.display_order, \
                      param.nested_order).all()
        nested = []
        prevNestedOrder=0
        for res in results:
            try :
                no = int(res.nested_order)
            except :
                no = 0
                
            if no:
                if no> prevNestedOrder:
                    nested.append( res.as_dict() )
                else:
                    data.append( nested )
                    nested = []
                    nested.append(res.as_dict())
            else:
                if len(nested) > 0:
                    data.append( nested )
                    nested=[]
                data.append(res.as_dict())
            prevNestedOrder=no
        if len(nested) > 0:
            data.append( nested )

        dbase.session.close()
        return ujson.dumps(data)
    except Exception as e:
        dbase.rollback_close_session()
        app.logger.exception("Error in get_strain_columns for %s, error message: %s" % (species,e.message))
        return ujson.dumps([])



@species.route("/get_user_workspaces",methods = ['GET','POST'])
def get_user_workspaces():
    '''Gets information on a user's workspace (analysis types)or just public workspaces if
    the user is not logged in. Thethe data is suitable to display in the JavaScript EnteroWorkspaceDialog Object

    * **url** /species/get_user_workspaces
    
    Returns (in json format) a dictionary  with the following entries :-
    
    * **saved workspaces** -all the workspaces an a user has access to in the form of 
    a list of lists, each describing the workspace in the format of  of a list with name,owner_id,owner_name,id,type

    .. code-block:: json

        [
            ["Para C Lineage",0,"public'"3246,"main_workspace"],
            ["Tennessee_cgMLST_V2","mine","mine",2553,"ms_tree"],
            ["ST_rep_4237",76,"nabil",2211,"snp_project"],
            .............
        ]

    * **workspace_folders** - the folder layout of the users workspaces (see bitbucket for the structure)
    * **buddy_folders** - the folder layout for shared folders (each buddy  has a folder with the workspaces they have shared in it)
    * **public folders** - the slayout of all public folders
    * **logged_in** - boolean
    * **is_administrator** If True the user can alter the layout of public folders (admin only)
    * **workspace_types** - The ANALYSIS TYPES in app.config, which describes each type
    *  **user_detals** -  a list comprising user_id,username

    GET Parameters
        * **database** the name of the database
    '''
    try:
        dbname = request.args.get('database')
        workspaces = []
        ws_map_id = {}
        logged_in=False
        administrator=False
        buddy_folders=None
        workspace_folders=None
        public_folders=None      
        buddies_foler_ws=[]


        if current_user.is_authenticated():
            #get buddy edit permission to be used to determin if the user has edit permission or not to workspaces
            SQL="select buddy_permission_tags.user_id, buddy_permission_tags.value from buddy_permission_tags where field='%s' and buddy_id=%i and species='%s'"%('edit_strain_metadata',current_user.id,dbname)
            buddy_edit_permission={}            
            results=query_system_database(SQL)
            for res in results:
                buddy_edit_permission[res['user_id']]=res['value']                
            print buddy_edit_permission            
            logged_in =True
            user_details=[current_user.id,current_user.username]
            if current_user.administrator:
                administrator = True
            #get all shared workspace ids and link to buddy name and  buddy id            
            sql = "SELECT buddy_permission_tags.value as ws_id ,buddy_permission_tags.field, buddy_permission_tags.user_id AS ws_owner_id, users.username AS ws_owner_name FROM"+\
                " buddy_permission_tags INNER JOIN users ON buddy_permission_tags.user_id = users.id WHERE buddy_id =%i AND species='%s' AND field IN %s" % (current_user.id,dbname,ws_types_list)
            tags = query_system_database(sql)   
            buddy_workspaces = []
            buddy_dict={}
            buddy_sub_folders=[]            
            buddy_folders={"RN":{"text":"Root","id":"RN","workspaces":[],"children":buddy_sub_folders}}
            for tag in tags:
                name =tag['ws_owner_name']
                
                if tag.get('field')=='workspace_folders':
                    ## add shared folders and its workspaces
                    ## not completed yet
                    shared=json.loads(tag.get('ws_id'))
                    shared_folder=shared.get('shared_folder')            
                    shared_value=shared_folder[1]#buddy_sub_folders
                    vv=shared_value.split('/')
                    shared_id=shared_folder[0]
                    value =shared_id+"_"+str(tag.get('ws_owner_id'))                    
                    budy_shared_foler, work_spaces=get_shared_folders(dbname, tag.get('ws_owner_id'), shared_folder[0], shared_folder[1], str(tag.get('ws_owner_id')))
                    if  budy_shared_foler:                                     
                        text=vv[-1]                    
                        buddy_sub_folder= buddy_folders.get(name)
                        if not buddy_sub_folder:
                            buddy_sub_folder={"text":name,"id":name,"workspaces":[],"children":[]}
                            buddy_folders[name]=buddy_sub_folder
                            buddy_sub_folders.append(name)                                                
                        buddy_sub_folder.get('children').append(value)   
                        buddy_shared_folder={"text":text,"id":value,"workspaces":[],"children":[]}
                        buddy_shared_folder[name]=[tag['ws_owner_id'],name] 
                        buddy_folders.update(budy_shared_foler)
                        buddy_workspaces=buddy_workspaces+work_spaces
                        for ws_ in work_spaces:
                            buddy_dict[ws_]=[tag['ws_owner_id'],name]

                    continue
                else:
                    try:
                        value = int (tag['ws_id'])
                    except Exception as e:
                        print "Errors: ", e.message
                        continue
                buddy_workspaces.append(value)
                
                buddy_dict[value]=[tag['ws_owner_id'],name]
                #create buddy subfolder if does not exist - folder id and text is the buddy user name
                buddy_sub_folder= buddy_folders.get(name)
                if not buddy_sub_folder:
                    #buddy_sub_folder={"text":name,"id":name,"workspaces":[],"children":[]}
                    buddy_sub_folder={"text":name,"id":name,"workspaces":[],"children":[]}
                    buddy_folders[name]=buddy_sub_folder
                    buddy_sub_folders.append(name)
                buddy_sub_folder['workspaces'].append(value)

            #get all workspaces that user has access to:- ones they own, public and shared
            prefs = db.session.query(UserPreferences.name,UserPreferences.type,UserPreferences.id,UserPreferences.user_id)\
                                                      .filter(UserPreferences.type.in_(ws_types),UserPreferences.database==dbname,
                                                      or_(UserPreferences.id.in_(buddy_workspaces), or_(UserPreferences.user_id == current_user.id,UserPreferences.user_id==0))).all()

            #create the workspaces and put in list and dictionary (with id as key)
            #fomat is:- name,user_id/mine, user_name/mine, id, type
            for pref in prefs:
                if pref.user_id<>0:
                    #own workspaces
                    if pref.user_id == current_user.id:
                        ws=[pref.name,'mine','mine',pref.id,pref.type,True]
                    #shared workspaces
                    else:
                        #fix issue of key error 
                        #K.M. 14/5/2019
                        #a check needs to be carried out to know why we have it
                        # I have checked and found out that one the key inside buddy_dict
                        #is a string so it is failed and thrown key error 
                        #I have added a check to be sure all the keys are integer
                        # I think  this fine for now and should keep an eye on it
                        if not buddy_dict.get(pref.id):
                            #send message to the server to inform the admin to check it.
                            app.logger.exception("Erro in get_user_workspaces while getting a workspace with id %s"%pref.id)
                            continue
                        b_id = buddy_dict[pref.id][0]
                        b_name =  buddy_dict[pref.id][1]
                        editable=False
                        if b_id in buddy_edit_permission:
                            editable=buddy_edit_permission[b_id]                        
                        ws=[pref.name,b_id,b_name,pref.id,pref.type, editable]
                #public workspaces
                else:
                    ws= [pref.name,0,'public',pref.id,pref.type, False]
                workspaces.append(ws)
                ws_map_id[ws[3]]=ws

            #get the folder structure for the user
            ws_folders = db.session.query(UserPreferences).filter_by(database=dbname,user_id=current_user.id,type="workspace_folders",name='default').first()

            #for new users
            if not ws_folders:
                workspace_folders={"RN":{"text":"Root","id":"RN","workspaces":[]}}
                for ws in workspaces:
                    #put in any workspaces they own
                    if ws[2]=='mine':
                        workspace_folders['RN']['workspaces'].append(ws[3])
            else:
                workspace_folders = ujson.loads(ws_folders.data)
                if len(workspace_folders)==0:
                    workspace_folders={"RN":{"text":"Root","id":"RN","workspaces":[]}}
                    for ws in workspaces:
                        #put in any workspaces they own
                        if ws[2]=='mine':
                            workspace_folders['RN']['workspaces'].append(ws[3])

                #convert from legacy format if present
                for name in workspace_folders:
                    ws_list=workspace_folders[name].get("workspaces")
                    if ws_list:
                        if len(ws_list)>0 and isinstance(ws_list[0],list):
                            temp_list=[]
                            for i in range(0,len(ws_list)):
                                #get rid of public and shared
                                if ws_list[i][1] <> 'mine':
                                    continue
                                #convert to id
                                temp_list.append(ws_list[i][3])
                            workspace_folders[name]['workspaces']=temp_list

                #need to check the workspaces obtained with those in the folders
                #and add or remove if necessary
                for name in workspace_folders:
                    ws=workspace_folders[name].get("workspaces")
                    if ws:
                        #temporay list of all exisiting workspaces
                        temp_ws=[]
                        for ws_id in ws:
                            item =ws_map_id.get(ws_id)
                            #this should not be None
                            if  not item:
                                continue
                            #the workspace still exists keep it
                            if  item:
                                temp_ws.append(ws_id)
                                del ws_map_id[ws_id]

                        #replace with temp list (only containing existing work spaces)
                        workspace_folders[name]['workspaces']=temp_ws

                #add any new workspaces to folder root (in theory there should not be any)
                for ws_id in ws_map_id:
                    item = ws_map_id[ws_id]
                    if item[2]=='mine':
                        workspace_folders['RN']['workspaces'].append(ws_id)


        else:
            #user not logged in
            user_details=[0,"public"]
            #just get all public workspaces
            prefs = db.session.query(UserPreferences.name,UserPreferences.type,UserPreferences.id).filter( UserPreferences.type.in_(ws_types),
                                                                                                           UserPreferences.database==dbname, UserPreferences.user_id ==  0).all()

            for pref in prefs:
                ws = [pref.name,0,'public',pref.id,pref.type];
                workspaces.append(ws)

        #get (or create) the public folder structure
        prefs = db.session.query(UserPreferences).filter_by( type='public_workspace_folders',database=dbname, user_id=0,name='default').first()
        if not prefs:
            public_folders = {"RN": {"text": "Root", "id": "RN", "workspaces": [], "children": []}}
            #Add row to the UserPreferences table in case of it is not found
            #K.M 07/05/2020
            #
            p_folders = UserPreferences(data=json.dumps(public_folders),
                                      database=dbname, name="default", type="public_workspace_folders",
                                      user_id=0)

            db.session.add(p_folders)
            db.session.commit()
            for ws in workspaces:
                if ws[1]==0:
                    public_folders["RN"]["workspaces"].append(ws[3])
        else:
            public_folders = ujson.loads(prefs.data)  
        #print "=============================="
        #print ujson.dumps(buddy_folders)
        #print "=============================="
        #print ujson.dumps(workspaces)
        return ujson.dumps({"saved_workspaces":workspaces,
                                          "workspace_folders":workspace_folders,
                                          "buddy_folders":buddy_folders,
                                          "public_folders":public_folders,
                                          "logged_in":logged_in,
                                          "is_administrator":administrator,
                                          "user_details":user_details,
                                          "workspace_types":app.config['ANALYSIS_TYPES']})

    except Exception as e:
        rollback_close_system_db_session()
        #db.session.rollback()
        app.logger.exception("Error in get_user_workspaces, error message: %s"%e.message)
        return ujson.dumps({})


@species.route("/<species>/get_scheme_details", methods = ['GET','POST'])
@view_species_required
def get_scheme_details(species):    
    '''A Returns details for all public nomencalture((MLST) non rMLST schemes fora species. Admins
    will get all schemes

    * **url** /species/<species>/get_scheme_details

    Returns a (json) list of dictionaries. suitiable  to display in a the JavaScript SchemeValidationGrid
    
    * scheme_label -The scheme label
    * label -The label of the locus
    * description - The description of the locus
    * extra_row_info

        * pipleline_species -The name of the species in the nserv name e.g Escherichia_UoW would be Escherichia
        * pipeline - the actual pipeline e.g. Escherichia_UoW woild be UoW
        * scheme_name - the name of the scheme in the schemes table e.g. MLST_Achtman
        * name - The actual name of the locus
    '''
    dbase = get_database(species)
    try:
        Schemes = dbase.models.Schemes
        DataParams = dbase.models.DataParam
        #not actually used
        limit = request.form.get('limit')
        
        # Get all nomenclature (mlst schemes)
        scheme_query = dbase.session.query(Schemes).filter(Schemes.param['pipeline'].astext == 'nomenclature')
        #admins can get all
        if current_user.is_authenticated() and current_user.administrator == 1:
            schemes = scheme_query.all()
        #plebs just get public and not rMLST
        else:
            schemes = scheme_query.filter(Schemes.description != 'rMLST',Schemes.param['display'].astext=='public').all()
            
        param_list = [] 
        for scheme in schemes :
            #K.M 10/09/2018
            #we check for this scheme as it is not in use and need to be excluded
            if scheme.name=='cgMLST(3020) Beta' and species=='senterica':
                continue
            pipeline_species = scheme.param['scheme'].split("_")[0]
            pipeline = scheme.param['scheme'].split("_")[1]

            folder=get_download_scheme_folder(pipeline_species, pipeline)
            if folder:
                lis=folder.split('.')
                #print lis
                pipeline_species=lis[0]
                pipeline=lis[1]


            # Get locus name list      
            columns = ['label','id','name','description']
            params = dbase.session.query(DataParams.label, DataParams.id, DataParams.name, DataParams.description)\
                .filter(DataParams.tabname == scheme.description).filter(DataParams.group_name == 'Locus').order_by(DataParams.id).all()        
            for param in params:
                locus_info={"extra_row_info":{}}
                #data to be displayed in the table
                locus_info['scheme_label']  = scheme.name
                locus_info['label'] = param.label
                locus_info['description']=param.description
                #hidden data required for analysis
                locus_info['extra_row_info']['pipeline_species']  = pipeline_species
                locus_info['extra_row_info']['pipeline'] = pipeline
                locus_info['extra_row_info']['scheme_name']=scheme.description
                locus_info['extra_row_info']['name']=param.name
                param_list.append(locus_info)
        return ujson.dumps(param_list)
    except Exception as e:
        app.logger.exception("Problem obtaining scheme details for species: %s, error message %s"%(species, e.message))
        dbase.rollback_close_session()
        return ujson.dumps([])



@species.route("/<species>/get_serotype_prediction",methods = ['GET','POST'])
def get_serotype_prediction(species):
    '''Returns a json list of lists, each containing a serovars and its frequency of assignment to that ST
    the 7 gene or rMLST scheme (In senterica only).e.g.

    .. code-block:: json
    
        [
            ["Newport",1020],
            ["I 6,8:E,H",12],
            ["Bardo",3]
        ]

    * **url** /species/<species>/get_serotype_prediction

    POST Parameters
        * **st** 
        * **scheme** - the description field in the schemes table
    '''
    database = species
    st = request.form['st']
    scheme=request.form['scheme']
    dbase = get_database(database)
    data=''
    try:

        Schemes = dbase.models.Schemes
        scheme_info = dbase.session.query(Schemes).filter_by(description=scheme).first()
        scheme_name = scheme_info.param['scheme']
        arr =scheme_name.split('_')
        url ="%s/search.api/%s/%s/STs" % ( app.config['NSERV_ADDRESS'],arr[0],arr[1])
        query = "ST_id = %i" % int(st)
        resp = requests.post(url=url,data={"filter":query},timeout=app.config['NSERV_TIMEOUT'])
        try:
            data = ujson.loads(resp.text)
        except Exception as e:
            raise Exception ("Error while loading response json string, resp is %s"%resp.text)
        dbase.session.close()
        return ujson.dumps(data[0]['info']['predict']['serotype'])

        
    except Exception as e:
        dbase.rollback_close_session()
        app.logger.exception("Error in getting serotype prediction for %s, data is %s error message: %s"%(species, data, e.message))
        return ujson.dumps([]);    



#**************************JOBS************************************************
@species.route("/get_individual_job", methods = ['GET','POST'])
@auth_login_method(True)
def get_individual_job():
    '''Returns in json format, the job details  as returned from Crobot. For
    nomenclature jobs with >300 loci, the alleles will not be displayed

    * **url**/species/get_individual_job

    GET  Parameters 
        * **id** The id of the job'''
    j_id = request.args.get('id')
    URI = app.config['CROBOT_URI'] + "/head/show_job/" + str(j_id)
    try:
        resp = requests.get(URI)
        data = ujson.loads(resp.text)
    except Exception as e:      
        app.logger.exception("Error obtaining information for job id: %s\n URL: %s, error message: %s" % (j_id, URI, e.message))
        return ujson.dumps("Cannot get Job Information")
    for key in data[0]:
        try:
            data[0][key]=json.loads(data[0][key])
            data[0][key]=json.loads(data[0][key])
            if key == 'log' and len(data[0][key]) >300:
                temp={'ST':data[0][key].get("ST")}
                temp["loci"]="Not Displayed"
                data[0][key] = temp
        except Exception as  e:
            pass
        
    return ujson.dumps(data[0])


@species.route("/<species>/get_job_details", methods = ['GET','POST'])
@auth_login_required
@view_species_required
def get_job_details(species):
    '''Returns a list of dictionaries, each describing a job, suitiable for loading into
    the JavaScript JobsGrid

    * **url** /species/<species>/get_job_details

    POST Parameters
        * **limit** The amount of jobs to return
        * **query** Can be the following values

            * failed - show only failed jobs
            * all - show all jobs
            * running - show only running (includes queued, wait resource etc)
            * stale - show only jobs that have been queued/running for greater than 24 hours
            * user_jobs_only - if 'true' the only the users jobs will be returned (if a user is not an administrator, this will be the case anyway)
    '''
    limit = request.form.get('limit')
    query =request.form.get('query')
    #not used
    query_type=request.form.get('query_type')
    user_jobs_only = request.form.get('user_jobs_only')
    database = species
    if query == "failed":
        text = "(status LIKE 'FAILED%' OR status LIKE 'KILLED%')"
    elif query == "all":
        text = "id>0"
    elif query == 'running' :
        text= "status IN ('WAIT RESOURCE','QUEUE','RUNNING','SUBMIT')"
    elif query == 'stale':
        yesterday = str(datetime.today() - timedelta(days=1))
        text = "status IN ('WAIT RESOURCE','QUEUE','RUNNING','SUBMIT') AND date_sent<'%s'" % (yesterday)
    if user_jobs_only == "true" or  not current_user.administrator:
        text=text+" AND user_id="+str(current_user.id)
    text="("+text+")"
    text=text+" AND database='"+database+"'"
    jobs = UserJobs.query.filter(text).order_by("id DESC").limit(limit)

    return __process_jobs_for_grid(jobs,database)


@species.route("/kill_jobs", methods = ['GET','POST'])
@auth_login_required
def kill_jobs():
    '''Kills all jobs specified jobs. Returns a list of jobs processed
    so that the JobsGrid can be updated

    * **url** /species/kill_jobs

    GET Parameters
        * **ids** a comma delimited list of job ids for killing
    '''
    jobs = request.args.get("ids")
    if jobs == None:
        return json.dumps({})
    nums = jobs.split(",")
    kills= {}

    for num in nums:
        try:
            URI = app.config['CROBOT_URI']+"/head/kill_job/"+str(num)
            resp = requests.get(URI)
            reply = json.loads(resp.text)
            kills[num] = reply[0]['status'].split(":")[0]
        except Exception as e:
            kills[num] = "Error"
        
    return json.dumps(kills)
        
@species.route("/<species>/force_job_callback", methods = ['GET','POST'])
@auth_login_required
@view_species_required
def force_job_callback(species):
    '''Forces callback on the job i.e. the data is retrieved from crobot and processed.
    Returns a json list of jobs processed so that the  JavaScript JobsGrid can be updated
    with any new data

    * **url** /species/<species>/force_job_callback

    GET parameters
         * **ids** A comma delimited list the job ids to processs
    '''
    nums = request.form.get("ids")
    database = species
    process_job(nums)
    text = "id in (%s)" % nums
    #reload the jobs to see what has changed 
    jobs = UserJobs.query.filter(text).order_by("id DESC").all()
    return __process_jobs_for_grid(jobs,database)

        
@species.route("/<species>/resend_jobs", methods = ['GET','POST'])
@auth_login_required
@view_species_required

def resend_jobs(species):
    '''Resends all specified jobs

    * **url** /species/<species>/resend_jobs

    GET parameters
        * **ids** A comma delimited list the job ids to processs
    '''    
    try:
        job_ids=request.form.get("ids")
        job_ids = job_ids.split(",")
        for job_id in job_ids:
            job = get_crobot_job(int(job_id))
            job.send_job()
        return json.dumps("OK")
    except Exception as e:
        return json.dumps("Failed")
          
        
            

@species.route("/<species>/refresh_jobs", methods = ['GET','POST'])
@auth_login_required
@view_species_required
def refresh_jobs(species):
    '''Refreshes the specified jobs -essentially the same as :meth:`force_job_callback
    Returns a list of jobs processed so that the JobsGrid can be updated.

    * **url** /species/<species>/refresh_jobs

       GET parameters
            * **ids** A comma delimited list the job ids to processs
    '''
    jobs = request.form.get("ids")
    database = species
    if jobs:
        ids = jobs.split(",")
    else:
        return(json.dumps({}))
    
    #only get jobs that are queued running
    current =["RUNNING","QUEUE","WAIT RESOURCE","SUBMIT"]  
    jobs =UserJobs.query.filter(UserJobs.status.in_(current),UserJobs.id.in_(ids)).order_by(UserJobs.id).all()
    id_list = []
   
    jobid_to_job={}
    for job in jobs:       
        id_list.append(str(job.id))
        jobid_to_job[job.id]=job
    
    if (len(id_list)) ==0:
        return json.dumps({})
    
    complete_jobs = []
    #get information on all these jobs
    try:
        complete_jobs_ = []
        URI = app.config['CROBOT_URI']+"/head/show_jobs"
        resp = requests.post(URI,data={"FILTER":"tag IN (%s)" % ",".join(id_list)})
        data = ujson.loads(resp.text)
        for rec in data:
            job = jobid_to_job.get(rec['tag'])
            job.status = rec['status']
            db.session.add(job)
            if rec['status'].split(":")[0] in ['FAILED','KILLED','COMPLETE']:
                complete_jobs_.append(str(job.id))
        db.session.commit()
        complete_jobs=complete_jobs_
    except Exception as e:
        db.session.rollback()
        app.logger.exception("Error in updating user jobs for %s, error message: %s"%(species, e.message))
    #send off complete jobs for processing
    if len(complete_jobs) <> 0:
        to_send = ",".join(complete_jobs)
        process_job(to_send)
    
    return __process_jobs_for_grid(jobs,database)
    

#extra job information

def __process_jobs_for_grid(jobs,database):
    '''Helper methods which takes a list of SQLAlchemy UserJobs and formats them such
    that they can be loaded into a JavaScript JobsGrid
    Adds details about the assembly location and tries to distill the information in the UserJobs
    object to create understandable inputs and outputs. Needs rewriting 
    
    :param  jobs: A list of UserJobs
    :param database: The name of teh database'''
    dbase = get_database(database)
    j_list = []
    try:
        Assemblies =  dbase.models.Assemblies
        Strains = dbase.models.Strains
        list_of_ids = []
        for job in jobs:
            list_of_ids.append(job.id)
        assemblies = dbase.session.query(Assemblies).filter(Assemblies.job_id.in_(list_of_ids)).all()

        jobid_to_assembly = {}
        strainids= []
        strainid_to_jobid = {}
        stid=0
        for ass in assemblies:
            params = []
            for trace in ass.traces:
                params.append(trace.get_read_names())
                strainids.append(trace.strain_id)
                stid=trace.strain_id
            jobid_to_assembly[ass.job_id] = [",".join(params),ass.status,ass.id,stid] 

        strain_names = dbase.session.query(Strains.strain,Strains.id).filter(Strains.id.in_(strainids)).all()

        strainid_to_name = {}
        for strain in strain_names:
            strainid_to_name[strain.id]= strain.strain


        for job in jobs:
            info = jobid_to_assembly.get(job.id)
            if info:
                temp = job.__dict__['accession']
                if ":" in temp:
                    temp = temp.split(":")[1]
                    temp=temp[1:]
                    temp=temp.replace("#",",")
                job.__dict__['input'] = info[0]
                if not info[1]:
                    info[1]='Unknown'
                strainname="Unknown"
                if info[3]:
                    strainname= strainid_to_name.get(info[3],"Unknown")
                    if not strainname:
                        strainname = "Unknown"
                job.__dict__['output'] = "Assembly:"+strainname+":"+info[1]
                job.__dict__['extra_row_info'] = '/upload/download?assembly_id='+str(info[2])+"&database="+database
            #no assembly therefore extract information from job
            else:
                inp = job.__dict__['accession']
                if not inp:
                    job.__dict__['input']="Unknown"
                else:
                    if ":" in inp:
                        reads = inp.split(":")[1]
                        job.__dict__['input'] = "Reads:"+reads[1:]
                    else:  
                        job.__dict__['input'] = "SRA:"+inp
            del job.__dict__['_sa_instance_state']
            job.__dict__['status'] = job.__dict__['status'].split("_")[0]
            if isinstance(job.__dict__['date_sent'], unicode):
                job.__dict__['date_sent'] = str(job.__dict__['date_sent'])
            else:
                job.__dict__['date_sent'] = job.__dict__['date_sent'].strftime("%Y-%m-%d %I:%M")
            j_list.append(job.__dict__)
            
    except Exception as e:
        dbase.rollback_close_session()
        app.logger.exception("Error in processing job %s in database %s for display, error message: %s"%(jobs,database, e.message))
    return ujson.dumps(j_list)    
#**************


#************************Obsolete?******************************************************************
# WVN 15/8/17 Attempt to get started on the MLST legacy sequence search
# Code borrowed from the locus_search
@species.route("/<species>/mlst_legacy_allele_st_search",  methods = ['GET','POST'])
@view_species_required
def _mlst_legacy_allele_st_search(species):
    return process_mlst_legacy_allele_st_search(species)
   
def process_mlst_legacy_allele_st_search(species,analysis_id =0,link_id=0):
    try:
        allowed_schemes=[]
        if current_user.is_authenticated():
            allowed_schemes=current_user.get_allowed_schemes(species)
        dbase =get_database(species)
        Schemes = dbase.models.Schemes
        schemes = dbase.session.query(Schemes.description,Schemes.name,Schemes.param).filter(Schemes.param['scheme']<>None).all()
        scheme_info = {}
        for scheme in schemes:
            if not scheme.name.startswith("CRISP"):
                if scheme.param['display']=='public' or scheme.description in allowed_schemes:
                    scheme_info[scheme.description]=scheme.name

        gene_categories = app.config['GENE_CATEGORIES'].get(species,[])

        return render_template("species/mlst_legacy_allele_st_search.html", species=species,scheme_info=scheme_info,
                               gene_categories=gene_categories,analysis_id=analysis_id,link_id=link_id)
    except Exception as e:
        dbase.rollback_close_session()
        app.logger.exception("Error in process_mlst_legacy_allele_st_search for %s, error message: %s" % (species, e.message))
        flash("Error in the request or/and The database is not available now, please check your request and try later.")
        return redirect(url_for('main.index'))
# WVN 15/8/17 End of code from MLST legacy sequence search

# WVN 4/9/17 Attempt to get started on the MLST legacy download
# Code borrowed from above, originally from the locus_search
@species.route("/<species>/mlst_legacy_download",  methods = ['GET','POST'])
@view_species_required
def _mlst_legacy_download(species):
    return process_mlst_legacy_download(species)
   
def process_mlst_legacy_download(species):
    dbase=get_database(species)
    try:
        DataParams = dbase.models.DataParam
        locus_list = []

        # Get locus name list
        columns = ['label','id','name','description']
        params = dbase.session.query(DataParams.label, DataParams.id, DataParams.name, DataParams.description)\
          .filter(DataParams.tabname == "MLST_Achtman").filter(DataParams.group_name == 'Locus').order_by(DataParams.id).all()
        for param in params:
          locus_list.append(param.label)

        return render_template("species/mlst_legacy_download.html", species=species,locus_list=locus_list)
    except Exception as e:
        dbase.rollback_close_session()
        app.logger.exception("Error in _mlst_legacy_download for %s, error message: %s" % (species, e.message))
        flash("Error in the request or/and The database is not available now, please check your request and try later.")
        return redirect(url_for('main.index'))

# WVN 15/8/17 End of code from MLST legacy download

# WVN 23/8/17 Species specific MLST old website support main page
@species.route("/<species>/warwick_mlst_legacy",  methods = ['GET','POST'])
@view_species_required
def warwick_mlst_legacy_species(species):
    dbase = dbhandle[species]    
    return render_template("species/warwick_mlst_legacy.html", species=species)

