tabname	name	sra_field	mlst_field	display_order	nested_order	label	datatype	min_value	max_value	required	default_value	allow_pattern	description	vals	max_length	no_duplicates	allow_multiple_values	group_name
strains	strain		STRAIN															
strains	contact		SOURCE_LAB															
strains	source_type		ISOLATED_FROM															
strains	collection_year		YEAR															
strains	continent		CONTINENT															
strains	country		COUNTRY															
strains	city		LOCATION															
strains	serotype		O_SEROTYPE	16	0	O Serotype	text			0							
strains	contact_name		CONTACT	17	0	Contact Name	text			0							
strains	disease			14	0	Disease	combo			0																					
strains	species	Sample,Metadata,Species		7	0	Species	combo			0								
strains	biogroup			7	0	Biogroup	combo			0								
strains	alternative_name			7	0	Alternative Name	text			0								