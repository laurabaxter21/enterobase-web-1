from flask import Blueprint
from entero import app
from apispec import APISpec
from flask_apispec import FlaskApiSpec
import  resources as rs
import admin_ops 

API_VERSION = '2.0'
api_bp = Blueprint('api.%s' %API_VERSION, __name__)

app.config.update({
    'APISPEC_SPEC': APISpec(
        title='Enterobase-API',
        version='v2.0',
        plugins=['apispec.ext.marshmallow'], 
        schemes=['http','https'],
        securityDefinitions=dict(api_key=dict(type='basic',description='Basic Authentication is required for all requests ')),
      ),
    'APISPEC_SWAGGER_UI_URL' : '/api/v2.0/swagger-ui',
    'APISPEC_SWAGGER_URL' : '/api/v2.0/swagger'
})


docs = FlaskApiSpec(app)

DESC = """
API for EnteroBase (http://enterobase.warwick.ac.uk) 

EnteroBase is a user-friendly online resource, where users can upload their 
own sequencing data for de novo assembly by a stream-lined pipeline. The assemblies 
are used for calling MLST and wgMLST patterns, allowing users to compare their strains 
to publically available genotyping data from other EnteroBase users, GenBank and classical MLST databases.

Click here to find how to get and use an API token: http://bit.ly/1TKlaOU
"""

docs.spec.info.update(dict(description=DESC,
        contact = dict(name='Nabil-Fareed Alikhan',email='enterobase@warwick.ac.uk')))

# --- List API access point --- #

for res in [rs.StrainsResource, rs.StrainsVersionResource, rs.TracesResource, rs.AssembliesResource,rs.SchemesResource, rs.StrainDataResource]:
    res_init = res()
    endpoint = '%slistresource' %(res_init.name)
    app.add_url_rule('/api/v%s/<string:database>/%s' %(API_VERSION,  res_init.name), view_func=res_init.as_view(endpoint))
    docs.register(res, endpoint=endpoint)
    docs = res_init.update_doc(docs)
    
# --- NServ dependent - Scheme related access points --- # 
for res in [rs.LociResource, rs.StsResource, rs.AllelesResource]:
    res_init = res()
    endpoint = '%slistresource' %(res_init.name)
    app.add_url_rule('/api/v%s/<string:database>/<string:scheme>/%s' %(API_VERSION,  res_init.name), view_func=res_init.as_view(endpoint))
    docs.register(res, endpoint=endpoint)
    docs = res_init.update_doc(docs)
    
# --- Single value {barcode} API access point --- # 
for res in [rs.StrainResource, rs.TraceResource, rs.AssemblyResource, rs.SchemeResource]:
    res_init = res()
    endpoint = '%sresource' %(res_init.name)
    app.add_url_rule('/api/v%s/<string:database>/%s/<string:barcode>' %(API_VERSION,  res_init.name), view_func=res_init.as_view(endpoint))
    docs.register(res, endpoint=endpoint)
    docs = res_init.update_doc(docs)


# --- Generic lookup API access point --- # 
res = rs.LookupResource
res_init = res()
endpoint = '%sresource' %(res_init.name)
app.add_url_rule('/api/v%s/%s/<string:barcode>' %(API_VERSION,  res_init.name), view_func=res_init.as_view(endpoint))
docs.register(res, endpoint=endpoint)
docs = res_init.update_doc(docs)

res = rs.LookupListResource
res_init = res()
endpoint = '%slistresource' %(res_init.name)
app.add_url_rule('/api/v%s/%s' %(API_VERSION,  res_init.name), view_func=res_init.as_view(endpoint))
docs.register(res, endpoint=endpoint)
docs = res_init.update_doc(docs)

# --- Top level API access point --- # 

res = rs.TopResource
res_init = res()
endpoint = '%stopresource' %(res_init.name)
app.add_url_rule('/api/v%s' %(API_VERSION), view_func=res_init.as_view(endpoint))
docs.register(res, endpoint=endpoint)
docs = res_init.update_doc(docs)


# --- Refresh Token API access point --- # 

res = rs.LoginResource
res_init = res()
endpoint = '%sloginresource' %(res_init.name)
app.add_url_rule('/api/v%s/login' %(API_VERSION), view_func=res_init.as_view(endpoint))
docs.register(res, endpoint=endpoint)
docs = res_init.update_doc(docs)


#print "=======>>>>", res.__dict__
