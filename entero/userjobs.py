from entero.databases.system.models import UserJobs,UserUploads
import json
import requests
from entero import app,db,dbhandle
import datetime
from sqlalchemy.sql import select,and_



#These methods contain atomic operations and should be queued


class EnteroJobs:
       
       #checks all records that are queued, running or waiting resource and updates
       def update(self):
              current =["RUNNING","QUEUE","WAIT RESOURCE"]
              jobs =UserJobs.query.filter(UserJobs.status.in_(current)).all()
              
              for jId in xrange(0, jobs, 300) :
                  jobBatch = jobs[jId:jId+300]
                  URI = app.config['CROBOT_URI']+"/head/show_jobs"
                  params = "tag IN (%s)".format(','.join([ job.id for job in jobBatch ]))
                  resp = requests.post(URI,data={"FILTER":params}, timeout=60)
                  statusMap = {}
                  for d in ujson.loads(resp.text) :
                      statusMap[d['tag']] = d['status']
                  for job in jobBatch :
                      if job.id in statusMap :
                          job.status = statusMap[job.id]
                          db.session.add(job)
                  db.session.commit()

              #for job in jobs:
              #       URI = app.config['CROBOT_URI']+"/head/show_job/"+str(job.id)
              #       resp = requests.get(URI)
              #       data = json.loads(resp.text)
              #       job.status =data[0]['status']
              #       db.session.add(job)
              #db.session.commit()
              
       #synchronizes with CRobot       
       def synchronize(self):
              jobNo = 1046
              moreJobs = True
              while (moreJobs):
                     moreJobs=  self.update_or_add_job(jobNo)
                     jobNo+=1
       
       #adds job if not already in database or updates job if already in the database
       #if job complete and assembly will update genomes database and user uploads
       #this method is called via the callback from CRobot when the job finishes 
       def update_or_add_job(self,jobNo):                   
              URI = app.config['CROBOT_URI']+"/head/show_job/" 
            
              resp = requests.get(url = URI+str(jobNo))
              data = json.loads(resp.text)
             
            
              if len(data)==0:
                     app.logger.error("updating a non existant job "+str(jobNo))
                     return False
              acc = ""
              if  data[0]["inputs"].has_key("read"):
                     reads = data[0]["inputs"]["read"]
                     if len(reads)>0:
                            acc = reads[0]
              
              location = ""
       
              if 'assembly' in data[0]['outputs']:
                     location =  data[0]['outputs']['assembly'][1]
              job =  UserJobs.query.filter_by(id=jobNo).first()
        
              if not job:
                     job = UserJobs(id = jobNo)
                     
              if not job.user_id:
                     job.user_id = 0
              job.status =data[0]['status']
              job.date_sent=data[0]['query_time'],
              job.pipeline=data[0]['pipeline']
              job.output_location = location,
              job.accession=acc
              
              
          
              #assembly job completed -update user uploads and genome database
              if job.status == "COMPLETE" and location and job.database:    
                     dbase  = dbhandle[job.database]
                     Genomes =  dbase.models.Genomes    
                     
                     genome = dbase.session.query(Genomes).filter(Genomes.accession == job.accession).first()
                     if genome:          
                            genome.status = "Assembled"
                     # could be  user uploaded       
                     else:
                            genome = dbase.session.query(Genomes).filter(Genomes.index_id == job.accession).first()
                            if genome:
                                   genome.status = "Assembled"
                                   genome.accession = job.accession
                                   recs = UserUploads.query.filter(UserUploads.genome_id==acc)                               
                                   for rec in recs:
                                          rec.status = "assembled"                    
                                          db.session.add(rec)
                     dbase.session.commit()      
                     if data[0]['pipeline'] == "QAssembly_ST":
                            print data[0]["log"]
                            allele_info = json.loads(data[0]["log"])
                            self.update_alleles(allele_info["MLST"],job.database,genome.id)                        
                    
              db.session.add(job)
              db.session.commit()
              
              
              
              return True
       
        
              
       '''
       Sends an assembly job either using SRA reads or user uploaded reads
       Accession Number - if from the SRA or genome index if user uploaded reads
       database - database 
       username - admin by default
       user_id  - 0 (admin) by default
       scheme - Salmonella_UoW by default
       users reads - dictionary  containing 
           'genome_id':genome index
           'location': directory containing the reads
           'reads': A list of the reads e.g. ["seq_01_R1.fastq.gz","seq_01_R2.fastq.gz"]
       '''    
       def send_assembly_job(self,accession,database,username="admin",user_id=0,scheme = "Salmonella_UoW",user_reads=None):
              jobs = UserJobs.query.filter_by(accession=accession).all()
              already = False
              for job in jobs:
                     if job.status == "QUEUE" or job.status == "RUNNING" or job.status == "COMPLETE":
                            already  = True
                            break
                     
              if already:
                     return False
              URI = app.config['CROBOT_URI']+"/head/submit"
              params = {
                  "source_ip":app.config['CALLBACK_ADDRESS'],
                  "usr":username,
                  "scheme":scheme,
                  "query_time":str(datetime.datetime.now()),
                  
                  "params":{
                      "prefix":"test"
                  },
                  "pipeline":"QAssembly_ST"
              }
              
              if  user_reads:
                     to_send = []
                     to_send.append(user_reads["genome_id"])
                     for read in user_reads["reads"]:
                            to_send.append(user_reads["location"]+"/"+read)
                     params["inputs"]={"read":to_send}                   
              else:
                     params["reads"]={"read":accession} 
              resp = requests.post(url = URI, data = {"SUBMIT":json.dumps(params)})
              try:
                     data = json.loads(resp.text)
              except Exception:
                     app.logger.error(resp.text)
                     return False
              #add to user jobs 
              job = UserJobs(id=data['tag'],pipeline="QAssembly_ST",date_sent=data["query_time"],database=database,
                             status=data["status"],accession = accession,user_id=user_id)
              db.session.add(job)
              #update the user reads
              if user_reads:
                     recs = UserUploads.query.filter(UserUploads.file_name.in_(user_reads["reads"]),UserUploads.species==database,UserUploads.user_id==user_id)
                                 
                     for rec in recs:
                            rec.genome_id=user_reads["genome_id"]
                            rec.status="queued"                       
                            db.session.add(rec)
              db.session.commit()
       #data is a dictionary of locus to allele, pos etc       
       def update_alleles(self,data,database,genome_id):
              dbase  = dbhandle[database]
              Loci =  dbase.models.Loci
              Alleles = dbase.models.Alleles
              AlleleGenome= dbase.models.AlleleGenome
              con = dbase.engine.connect()
              #go through each loci = only loci name from Zhemin (equates to locus.product ?)
              for locus in data:
                     #nothing in the loci data
                     if type(data[locus]) <> list:
                            continue
                     rec = data[locus][0]
                     legacy_id = rec["id"]
                   
                        
                     #get the locus ID                    
                     locus_id =dbase.session.query(Loci).filter(Loci.product== locus).first().id
              
                     #does new allele exist
                     allele =  dbase.session.query(Alleles).filter(Alleles.legacy_id== legacy_id,                 
                                                                 Alleles.locus_id == locus_id).first()
                     #create new allele - need also to add sequence
                     if allele == None:
                            allele = Alleles(legacy_id = legacy_id,locus_id=locus_id)
                            dbase.session.add(allele)
                            #need to flush to get the new ID
                            dbase.session.flush()
                     
                     #store for later
                     rec["allele_id"]= allele.id
                     
              dbase.session.commit()   
                    
                    
                          
              for locus in data:
                     if type(data[locus]) <> list:
                            continue
                     rec = data[locus][0]
                     legacy_id = rec["id"]
                     #fill out the data
                     sel = select([AlleleGenome.c.allele_id]).where(and_(AlleleGenome.c.allele_id ==rec["allele_id"],AlleleGenome.c.genome_id==genome_id))
                     r =con.execute(sel).fetchall()
                     if len(r) == 0:
                            ins = AlleleGenome.insert().values(allele_id = rec["allele_id"],genome_id=genome_id,
                                                  contig=rec["contig"],
                                                  start=rec["start"],end = rec["end"])
                            con.execute(ins)
              con.close()
                    
                     
              
       
enteroJobs = EnteroJobs()
