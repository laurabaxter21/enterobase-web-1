Installing stand-alone GrapeTree
================================
The stand-alone version emulates the EnteroBase version through a lightweight
webserver running on your local computer. You will be interacting with the
program as you would in EnteroBase; through a web browser. We recommend
`Google Chrome <https://www.google.com/chrome/index.html>`_ for best results.
There are number of different ways to interact with GrapeTree, the easiest is to 
install via pip, or download the ready-built software here:
https://github.com/achtman-lab/GrapeTree/releases

Installing and Running GrapeTree
--------------------------------
**Install via pip**

.. code-block:: bash

   pip install grapetree
   grapetree

For more information see: https://pypi.org/project/GrapeTree/

**Running on Mac: Download GrapeTree_mac.zip**

You will need to unzip GrapeTree_mac.zip (just double click). Inside there will 
be a directory. From this directory launch grapetree_mac. You may be warned about
Security settings, if you right click on the grapetree_mac and then click
"Open" it should be fine.

**Running on Windows: Download GrapeTree_win.zip**

Once downloaded, you will need to unzip GrapeTree_win.zip and then open the
extracted folder and  run GrapeTree_win.exe. When you run it the first time on 
windows you might get a prompt about security. On Windows 10, click the small 
text: "More info", and then the button "Run Anyway". 

**Running from Source code** 

EnteroMSTree - GrapeTree requires `Python 2.7 <https://www.python.org/downloads/release/python-2712/>`_
and some additional python modules (listed in requirements.txt). 
The easiest way to install these modules is with pip:

.. code-block:: bash
   
   pip install -r requirements.txt
   chmod +x binaries/
   
On Linux or MacOSX you need to make sure the binaries in binaries/ can be
executed. To run GrapeTree;

#. Navigate to the directory where you installed GrapeTree. 
#. Run it through python as below. 

.. code-block:: bash

    \GrapeTree>python main.py
     * Running on http://127.0.0.1:8000/ (Press CTRL+C to quit)

**Running GrapeTree with no installation**

If you just want to view and manipulate trees, without processing
profile/FASTA files, it can simply be run by opening `MSTree_holder.html` in
your web browser. It requires the 'static folder'. This is essentially what
happens if you visit 
https://achtman-lab.github.io/GrapeTree/MSTree_holder.html

First look at GrapeTree
----------------------
The program will automatically open your web browser and you will see the 
GrapeTree Splash Screen, as shown below.

.. image:: https://bitbucket.org/repo/Xyayxn/images/2739749373-GrapeTree_start1.png

If at anytime you want to restart the page you can visit http://localhost:8000 in your web browser, as shown below.

.. image:: https://bitbucket.org/repo/Xyayxn/images/1646166765-Grapetreestart2.png

Let's apply these concepts with data from a previously published study of
*Salmonella enterica* serovar Agona. :doc:`Click here for the first tutorial
<grapetree-tutorial-1>`
