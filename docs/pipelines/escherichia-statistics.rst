Details of assembly methods and in silico genotyping for *Escherichia*
======================================================================

* **All MLST-like typing methods in EnteroBase are derived from a genome assembly of sequenced reads. For an explanation of this method**, see :doc:`here </pipelines/backend-pipeline-qassembly>`
* **For a general description of the in silico typing method**, see :doc:`here </pipelines/backend-pipeline-nomenclature>`



+-----------------+----------------+---------------+-----------------+
| MLST – Classic  | Ribosomal MLST | Core Genome   | Whole Genome    |
|                 | (Jolley, 2012) | MLST          | MLST            |
+=================+================+===============+=================+
| 7 Loci          | 53 Loci        | 2,513 Loci    | 25,002 Loci     |
+-----------------+----------------+---------------+-----------------+
| Conserved       | Ribosomal      | Core genes    | Any coding      |
| Housekeeping    | proteins       |               | sequence        |
| genes           |                |               |                 |
+-----------------+----------------+---------------+-----------------+
| Highly          | Highly         | Variable;     | Highly          |
| conserved; Low  | conserved;     | High          | variable;       |
| resolution      | Medium         | resolution    | Extreme         |
|                 | resolution     |               | resolution      |
+-----------------+----------------+---------------+-----------------+
| Different       | Single scheme  | Different     | Different       |
| scheme for each | across tree of | scheme for    | scheme for each |
| species/genus   | life           | each          | species/genus   |
|                 |                | species/genus |                 |
+-----------------+----------------+---------------+-----------------+

7 Gene MLST
-----------

Classic MLST scheme is described in `Wirth et al (2006) Mol. Microbiol.
60(5), 1136-1151`_.

Genes included in 7 gene MLST (together with the length of sequence used
for MLST taken from figure 1 in the above cited paper):

==== ======
Gene Length
==== ======
adk  536
fumC 469
gyrB 460
icd  518
mdh  452
recA 510
purA 478
==== ======

Ribosomal MLST (rMLST)
----------------------

-  **rMLST** is Copyright © 2010-2016, University of Oxford. rMLST is
   described in `Jolley et al. 2012 Microbiology 158:1005-15`_.


.. _please click here: EnteroBase%20Backend%20Pipeline%3A%20nomenclature
.. _Wirth et al (2006) Mol. Microbiol. 60(5), 1136-1151: http://onlinelibrary.wiley.com/doi/10.1111/j.1365-2958.2006.05172.x/full
.. _`Jolley et al. 2012 Microbiology 158:1005-15`: http://www.ncbi.nlm.nih.gov/pubmed/22282518