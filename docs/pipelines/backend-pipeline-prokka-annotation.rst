Top level links: 

* **[Main top level page for all documentation](Home)**
* **[EnteroBase Features](Home)**
* **[Registering on EnteroBase and logging in](Enterobase%20website)**
* **[Tutorials](Tutorials)**
* **[Using the API](About%20the%20API)**
* **[About the underlying pipelines and other internals](EnteroBase Backend Pipeline)**
* **[How schemes in EnteroBase work](About%20EnteroBase%20Schemes)**
* **[FAQ](FAQ)**

# prokka_annotation #

[TOC]

# Overview #

prokka_annotation is an annotation pipeline and is run on an assembly (after successful assembly from short
reads by [QAssembly]).  Annotations are then available for download from the EnteroBase website in gzipped
[GFF] and [GenBank Flat File]/[GBK] format files.  (The normal method for user download of annotations is
described [here](user_download_annotations "wikilink").)

The current version of prokka_annotation is 1.0.

# Annotation #

The prokka_annotation pipeline is primarily a wrapper around the program [prokka] (version 1.11).  It goes through the
following steps:

* read the assembled sequence
* runs prokka with the "--listdb" option in order to check if the desired bacterial genus is supported
* runs prokka with the "--compliant" option for GenBank/ENA/DDJB compliance, using "--genus" if the desired bacterial genus is supported or a genetic bacterial model if not
* some minor cleanup of the output GFF and GBK files from prokka

# Pan-gene sets #

| Species | No. of genes |
|-|-|
|Salmonella|21,065|
|Escherichia|25,002|
|Yersinia|19,591|

[QAssembly]: EnteroBase%20Backend%20Pipeline%3A%20QAssembly

[GFF]: http://en.wikipedia.org/wiki/General_feature_format "external link"
[GBK]: http://www.ncbi.nlm.nih.gov/Sitemap/samplerecord.html "external link"
[GenBank Flat File]: http://www.ncbi.nlm.nih.gov/Sitemap/samplerecord.html "external link"
[prokka]: http://www.vicbioinformatics.com/software.prokka.shtml "external link"