Protocols used for MLST of *Escherichia coli* and *Shiella spp.*
===========================================
Updates
-------
For citation please refer to: `Wirth, T., Falush, D., Lan, R., Colles, F.,
Mensa, P., Wieler, L.H., Karch, H., Reeves, P. R., Maiden, M. C., Ochman, H.,
and Achtman M. 2006. Sex and virulence in Escherichia coli: an evolutionary
perspective. Mol.Microbiol. 60(5),
1136-1151. <http://www.ncbi.nlm.nih.gov/pmc/articles/PMC1557465/>`_

**Update information**: ST complexes have been updated again on 17.05.2007.
There are currently 600 STs and 54 ST complexes. The criteria have also been
changed and are now groups of at least 3 STs sharing 6 alleles in pair-wise
comparisons. The assignments of STs to some of the previous ST complexes have
changed as a result, although we have tried to maintain consistency.

**Update information**:ST complexes have been updated again on 24.08.2005.
Multiple new ST Complexes have been assigned and multiple STs have been
assigned to known complexes. Due to the increased number of strains assigned
to the ST29 Complex, it has become unclear whether these bacteria are closely
related or only linked by one intermediate recombinant. Therefore, this has
now been split into the ST23 and ST29 Complexes.

**Update information**: ST complexes have been updated on 23.11.2004. This includes the merging of
ST21, 29 and 90 Complexes into ST29 Complex and ST3 and 17 Complexes into
ST20 Complex. Multiple new ST complexes have been assigned. A number of STs
have been merged with other STs due to curation of the database.


Genes
-----
The E. coli MLST scheme uses internal fragments of the following seven 
house-keeping genes:  

* *adk* (adenylate kinase)  
* *fumC* (fumarate hydratase)  
* *gyrB* (DNA gyrase)  
* *icd* (isocitrate/isopropylmalate dehydrogenase)  
* *mdh* (malate dehydrogenase)  
* *purA* (adenylosuccinate dehydrogenase)  
* *recA* (ATP/GTP binding motif)  

PCR Amplification
-----------------
Please note: These include new primer sequences (added 26 July 2004;
labelling corrected on 5 March, 2007) whose labels indicate the genomic
direction rather than reading frame. The primer pairs for the PCR
amplification of internal fragments of these genes can be chosen from:

+------+---------------------------------------------+--------------+----------------------+
|Gene  |Primer pair sequences                        |Product length|Annealing Temperature |
+=======+============================================+==============+======================+
|*adk* | F 5'-ATTCTGCTTGGCGCTCCGGG-3'                |583 bp        |54° C                 |
|      | R 5'-CCGTCAACTTTCGCGTATTT-3'                |              |                      |
|      | F1 5'-TCATCATCTGCACTTTCCGC-3'               |              |                      |
|      | R1 5'-CCAGATCAGCGCGAACTTCA-3'               |              |                      |
+------+---------------------------------------------+--------------+----------------------+
|*fumC*| R1 5'-TCCCGGCAGATAAGCTGTGG-3'               |806 bp        |54° C                 | 
|      | F 5'-TCACAGGTCGCCAGCGCTTC-3'                |              |                      | 
|      | R 5'-GTACGCAGCGAAAAAGATTC-3'                |              |                      |
+------+---------------------------------------------+--------------+----------------------+
|*gyrB*| F  5'-TCGGCGACACGGATGACGGC-3'               |911 bp        |60° C                 |
|      | R1  5'-GTCCATGTAGGCGTTCAGGG-3'              |              |                      |
|      | R  5'-ATCAGGCCTTCACGCGCATC-3'               |              |                      |
+------+---------------------------------------------+--------------+----------------------+
|*icd* | F 5'-ATGGAAAGTAAAGTAGTTGTTCCGGCACA-3'       |878 bp        |54° C                 |
|      | R 5'-GGACGCAGCAGGATCTGTT-3'                 |              |                      |
+------+---------------------------------------------+--------------+----------------------+
|*mdh* | F 5'-ATGAAAGTCGCAGTCCTCGGCGCTGCTGGCGG-3'    |932 bp        |60° C                 |
|      | R 5'-TTAACGAACTCCTGCCCCAGAGCGATATCTTTCTT-3' |              |                      |
|      | F1 5'-AGCGCGTTCTGTTCAAATGC-3'               |              |                      |
|      | R1 5'-CAGGTTCAGAACTCTCTCTGT-3'              |              |                      |
+------+---------------------------------------------+--------------+----------------------+
|*purA*|F1 5'-TCGGTAACGGTGTTGTGCTG-3'                |816 bp        |54° C                 | 
|      | F 5'-CGCGCTGATGAAAGAGATGA-3'                |              |                      |
|      | R 5'-CATACGGTAAGCCACGCAGA-3'                |              |                      | 
+------+---------------------------------------------+--------------+----------------------+
|*recA*|R1 5'-AGCGTGAAGGTAAAACCTGTG-3'               |780 bp        |58° C                 |
|      |F 5'-CGCATTCGCTTTACCCTGACC-3'                |              |                      |
|      |F1 5'-ACCTTTGTAGCTGTACCACG-3'                |              |                      |
|      |R 5'-TCGTCGAAATCTACGGACCGGA-3'               |              |                      |
+------+---------------------------------------------+--------------+----------------------+

**Conditions:**

PCR: 2 min at 95°, 30 cycles of 1 min at 95°, 1 min at annealing temp,
2 min at 72° followed by 5 min at 72°. The PCR reaction contains 50 ng
of chromosomal DNA, 20 pmol of each primer, 200 umol (10 ul of a 2 mM
solution) of the dNPTs, 10 ul of 10x PCR buffer, 5 units of Taq polymerase
and water to 100 ul.

Sequencing
----------
We use the amplification primer pairs for the sequencing step.  

Allele template
---------------
Allelic profile of E. coli strain MG1655 (see Genebank)  

*adk* (536 bp):  

.. code-block:: bash

    GGGGAAAGGGACTCAGGCTCAGTTCATCATGGAGAAATATGGTATTCCGCAAATCTCCACTGGCGATATGCTGCGTG  
    CTGCGGTCAAATCTGGCTCCGAGCTGGGTAAACAAGCAAAAGACATTATGGATGCTGGCAAACTGGTCACCGACGAA  
    CTGGTGATCGCGCTGGTTAAAGAGCGCATTGCTCAGGAAGACTGCCGTAATGGTTTCCTGTTGGACGGCTTCCCGCG  
    TACCATTCCGCAGGCAGACGCGATGAAAGAAGCGGGCATCAATGTTGATTACGTTCTGGAATTCGACGTACCGGACG  
    AACTGATCGTTGACCGTATCGTCGGTCGCCGCGTTCATGCGCCGTCTGGTCGTGTTTATCACGTTAAATTCAATCCG  
    CCGAAAGTAGAAGGCAAAGACGACGTTACCGGTGAAGAACTGACTACCCGTAAAGATGATCAGGAAGAGACCGTACG  
    TAAACGTCTGGTTGAATACCATCAGATGACAGCACCGCTGATCGGCTACTACTCCAAAGAAGCAGAAGCGGGTA  


*fumC* (469 bp):  

.. code-block:: bash

    CGAGCGCCATTCGTCAGGCGGCGGATGAAGTACTGGCAGGACAGCATGACGACGAATTCCCGCTGGCTATCTGGCAG  
    ACCGGCTCCGGCACGCAAAGTAACATGAACATGAACGAAGTGCTGGCTAACCGGGCCAGTGAATTACTCGGCGGTGT  
    GCGCGGGATGGAACGTAAAGTTCACCCTAACGACGACGTGAACAAAAGCCAAAGTTCCAACGATGTCTTTCCGACGG  
    CGATGCACGTTGCGGCGCTGCTGGCGCTGCGCAAGCAACTCATTCCTCAGCTTAAAACCCTGACACAGACACTGAAT  
    GAGAAATCCCGTGCTTTTGCCGATATCGTCAAAATTGGTCGTACTCACTTGCAGGATGCCACGCCGTTAACGCTGGG  
    GCAGGAGATTTCCGGCTGGGTAGCGATGCTCGAGCATAATCTCAAACATATCGAATACAGCCTGCCTCACGTAGCGG  
    AACTGGC  
    

*gyrB* (460 bp):  

.. code-block:: bash

    GGTCTGCACGGCGTTGGTGTTTCGGTAGTAAACGCCCTGTCGCAAAAACTGGAGCTGGTTATCCAGCGCGAGGGTAA  
    AATTCACCGTCAGATCTACGAACACGGTGTACCGCAGGCCCCGCTGGCGGTTACCGGCGAGACTGAAAAAACCGGCA  
    CCATGGTGCGTTTCTGGCCCAGCCTCGAAACCTTCACCAATGTGACCGAGTTCGAATATGAAATTCTGGCGAAACGT  
    CTGCGTGAGTTGTCGTTCCTCAACTCCGGCGTTTCCATTCGTCTGCGCGACAAGCGCGACGGCAAAGAAGACCACTT  
    CCACTATGAAGGCGGCATCAAGGCGTTCGTTGAATATCTGAACAAGAACAAAACGCCGATCCACCCGAATATCTTCT  
    ACTTCTCCACTGAAAAAGACGGTATTGGCGTCGAAGTGGCGTTGCAGTGGAACGATGGCTTCCAGGAAAACATCT  
    

*icd* (518 bp):  

.. code-block:: bash

    CGACGCTGCAGTCGAGAAAGCCTATAAAGGCGAGCGTAAAATCTCCTGGATGGAAATTTACACCGGTGAAAAATCCA  
    CACAGGTTTATGGTCAGGACGTCTGGCTGCCTGCTGAAACTCTTGATCTGATTCGTGAATATCGCGTTGCCATTAAA  
    GGTCCGCTGACCACTCCGGTTGGTGGCGGTATTCGCTCTCTGAACGTTGCCCTGCGCCAGGAACTGGATCTCTACAT  
    CTGCCTGCGTCCGGTACGTTACTATCAGGGCACTCCAAGCCCGGTTAAACACCCTGAACTGACCGATATGGTTATCT  
    TCCGTGAAAACTCGGAAGACATTTATGCGGGTATCGAATGGAAAGCTGACTCTGCCGACGCCGAGAAAGTGATTAAA  
    TTCCTGCGTGAAGAGATGGGCGTGAAGAAAATTCGCTTCCCGGAACATTGCGGTATCGGTATTAAGCCGTGTTCTGA  
    AGAAGGCACCAAACGTCTGGTTCGTGCAGCGATCGAATACGCAATTGCTAACGATC  
    

*mdh* (452 bp):  

.. code-block:: bash

    GGCGTAGCGCGTAAACCGGGTATGGATCGTTCCGACCTGTTTAACGTTAACGCCGGCATCGTGAAAAACCTGGTACA  
    GCAAGTTGCGAAAACCTGCCCGAAAGCGTGCATTGGTATTATCACTAACCCGGTTAACACCACAGTTGCAATTGCTG  
    CTGAAGTGCTGAAAAAAGCCGGTGTTTATGACAAAAACAAACTGTTCGGCGTTACCACGCTGGATATCATTCGTTCC  
    AACACCTTTGTTGCGGAACTGAAAGGCAAACAGCCAGGCGAAGTTGAAGTGCCGGTTATTGGCGGTCACTCTGGTGT  
    TACCATTCTGCCGCTGCTGTCACAGGTTCCTGGCGTTAGTTTTACCGAGCAGGAAGTGGCTGATCTGACCAAACGCA  
    TCCAGAACGCGGGTACTGAAGTGGTTGAAGCGAAGGCCGGTGGCGGGTCTGCAACCCTGTCTATGGG  
    

*purA* (478 bp):  

.. code-block:: bash

    ATAACGCGCGTGAGAAAGCGCGTGGCGCGAAAGCGATCGGCACCACCGGTCGTGGTATCGGGCCTGCTTATGAAGAT  
    AAAGTAGCACGTCGCGGTCTGCGTGTTGGCGACCTTTTCGACAAAGAAACCTTCGCTGAAAAACTGAAAGAAGTGAT  
    GGAATATCACAACTTCCAGTTGGTTAACTACTACAAAGCTGAAGCGGTTGATTACCAGAAAGTTCTGGATGATACGA  
    TGGCTGTTGCCGACATCCTGACTTCTATGGTGGTTGACGTTTCTGACCTGCTCGACCAGGCGCGTCAGCGTGGCGAT  
    TTCGTCATGTTTGAAGGTGCGCAGGGTACGCTGCTGGATATCGACCACGGTACTTATCCGTACGTAACTTCTTCCAA  
    CACCACTGCTGGTGGCGTGGCGACCGGTTCCGGCCTGGGCCCGCGTTATGTTGATTACGTTCTGGGTATCCTCAAAG  
    CTTACTCCACTCGTGT  
    

*recA* (510 bp):  

.. code-block:: bash

    CGCACGTAAACTGGGCGTCGATATCGACAACCTGCTGTGCTCCCAGCCGGACACCGGCGAGCAGGCACTGGAAATCT  
    GTGACGCCCTGGCGCGTTCTGGCGCAGTAGACGTTATCGTCGTTGACTCCGTGGCGGCACTGACGCCGAAAGCGGAA  
    ATCGAAGGCGAAATCGGCGACTCTCACATGGGCCTTGCGGCACGTATGATGAGCCAGGCGATGCGTAAGCTGGCGGG  
    TAACCTGAAGCAGTCCAACACGCTGCTGATCTTCATCAACCAGATCCGTATGAAAATTGGTGTGATGTTCGGTAACC  
    CGGAAACCACTACCGGTGGTAACGCGCTGAAATTCTACGCCTCTGTTCGTCTCGACATCCGTCGTATCGGCGCGGTG  
    AAAGAGGGCGAAAACGTGGTGGGTAGCGAAACCCGCGTGAAAGTGGTGAAGAACAAAATCGCTGCGCCGTTTAAACA  
    GGCTGAATTCCAGATCCTCTACGGCGAAGGTATCAACTTCTACGGCGA