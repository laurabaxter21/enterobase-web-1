How EnteroBase uses your personal data (GDPR)
=============================================
The General Data Protection Regulation is an EU regulation that was set up 
in 2016 and came into force on 25th May 2018. These rules give people the 
explicit right to know what websites and companies are doing with their personal 
information (e.g. how it is stored, how it is processed, or shared with third
parties). These regulations also explicitly give people a say in how their 
information is used.

We feel these are important issues and wish to clarify what happens within 
EnteroBase in relation to the GDPR. A full description of the EnteroBase terms 
and conditions can be found here  http://enterobase.readthedocs.io/en/latest/enterobase-terms-of-use.html


What data do we store
---------------------
On registration we ask for the following:

* Your username & email.
* Your password
* Your First/last name.
* Your department & institution.
* The city and country of the institution. 

This is to allow us to contact you if there are any problems. We will be
emailing you from time to time. Some emails will be automated messages
informing you that long running analyses, such as SNP analyses, have been
completed. Other emails will be emails such as this one informing you about
changes to EnteroBase. These emails are an integral part of EnteroBase. If
this is problematic, you may delete your account at any time.

We also store any sequence read data that you upload for processing. We keep
it so we can troubleshoot any unexpected downstream results, and so we can
apply your data to new analyses after you�ve uploaded it. Your sequence
read data is only available to EnteroBase developers and is not made public.
We would eventually like to transfer sequence read data to services such as
ENA/NCBI/DDBJ, but we will contact you directly for your permission when this
occurs.

We also use cookies, track your IP and webpages that you access on EnteroBase. 
We can also track any data you generate and share through EnteroBase, such as 
workspaces or trees. 
This is so we can analyse the performance of EnteroBase and make improvements. We 
also use this information to detect abuse.

We do not sell your information to any third party but we may need to share it 
in exceptional cases. Please see the terms and conditions for more details 
 
http://enterobase.readthedocs.io/en/latest/enterobase-terms-of-use.html#when-we-share-your-information

How is data stored?
-------------------
Your user account data is stored in a relational database with strict access only  
to EnteroBase developers and system administrators at the University of Warwick. 
These data are not readily accessible outside of the University. 

All passwords are hashed and encrypted. We have no way to read your original password.

Transferring EnteroBase
-----------------------
In future, we may transfer the administration of EnteroBase to another 
organisation. We will always tell you by email if this happens and we will 
ensure that the transfer maintains your rights.

Deletion from EnteroBase
------------------------
You can delete your EnteroBase account, which will remove all your personal 
information immediately by visiting 

http://enterobase.warwick.ac.uk/auth/delete-account

You will need to login to delete your account.

Requests from you
-----------------
* We will send you a copy of all your stored personal data, on request
* We will delete your account and all your personal data, on request
* We will clarify any specifics, on request

Please send requests to enterobase@warwick.ac.uk

