Local EnteroBase's documentation
================================

The main purpose of Local EnteroBase version is to enable our partners (users) to assemble their read files locally and send the strains metadata along with the assembly files to Warwick EnteroBase

Click here for the `full Local EnteroBase documentation <https://local-enterobase.readthedocs.io>`_




