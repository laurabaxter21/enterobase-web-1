EnteroBase SNPs JBrowse
=======================
In an EnteroBase SNP project (see :doc:`snp-projects`), it is possible to
visualize the SNPs with respect to the reference using a window which is an
instance of JBrowse. :doc:`JBrowse <enterobase-snps-jbrowse>` provides genome
browser functionality which is described on this page. This allows scrolling
and zooming through the genome with a track selector controlling what data is
displayed.

The :doc:`JBrowse <enterobase-snps-jbrowse>` window has its own online
documentation accessible from the Help menu at ``Help -> ? General`` which
covers all of the basics of using the :doc:`JBrowse
<enterobase-snps-jbrowse>` graphical user interface. The documentation below
repeats some information available from the online help and also includes
some EnteroBase specific information.

Moving/ scrolling
-----------------
The view can be moved/ scrolled by clicking and dragging in the track area or
by clicking the left and right arrows in the navigation bar or by pressing
the left and right arrow keys while holding down "shift".

Zooming in and out
------------------
When the :doc:`JBrowse <enterobase-snps-jbrowse>` window is first opened, the
default tracks selected are "Prokka Annotation", "wgMLST" and "Reference
sequence". (It is likely that the initial zoom level will not allow reading
the bases in the reference sequence. Zooming in may be necessary in order to
make the bases readable and also to read the annotations from Prokka or
wgMLST - or other - loci.)

Zooming in can be achieved by clicking the magnification glass icon with a
plus sign in the navigation bar, pressing the up arrow key while holding down
the shift key and also by double-clicking in the main display area.

Similarly, zooming out is done by clicking the magnification glass icon with
a minus sign in the navigation bar, pressing the down arrow key while holding
down the shift key and double-clicking in the main display area with the
shift key held down.

It is also possible to zoom into a specific area of the display area by using
"rubber-band" zooming (or "dynamic zooming"). In order to "rubber-band" zoom,
click on one end of the targeted area with the shift key held down. Then,
continuing to hold down the mouse button, drag the mouse to the other end of
the target area. The targeted area will expand to fill the main display area.

Tracks
------
The available tracks in EnteroBase are organised in three main areas in the
track selector which is on the left hand side of the display. The top area of
the track selector include tracks for the EnteroBase genotyping schemes (such
as Achtman 7 Gene and cgMLST), Prokka annotation, repeat/ masked regions and
assembly errors. The tracks in the top area of the track selector are not
specific to the datasets/ strains included in the SNP project. The middle
area of the track selector is for the reference sequence track. The bottom
area of the track selector - with the heading "snp" - is for tracks with the
SNPs in all of the strains in the SNP project.

The tracks currently displayed may be changed by clicking on the tick/ check
box next to the track's name in the track selector on the left hand side of
the display. (This track selector differs from the "track list" and also the
more complex "faceted track selector" referred to in some of the JBrowse
documentation.) A track may also be unselected by clicking on the circled
cross icon on the left end of the track label for the track in the main
display area. (Other options for manipulating the track are available by
clicking on the downward arrow which appears on mousing over the track
label.)

The tracks in the main display area may be put into any desired order by
dragging and dropping the track labels on the left hand side (in the main
display area, not the track selector).

Searching
---------
It is possible to search for either a feature or reference sequence by typing
its name into the location box in the JBrowse window and pressing Enter. Then
the display will jump to that feature or reference feature. Also, while
typing in a feature name, JBrowse will provide suggestions. A particular
suggestion may be chosen by clicking on the suggestion in the dropdown menu
that appears and clicking on the "Go" button.

Example Searches
^^^^^^^^^^^^^^^^
Example searches are described for the "workshop_snp" project in the
Escherichia database. Trying these searches will require going to the
`Escherichia database
page <http://enterobase.warwick.ac.uk/species/index/ecoli>`_ and clicking on
"Load Workspace" on the right hand side of the page near the top. Then click
on "workshop_snp" and press the "Load" button.

``lacZ`` will jump to the feature with that name.  (Also, notice that typing "lac" alone will provide a number of
suggestions.)  

``25000`` will centre the display at base 25000 on the current sequence (for the current contig).  

``NODE_5_length_173547_cov_28.7718_ID_9`` jumps to contig with name NODE_5_length_173547_cov_28.7718_ID_9  

``NODE_6_length_151020_cov_31.5251_ID_11`` jumps to contig with name NODE_6_length_151020_cov_31.5251_ID_11  

``NODE_2_length_244251_cov_28.0988_ID_3:670..2417`` jumps to the region on the contig NODE_2_length_244251_cov_28.0988_ID_3 between 670 and 2417  
