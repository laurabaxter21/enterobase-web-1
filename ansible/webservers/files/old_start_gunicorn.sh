#!/bin/sh

NAME="Entero"
FLASKDIR=/home/u1470946/enterobase-web/     # Change this to location of app.py
SOCKFILE=/home/u1470946/sock # change this to project_dir/sock (new file will be created)
USER=u1470946                           # Change this to your user/group
GROUP=admin

echo "Starting $NAME as `whoami`"

export PYTHONPATH=$FLASKDIR:$PYTHONPATH

# Create the run directory if it doesn't exist
RUNDIR=$(dirname $SOCKFILE)
test -d $RUNDIR || mkdir -p $RUNDIR

# Start your unicorn
cd $FLASDIR
exec /home/u1470946/venv/bin/gunicorn "entero:create_app('production')" -b 0.0.0.0:8000 \
#  --enable-stdio-inheritance \
  --access-logfile=$LOG/access.log \
  --error-logfile=$LOG/error.log \
  --log-level=debug \
  --name $NAME \
#  --workers $NUM_WORKERS \
  --user=$USER #--group=$GROUP \
  --log-file=$LOG/gunilog.log \
  --bind=unix:$SOCKFILE \
  --timeout 2000
